local M = {}
local constants = {
    -- Conversion rates
    avToRPM = 9.549296596425384,
    rpmToAV = 1.0 / 9.549296596425384,
}

local mathutil = require("arcanox/mathutil")

local function clearExtraData(engine)
    engine["__engineUtilExtraData"] = nil
end

local function getExtraData(engine)
    local extraData = engine["__engineUtilExtraData"]
    
    if not extraData then
        extraData = {}
        engine["__engineUtilExtraData"] = extraData
    end
    
    return extraData
end

local function getTorqueData(engine, forceFresh)
    local data = getExtraData(engine)
    
    if forceFresh or not data.torqueData then
        data.torqueData = engine:getTorqueData()
    end
    
    return data.torqueData
end

local function getIdealLoad(engine) 
    local data = getExtraData(engine)
    
    return data.idealLoad or 1
end

local function calculateBestEfficiencyLoad(engine)
    local efficiencyTable = engine.invBurnEfficiencyTable
    local maxEfficiency = 0
    local bestLoad = 0
    
    for k, v in pairs(efficiencyTable) do
        local efficiency = 1 / v
        
        if efficiency >= maxEfficiency then
            bestLoad = k
        end
        
        maxEfficiency = math.max(maxEfficiency, efficiency)
    end
    
    return bestLoad
end

local function calculateTorqueBestRpmCurve(engine)
    local idealLoad = getIdealLoad(engine)
    local torqueData = getTorqueData(engine)
    local curveData = torqueData.curves[torqueData.finalCurveName]
    local torqueCurve = curveData.torque
    local staticFriction = engine.friction
    local points = {}
    
    for t = 0, torqueData.maxTorque + 10, 10 do
        local bestRpm = torqueData.maxTorqueRPM
        local found = false
        
        for r = engine.idleRPM, torqueData.maxTorqueRPM, 10 do
            local dynamicFriction = r * constants.rpmToAV * engine.dynamicFriction
            local maxTorque = torqueCurve[r] - staticFriction - dynamicFriction
            
            if not found and maxTorque >= t / idealLoad then
                bestRpm = r
                found = true
            end
        end
        
        table.insert(points, {t, bestRpm})
    end
    
    return createCurve(points)
end

local function getBestRpmForTorque(engine, torque)
    local data = getExtraData(engine)
    local torqueData = getTorqueData(engine)
    local requestedFlywheelTorque = torque + engine.friction + engine.dynamicFriction * engine.outputAV1
    
    if not data.torqueBestRpmCurve then
       data.torqueBestRpmCurve = calculateTorqueBestRpmCurve(engine)
    end
    
    local requestedTorque = mathutil.round(math.min(requestedFlywheelTorque, torqueData.maxTorque))
    
    return data.torqueBestRpmCurve[requestedTorque]
end

local function getAchievableEngineTorque(engine, forceFresh)
    local torqueData = getTorqueData(engine, forceFresh)
    local curveData = torqueData.curves[torqueData.finalCurveName]
    local torqueCurve = curveData.torque
    local rpm = math.floor(engine.outputAV1 * constants.avToRPM)
    local rawMaxTorque = torqueCurve[rpm] or 0
    
    return math.abs(rawMaxTorque - engine.friction - (engine.dynamicFriction * engine.outputAV1))
end

local function setIdealLoadForEngine(engine, idealLoad)
    clearExtraData(engine)
    
    local extraData = getExtraData(engine)
    
    extraData.idealLoad = idealLoad
end

M.calculateBestEfficiencyLoad = calculateBestEfficiencyLoad
M.getBestRpmForTorque = getBestRpmForTorque
M.getAchievableEngineTorque = getAchievableEngineTorque
M.setIdealLoadForEngine = setIdealLoadForEngine

return M