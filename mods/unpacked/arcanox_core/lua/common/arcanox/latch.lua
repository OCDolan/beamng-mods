local M = {}

-- Timed Latch
local timedLatch = {}
timedLatch.__index = timedLatch

local function newTimedLatch(timeToLatch, immediateLatchOnTrue, immediateLatchOnFalse)
    local obj = {
        timeToLatch = timeToLatch,
        value = false,
        elapsedTime = 0,
        immediateTrue = immediateLatchOnTrue == true,
        immediateFalse = immediateLatchOnFalse == true,
    }
    
    setmetatable(obj, timedLatch)
    return obj
end

function timedLatch:test(value, dt)
    if value == true and self.immediateTrue then
        self.elapsedTime = 0
        self.value = true
        return true
    end
    
    if value == false and self.immediateFalse then
        self.elapsedTime = 0
        self.value = false
        return false
    end
    
    if value == self.value then
        self.elapsedTime = 0
    else
        self.elapsedTime = self.elapsedTime + dt
    end
    
    if self.elapsedTime >= self.timeToLatch then
        self.elapsedTime = 0
        self.value = value
    end
    
    return self.value
end

function timedLatch:reset(value)
    value = not not value -- coerce to boolean
    
    self.elapsedTime = 0
    self.value = value
end

M.newTimedLatch = newTimedLatch

-- Symmetric threshold latch
local thresholdLatch = {}
thresholdLatch.__index = thresholdLatch

--- A threshold latch can be "set" with a particular value, where it will remain until
--- that value exceeds the set value by a given threshold in either direction
-- @param thresholdWidth The amount in either direction by which the test value must exceed the set value for the latch to reset
local function newThresholdLatch(thresholdWidth)
    local obj = {
        thresholdWidth = thresholdWidth,
        state = false,
        setValue = nil,
    }
    
    setmetatable(obj, thresholdLatch)
    return obj
end

--- Sets the latch with the given value
-- @tparam number value The initial value to which the latch is set
function thresholdLatch:set(value)
    self.setValue = value
    self.state = true
end

--- Gets the current set value for the latch
-- @treturn number
function thresholdLatch:getSetpoint()
    return self.setValue
end

--- Gets whether the latch is active or not
-- @treturn bool
function thresholdLatch:get()
    return self.state
end

--- Queries the latch with the given value
-- @tparam number value The test value with which to query
-- @treturn bool True if the latch is still set, or false if it has unlatched (i.e. the test value has exceeded the threshold relative to the set value)
function thresholdLatch:test(value)
    if self.setValue ~= nil and math.abs(value - self.setValue) > self.thresholdWidth then
        self.state = false
        self.setValue = nil
    end
    
    return self.state
end

--- Resets (unsets) the latch
function thresholdLatch:reset()
    self.state = false
    self.setValue = nil
end

M.newThresholdLatch = newThresholdLatch

-- Asymmetric threshold latch
local thresholdGate = {}
thresholdGate.__index = thresholdGate

--- A threshold gate will become enabled once the test value exceeds the "on" threshold,
--- and will remain enabled until the test value exceeds the "off" value in the opposite direction
-- @tparam number thresholdOn The "on" threshold for the gate
-- @tparam number thresholdOff The "off" threshold for the gate
-- @tparam[opt] bool initialState The initial state of the gate. Defaults to off.
local function newThresholdGate(thresholdOn, thresholdOff, initialState)
    local obj = {
        thresholdOn = thresholdOn,
        thresholdOff = thresholdOff,
        state = initialState or false,
        initialState = initialState or false,
    }
    
    setmetatable(obj, thresholdGate)
    return obj
end

--- Queries the gate with the given value
-- @tparam number value The test value with which to query
-- @treturn bool True if the gate is still enabled, false if it has become disabled
function thresholdGate:test(value)
    local testThreshold = self.state and self.thresholdOff or self.thresholdOn
    
    if self.thresholdOn > self.thresholdOff then
        self.state = value >= testThreshold
    else
        self.state = value <= testThreshold
    end
    
    return self.state
end

-- Gets whether the gate is active or not
function thresholdGate:get()
    return self.state
end

--- Resets the gate to its initial state
function thresholdGate:reset()
    self.state = self.initialState
end

M.newThresholdGate = newThresholdGate

return M