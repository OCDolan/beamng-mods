local M = {}

local function round(val, precision)
    precision = precision or 0
    
    local mult = 10 ^ precision
    
    return math.floor(val * mult + 0.5) / mult
end

local function sign(val)
    if val > 0 then return 1 end
    if val < 0 then return -1 end
    return 0
end

local function maxall(...)
    local arg = {...}
    
    if #arg == 0 then 
        error("bad number of arguments")
    end
    
    local vMax = arg[1]
    
    for i = 2, #arg do
        if arg[i] > vMax then vMax = arg[i] end
    end
    
    return vMax
end

local function minall(...)
    local arg = {...}
    
    if #arg == 0 then 
        error("bad number of arguments")
    end
    
    local vMin = arg[1]
    
    for i = 2, #arg do
        if arg[i] < vMin then vMin = arg[i] end
    end
    
    return vMin
end

local function clamp(val, min, max)
    return math.min(math.max(val, min), max)
end

local function maprange(val, srcMin, srcMax, dstMin, dstMax, clampOutput)
    if clampOutput ~= false then clampOutput = true end
    if type(dstMin) ~= "number" then dstMin = 0.0 end
    if type(dstMax) ~= "number" then dstMax = 1.0 end

    local normalized = (val - srcMin) / (srcMax - srcMin) -- 0.0 to 1.0 when inside [srcMin, srcMax]
    
    local dstRange = dstMax - dstMin
    local output = dstRange * normalized + dstMin
    
    if clampOutput then
        local cMin = math.min(dstMin, dstMax)
        local cMax = math.max(dstMin, dstMax)
        
        output = clamp(output, cMin, cMax)
    end
    
    return output
end

local function blend(val1, val2, factor)
    return val1 * (1.0 - factor) + val2 * factor
end

local function kwToTorque(kw, av)
    -- P = T * w => T = P / w
    return ( kw * 1000 ) / av
end

local function torqueToKw(torque, av)
    -- P = T * w
    return (torque * av) / 1000
end

-- ROLLING AVERAGE
local rollingAverage = {}
rollingAverage.__index = rollingAverage

function newRollingAverage(windowSize)
    assert(type(windowSize) == "number", "windowSize must be a number")
    assert(windowSize > 0, "windowSize must be greater than 0")
    
    local obj = {
        windowSize = windowSize,
        bucket = {},
        value = 0,
        position = 0,
    }
    
    for i = 1, windowSize do
        obj.bucket[i] = 0
    end
    
    setmetatable(obj, rollingAverage)
    
    return obj
end

function rollingAverage:push(value)
    -- subtract previous value for this spot from average
    self.value = self.value - self.bucket[self.position + 1] / self.windowSize
    -- add new value for this spot to average
    self.value = self.value + value / self.windowSize
    -- put new value in this spot
    self.bucket[self.position + 1] = value
    -- move to next spot
    self.position = (self.position + 1) % self.windowSize
    
    return self.value
end

function rollingAverage:get()
    return self.value
end

function rollingAverage:reset(value)
    for i = 1, self.windowSize do
        self.bucket[i] = value or 0
    end
end

M.round = round
M.sign = sign
M.clamp = clamp
M.minall = minall
M.maxall = maxall
M.maprange = maprange
M.blend = blend
M.kwToTorque = kwToTorque
M.torqueToKw = torqueToKw
M.newRollingAverage = newRollingAverage

return M