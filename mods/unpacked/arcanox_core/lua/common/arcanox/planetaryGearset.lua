local M = {}
local abs = math.abs

-- Planetary gearset class
local planetaryGearset = {}
planetaryGearset.__index = planetaryGearset

local function newPlanetaryGearset(config)
    local gearset = {
        sunGearTeeth = config.sunGearTeeth,
        planetGearTeeth = config.planetGearTeeth,
        ringGearTeeth = config.ringGearTeeth,
        
        ringCarrierRatio = config.ringGearTeeth / (config.ringGearTeeth + config.sunGearTeeth),
        sunCarrierRatio = config.sunGearTeeth / (config.ringGearTeeth + config.sunGearTeeth),
        sunRingRatio = config.sunGearTeeth / config.ringGearTeeth,
        
        sunGearAV = 0,
        ringGearAV = 0,
        carrierAV = 0,
    }
    
    -- Calculate gear radii/masses
    local gearDensity = config.gearDensity or 8050 -- kg/m3, density of steel
    local gearMassCoef = math.pi * config.gearWidth * gearDensity -- precompute pi * height * density part of mass calculation
    local sunGearRadius = config.sunGearTeeth * config.gearRadiusCoef
    local sunGearMass = sunGearRadius * sunGearRadius * gearMassCoef
    local planetGearRadius = config.planetGearTeeth * config.gearRadiusCoef
    local planetGearMass = planetGearRadius * planetGearRadius * gearMassCoef
    local ringGearRadius = config.ringGearTeeth * config.gearRadiusCoef
    local ringGearMass = ringGearRadius * ringGearRadius * gearMassCoef
    local carrierRadius = sunGearRadius + planetGearRadius * 2
    local carrierMass = planetGearMass * (config.planetGearCount or 4) + (config.additionalCarrierMass or planetGearMass) -- assume one planet gear's worth of mass on the carrier itself
    
    -- Calculate inertias
    gearset.sunGearInertia = 0.5 * sunGearMass * sunGearRadius * sunGearRadius
    gearset.ringGearInertia = 0.5 * ringGearMass * ringGearRadius * ringGearRadius
    gearset.carrierInertia = 0.5 * carrierMass * carrierRadius * carrierRadius
    
    setmetatable(gearset, planetaryGearset)
    
    return gearset
end

-- AV EQUATIONS

function planetaryGearset:getRingAVFromSunAV(sunAV)
    return ((self.sunGearTeeth + self.ringGearTeeth) * self.carrierAV - self.sunGearTeeth * sunAV) / self.ringGearTeeth
end

function planetaryGearset:getRingAVFromCarrierAV(carrierAV)
    return ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.sunGearTeeth * self.sunGearAV) / self.ringGearTeeth
end

function planetaryGearset:getCarrierAVFromSunAV(sunAV)
    return (self.sunGearTeeth * sunAV + self.ringGearTeeth * self.ringGearAV) / (self.sunGearTeeth + self.ringGearTeeth)
end

function planetaryGearset:getCarrierAVFromRingAV(ringAV)
    return (self.sunGearTeeth * self.sunGearAV + self.ringGearTeeth * ringAV) / (self.sunGearTeeth + self.ringGearTeeth)
end

function planetaryGearset:getSunAVFromRingAV(ringAV)
    return ((self.sunGearTeeth + self.ringGearTeeth) * self.carrierAV - self.ringGearTeeth * ringAV) / self.sunGearTeeth
end

function planetaryGearset:getSunAVFromCarrierAV(carrierAV)
    return ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.ringGearTeeth * self.ringGearAV) / self.sunGearTeeth
end

-- TORQUE EQUATIONS

function planetaryGearset:getRingTorqueFromSunTorque(sunTorque)
    return sunTorque / self.sunRingRatio
end

function planetaryGearset:getRingTorqueFromCarrierTorque(carrierTorque)
    return -carrierTorque * self.ringCarrierRatio
end

function planetaryGearset:getCarrierTorqueFromSunTorque(sunTorque)
    return -sunTorque / self.sunCarrierRatio
end

function planetaryGearset:getCarrierTorqueFromRingTorque(ringTorque)
    return -ringTorque / self.ringCarrierRatio
end

function planetaryGearset:getSunTorqueFromRingTorque(ringTorque)
    return ringTorque * self.sunRingRatio
end

function planetaryGearset:getSunTorqueFromCarrierTorque(carrierTorque)
    return -carrierTorque * self.sunCarrierRatio
end

-- INERTIA EQUATIONS

function planetaryGearset:getTotalCarrierInertia()
    local fromSun = self.sunGearInertia / self.sunCarrierRatio / self.sunCarrierRatio
    local fromRing = self.ringGearInertia / self.ringCarrierRatio / self.ringCarrierRatio
    
    return self.carrierInertia + fromSun + fromRing
end

function planetaryGearset:getTotalSunGearInertia()
    local fromCarrier = self.carrierInertia * self.sunCarrierRatio * self.sunCarrierRatio
    local fromRing = self.ringGearInertia * self.sunRingRatio * self.sunRingRatio
    
    return self.sunGearInertia + fromCarrier + fromRing
end

function planetaryGearset:getTotalRingGearInertia()
    local fromSun = self.sunGearInertia / self.sunRingRatio / self.sunRingRatio
    local fromCarrier = self.carrierInertia * self.ringCarrierRatio * self.ringCarrierRatio
    
    return self.ringGearInertia + fromSun + fromCarrier
end

-- UPDATE FUNCTIONS
function planetaryGearset:updateAVs(sunGearAV, carrierAV, ringGearAV)
    if sunGearAV and carrierAV and ringGearAV then
        error("Only two of three values may be provided to updateAV")
    end
    
    if not sunGearAV then
        self.carrierAV = carrierAV
        self.ringGearAV = ringGearAV
        self.sunGearAV = ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.ringGearTeeth * ringGearAV) / self.sunGearTeeth
    elseif not carrierAV then
        self.ringGearAV = ringGearAV
        self.sunGearAV = sunGearAV
        self.carrierAV = (self.sunGearTeeth * sunGearAV + self.ringGearTeeth * ringGearAV) / (self.sunGearTeeth + self.ringGearTeeth)
    else
        self.sunGearAV = sunGearAV
        self.carrierAV = carrierAV
        self.ringGearAV = ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.sunGearTeeth * sunGearAV) / self.ringGearTeeth
    end
end

M.newPlanetaryGearset = newPlanetaryGearset

return M