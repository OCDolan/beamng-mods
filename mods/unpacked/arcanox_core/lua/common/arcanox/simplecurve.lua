local M = {}

local mathutil = require("arcanox/mathutil")

local simpleCurve = {}
simpleCurve.__index = simpleCurve

local function createSimpleCurve(headerTable, xAxisName, yAxisName, default)
    headerTable = headerTable or default
    
    if not headerTable or type(headerTable) ~= "table" then
        error("non-table value passed to createSimpleCurve (" .. "x axis: ".. xAxisName .. ", y axis: " .. yAxisName ..")")
    end
    
    local workingTable = tableFromHeaderTable(headerTable)
    local rawPoints = {}
    local minX = math.huge
    local minY = math.huge
    local maxX = -math.huge
    local maxY = -math.huge
    
    for i, v in pairs(workingTable) do
        local x = v[xAxisName]
        local y = v[yAxisName]
        
        if not x or not y then
            error("Curve table contained a point without an x or a y value (index " .. tostring(i) .. ", x axis: ".. xAxisName .. ", y axis: " .. yAxisName ..")")
        end
        
        minX = math.min(minX, x)
        minY = math.min(minY, y)
        maxX = math.max(maxX, x)
        maxY = math.max(maxY, y)
        table.insert(rawPoints, {x, y})
    end
    
    local rawCurve = createCurve(rawPoints)
    local newSimpleCurve = {
        minX = minX,
        minY = minY,
        maxX = maxX,
        maxY = maxY,
        curve = rawCurve,
    }
    
    setmetatable(newSimpleCurve, simpleCurve)
    return newSimpleCurve
end

function simpleCurve:get(x)
    local normalizedLow = math.floor(x)
    local normalizedHigh = math.ceil(x)
    
    if normalizedLow < self.minX then return self.curve[self.minX] end
    if normalizedHigh > self.maxX then return self.curve[self.maxX] end
    
    local low = self.curve[normalizedLow]
    local high = self.curve[normalizedHigh]
    
    return mathutil.maprange(x, normalizedLow, normalizedHigh, low, high)
end

M.createSimpleCurve = createSimpleCurve

return M