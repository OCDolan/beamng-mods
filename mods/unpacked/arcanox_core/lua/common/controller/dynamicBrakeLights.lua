-- required modules
local latch = require("arcanox/latch")
local debug = require("arcanox/debugutil")("brakeLights", false)

-- module
local M = {
    type = "auxiliary",
    relevantDevice = nil,
    defaultOrder = 1000,
    
    -- properties
    jbeamData = nil,
    activeGears = nil,
    lightsGate = nil,
    accelSmoother = nil,
    electricsName = nil,
    electricsOnValue = nil,
}

function M.updateGFX(dt)
    local currentSpeed = electrics.values.wheelspeed
    local acceleration = M.accelSmoother:get(sensors.gy2, dt)
    
    if M.activeGears[electrics.values.gear] and M.lightsGate:test(-acceleration) then
        electrics.values[M.electricsName] = math.max(electrics.values[M.electricsName], M.electricsOnValue)
    end
    
    debug:putValue("Speed (M/S)", currentSpeed)
    debug:putValue("Acceleration (M/S/S)", acceleration)
    debug:putValue("Latch", M.lightsGate.state)
end

function M.init(jbeamData)
    M.jbeamData = jbeamData
    
    M.electricsName = jbeamData.electricsName or "brakelights"
    M.electricsOnValue = jbeamData.electricsOnValue or 1
    
    if type(jbeamData.activeGears) == "string" then
        M.activeGears = {}
        
        for i = 1, string.len(jbeamData.activeGears) do
            local gear = string.sub(jbeamData.activeGears, i, i)
            
            M.activeGears[gear] = true
        end
    else
        M.activeGears = {["D"] = true, ["L"] = true}
    end
    
    M.lightsGate = latch.newThresholdGate(
        jbeamData.decelForBrakeLightsOn  or 1.3,
        jbeamData.decelForBrakeLightsOff or 0.5
    )
    
    M.accelSmoother = newTemporalSmoothingNonLinear(jbeamData.accelSmoothing or 5.5)
end

return M