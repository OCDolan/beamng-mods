local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 1000
M.version = "4.1.0"

local constants = {
    -- Conversion rates
    mpsToMph = 2.23694,
    hpToKw = 0.7457,
    kwToHp = 1.341021,
    torqueToHpCoef = 7120.54016,
    kWhToJ = 3600000,
    sToH = 1.0 / 3600,
    avToRPM = 9.549296596425384,
    rpmToAV = 1.0 / 9.549296596425384,
}

-- lib requires
local mathutil = require("arcanox/mathutil")
local latch = require("arcanox/latch")
local pid = require("arcanox/pid")
local debugutil = require("arcanox/debugutil")("hybrid", true)
local engineutil = require("arcanox/engineutil")
local simpleCurve = require("arcanox/simplecurve")

local min = math.min
local max = math.max
local abs = math.abs
local maprange = mathutil.maprange
local clamp = mathutil.clamp
local blend = mathutil.blend
local sign = mathutil.sign
local fnum = function(v, p) return debugutil:formatNumber(v, p) end

local engine = nil
local gearbox = nil
local rearMotor = nil
local battery = nil
local mainTank = nil
local CMU = nil

local soundSources = {
    mg1Whine = nil,
    mg2Whine = nil,
    motorRamp = nil,
    motorRampHarmony = nil,
}

-- Drive modes control parameters
local controlParameters = {
    hybrid = {
        throttleCurveExponent = 0.9
    }
}
local initialControlParameters

-- Calculated/initial values
local initialWheelBrakeSplit = {}

-- Torque data
local torqueData = {
    firstCalculation = true,
    maxElectricTorque = 0,
    fullBatteryElectricTorque = 0,
    maxEngineGeneratorTorque = 0,
    maxEngineTorque = 0,
    maxRawEngineTorque = 0,
    maxCombinedTorque = 0,
    maxRearAxleTorque = 0,
    engineIdleTorque = 0,
    engineMaxTorqueRpm = 0,
    engineTorqueCurve = {},
    gearboxMaxMG2Torque = 0,
    gearboxMaxMG1TorqueUnchecked = 0,
    gearboxMaxMG2TorqueUnchecked = 0,
    targetMG1Direction = 1,
    targetMG1Torque = 0,
    targetMG2Torque = 0,
    targetRearMotorTorque = 0,
    achievableEngineTorque = 0,
    achievableEngineTorqueCarrier = 0,
    achievableEngineTorqueSun = 0,
    achievableEngineTorqueRing = 0,
    requestedEngineTorque = 0,
    requestedEngineTorqueCarrier = 0,
    requestedEngineTorqueSun = 0,
    requestedEngineTorqueRing = 0,
    actualEngineTorque = 0,
    actualEngineTorqueCarrier = 0,
    actualEngineTorqueRing = 0,
    actualEngineTorqueSun = 0,
    requestedTotalOutputTorque = 0,
    requestedElectricOutputTorque = 0,
    requestedEngineOutputTorque = 0,
}

-- State-related variables
local state = {
    current = "init",
    next = nil,
    last = nil,
    timeSinceVehicleLoaded = 0,
    timeSinceInit = 0,
    timeSincePowerOn = 0,
    gearLockedInPark = false,
    ecvtDriveMode = 0,
    shownDisabledMessage = false,
    engineOn = false,
    watertemp = 0,
    speedMph = 0,
    brake = 0,
    throttle = 0,
    posThrottle = 0,
    negThrottle = 0,
    throttleAggressiveness = 0,
    gear = "P",
    brakeInput = 0,
    chargeModeEnabled = false,
    isInDriveGear = false,
    isParked = true,
    isStopped = true,
    launchControl = false,
    mg2TorqueAdjustment = 0,
    miscEngineDemandReason = "none",
    miscEngineDemand = false,
    lastThrottle = 0,
    startingTime = 0,
    forcedChargeMode = false,
    reducedPropulsion = false,
    currentAcceleration = 0,
}

-- Engine demand variables
local engineDemand = {
    isDemanded = false,
    wasDemanded = false,
    reason = "none",
    triggerReason = "none",
    miscDemand = false,
    miscDemandReason = "none",
    waterTempDemand = false, -- TODO: Change to latch?
}

-- Engine state-related variables
local engineState = {
    runTimeSincePowerOn = 0,
    timeSinceStart = 5,
    timeSinceCanShutOff = 1,
    loadTorque = 0,
    minWorkingRpm = 0,
    minRpm = 0,
    maxRpm = 0,
    targetRpm = 0,
    rpm = 0,
    targetThrottle = 0,
    throttleOverride = nil,
    chargeThrottle = 0,
    startAttempts = 0,
    hasStartedSinceInit = false,
    powerGenerated = 0,
    initialOutputTorqueState = 1,
    initialScaleOutputTorque = nop,
}

-- Braking-related variables
local braking = {
    desiredRegen = 0,
    disableRegen = false,
    totalPossibleBrakeTorque = 0,
    totalPropulsedBrakeTorque = 0,
    totalUnpropulsedBrakeTorque = 0,
    currentBrakingCoef = 0,
    timeSinceWheelSlip = 0,
    lastAverageWheelSpeed = 0,
    timeSinceAppliedBrakes = 0,
    maxRegenTorque = 0,
    uncappedMaxRegenTorque = 0,
    requestedRegenTorque = 0,
    currentRegenTorque = 0,
}

-- Power-related variables
local power = {
    totalPowerOutKw = 0,
    availableMG1Power = 0,
    availableMG2Power = 0,
    availablePriorityEvPower = 0,
    availableSecondaryEvPower = 0,
    requestedChargePower = 0,
    maxBatteryOutputPower = 0,
    availableBatteryPower = 0,
    batteryLevelRaw = 1,
    batteryLevel = 0,
}

local function updateDefaultDebugValues()
    debugutil:putValue("Power", "")
    debugutil:putValue("State", "")
    debugutil:putValue("eCVT Drive Mode", "")
    debugutil:putValue("Fuel Remaining (L)", "")
    debugutil:putValue("Engine Run Time", "")
    debugutil:putValue("Engine demanded on", "")
    debugutil:putValue("Engine on reason", "")
    debugutil:putValue("Engine triggered on by", "")
    debugutil:putValue("Battery power (kW)", "")
    debugutil:putValue("Generator power (kW)", "")
    debugutil:putValue("Battery SOC %", "")
    debugutil:putValue("Battery SOC % (usable)", "")
    debugutil:putValue("Wheel Speed (Avg/Effective, MPH)", "")
    debugutil:putValue("Input Throttle", "")
    debugutil:putValue("Effective Throttle / Brake", "")
    debugutil:putValue("Throttle % EV thresh.", "")
    debugutil:putValue("Throttle Aggr.", "")
    debugutil:putValue("Throttle Rate", "")
    debugutil:putValue("Requested Braking Torque", "")
    debugutil:putValue("Requested Regen Torque (Wheels)", "")
    debugutil:putValue("Max Regen Torque (Wheels)", "")
    debugutil:putValue("Actual Regen Torque (Wheels)", "")
    debugutil:putValue("Friction Braking Torque (Req, Act)", "")
    debugutil:putValue("Total Braking Torque (%)", "")
    debugutil:putValue("Regen % of brake demand", "")
    debugutil:putValue("Friction % of full brakes", "")
    debugutil:putValue("Adjusted friction torque", "")
    debugutil:putValue("Available Power (Priority/Secondary)", "")
    debugutil:putValue("Available EV Torque (Priority/Secondary)", "")
    debugutil:putValue("Requested Torque (Total, EV, ICE)", "")
    if rearMotor then
        debugutil:putValue("Actual Torque (Total, EV F, EV R, ICE)", "")
    else
        debugutil:putValue("Actual Torque (Total, EV, ICE)", "")
    end
    debugutil:putValue("Achievable Engine Torque (Eng, S, C, R)", "")
    debugutil:putValue("Requested Engine Torque (Eng, S, C, R)", "")
    debugutil:putValue("Actual Engine Torque (Eng, S, C, R)", "")
    debugutil:putValue("ICE HP", "")
    debugutil:putValue("ICE Torque", "")
    debugutil:putValue("Combustion Torque", "")
    debugutil:putValue("Engine Torque Compensation", "")
    debugutil:putValue("Requested Charge Power", "")
    debugutil:putValue("Requested Charge Power", "")
    debugutil:putValue("Min RPM (Working/Actual)", "")
    debugutil:putValue("Max RPM", "")
    debugutil:putValue("Min Target RPM", "")
    debugutil:putValue("Max Target RPM", "")
    debugutil:putValue("Target RPM (Raw, Smooth)", "")
    debugutil:putValue("Actual RPM", "")
    debugutil:putValue("RPM Error", "")
    debugutil:putValue("Load Bias", "")
    debugutil:putValue("Engine Load Torque", "")
    debugutil:putValue("Charge Throttle", "")
    debugutil:putValue("Desired ICE Throttle", "")
    debugutil:putValue("Actual ICE Throttle", "")
    debugutil:putValue("ICE Ignition Coef", "")
    debugutil:putValue("ICE Load", "")
    debugutil:putValue("Available MG1 Power", "")
    debugutil:putValue("Available MG2 Power", "")
    debugutil:putValue("MG2 Adjustment", "")
    debugutil:putValue("Coolant Temp.", "")
    debugutil:putValue("Acceleration", "")
end
    
local function getProportionalBatteryRatio()
    return maprange(power.batteryLevelRaw, M.minBatteryPowerLevel, M.maxBatteryPowerLevel)
end

local function calculateElectricLimits(dt)
    power.batteryLevelRaw = battery.remainingRatio
    power.batteryLevel = getProportionalBatteryRatio()
    
    -- Calculate battery power limits
    if state == "poweroff" then
        power.maxBatteryOutputPower = 0
        power.availableBatteryPower = 0
        power.availablePriorityEvPower = 0
        power.availableSecondaryEvPower = 0
        engineState.powerGenerated = 0
    else
        local availableBatteryPowerCoef = maprange(power.batteryLevelRaw, M.minBatteryPowerLevel - 0.05, M.minBatteryPowerLevel)
        
        power.maxBatteryOutputPower = (battery.jbeamData.maxPowerOutput or 1e5) - M.idlePowerDraw
        power.availableBatteryPower = availableBatteryPowerCoef * power.maxBatteryOutputPower
        
        -- Figure out some general values about what the electric subsystem can currently provide
        if state.current == "evmode" then
            engineState.powerGenerated = 0
            power.availablePriorityEvPower = power.availableBatteryPower
            power.availableSecondaryEvPower = power.availableBatteryPower
        else
            local priorityPowerSpeed
            local priorityPowerBatteryLevel = M.maxPriorityPowerBatteryLevel:get(power.batteryLevel * 100)
            
            if state.engineOn then
                priorityPowerSpeed = M.maxEngineAssistPriorityPowerSpeed:get(state.speedMph)
            else
                priorityPowerSpeed = M.maxEvOnlyPriorityPowerSpeed:get(state.speedMph)
            end
            
            engineState.powerGenerated = max(0, -gearbox.motorGenerator1Power)
            power.availablePriorityEvPower = state.chargeModeEnabled and 0 or mathutil.minall(power.availableBatteryPower, priorityPowerSpeed, priorityPowerBatteryLevel) - power.requestedChargePower
            power.availableSecondaryEvPower = min(power.availableBatteryPower, M.maxSecondaryPowerBatteryLevel:get(power.batteryLevel * 100))
            
            if state.engineOn then
                power.availableSecondaryEvPower = min(M.maxEngineAssistSecondaryPowerSpeed:get(state.speedMph), power.availableSecondaryEvPower)
            end
        end
    end
end

local function calculateTorqueStatistics(dt)
    -- Calculate some torque performance statistics
        
    -- Raw max torques from gearbox and engine
    if torqueData.firstCalculation then
    	local maxEngineTorque = engine and engine.torqueData.maxTorque or 0
        local maxSunGearTorque = gearbox.mg1TorqueRating
        local maxEngineTorqueToCarrier = maxEngineTorque * gearbox.inputGearRatio
        local maxEngineTorqueToSun = abs(gearbox.psd:getSunTorqueFromCarrierTorque(maxEngineTorqueToCarrier))
        local maxUsableEngineTorqueToSun = min(maxEngineTorqueToSun, maxSunGearTorque)
        local maxEngineTorqueToOutput = abs(gearbox.psd:getRingTorqueFromSunTorque(maxUsableEngineTorqueToSun)) * gearbox.counterRingGearRatio * gearbox.cumulativeGearRatio
        
        torqueData.maxEngineGeneratorTorque = maxUsableEngineTorqueToSun
        torqueData.maxEngineTorque = maxEngineTorqueToOutput
        torqueData.maxRawEngineTorque = maxEngineTorque
        
        -- Figure out engine torque characteristics
        if engine then
            local curve = engine.torqueData.curves[engine.torqueData.finalCurveName]
            local idleTorque = curve.torque[engine.idleRPM]
            
            torqueData.engineIdleTorque = idleTorque
            torqueData.engineMaxTorqueRpm = engine.torqueData.maxTorqueRPM
            torqueData.engineTorqueCurve = curve.torque
        else
            torqueData.engineIdleTorque = 0
            torqueData.engineMaxTorqueRpm = 0
            torqueData.engineTorqueCurve = nil
        end
    
        torqueData.firstCalculation = false
    end
    
    local maxAvailableGeneratorPower = state.current == "evmode" and 0 or abs(mathutil.torqueToKw(torqueData.maxEngineGeneratorTorque, gearbox.motorGenerator1AV))
    local maxAvailableMG2Power = power.availablePriorityEvPower + power.availableSecondaryEvPower + maxAvailableGeneratorPower
    local maxAvailableMG2Torque = gearbox:getMaxMG2Torque(maxAvailableMG2Power) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
    
    if state.current == "evmode" then
        local maxAvailableMG1Torque = gearbox.psd:getRingTorqueFromSunTorque(gearbox:getMaxMG1Torque()) * gearbox.counterRingGearRatio * gearbox.cumulativeGearRatio
        
        torqueData.maxElectricTorque = maxAvailableMG2Torque + maxAvailableMG1Torque
        torqueData.fullBatteryElectricTorque = torqueData.maxElectricTorque
        torqueData.maxCombinedTorque = M.maxCombinedTorqueSmoother:get(torqueData.maxElectricTorque, dt)
    else
        -- Figure out compound torques
        
        torqueData.maxElectricTorque = maxAvailableMG2Torque
        torqueData.fullBatteryElectricTorque = gearbox:getMaxMG2Torque(power.maxBatteryOutputPower) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
        torqueData.maxCombinedTorque = M.maxCombinedTorqueSmoother:get(torqueData.maxEngineTorque + torqueData.maxElectricTorque, dt)
    end
    
    debugutil:putValue("maxEngineTorque", torqueData.maxEngineTorque)
    debugutil:putValue("maxElectricTorque", torqueData.maxElectricTorque)
    debugutil:putValue("fullBatteryElectricTorque", torqueData.fullBatteryElectricTorque)
    debugutil:putValue("maxCombinedTorque", torqueData.maxCombinedTorque)
    
    if M.maxOutputTorqueVsSpeed and (state.gear == "D" or state.gear == "L") then
        torqueData.maxCombinedTorque = min(torqueData.maxCombinedTorque, M.maxOutputTorqueVsSpeed:get(state.speedMph))
    end
    
    if M.maxReverseOutputTorqueVsSpeed and state.gear == "R" then
        torqueData.maxCombinedTorque = min(torqueData.maxCombinedTorque, M.maxReverseOutputTorqueVsSpeed:get(state.speedMph))
    end
    
    if state.reducedPropulsion then
        torqueData.maxCombinedTorque = M.reducedPropulsionTorqueCoef * torqueData.maxCombinedTorque
    end
    
    torqueData.gearboxMaxMG2Torque = gearbox:getMaxMG2Torque()
    torqueData.gearboxMaxMG1TorqueUnchecked = gearbox:getMaxMG1Torque(true)
    torqueData.gearboxMaxMG2TorqueUnchecked = gearbox:getMaxMG2Torque(true)
end

local function getTorqueData()
    return torqueData
end

local function printTorqueData()
    local curTorqueData = getTorqueData()
    
    debugutil:log("I", "Max Electric Torque: " .. fnum(curTorqueData.maxElectricTorque))
    debugutil:log("I", "Max Full-Battery Electric Torque: " .. fnum(curTorqueData.fullBatteryElectricTorque))
    debugutil:log("I", "Max Usable Engine Torque: " .. fnum(curTorqueData.maxEngineTorque))
    debugutil:log("I", "Max Optimal Total Torque: " .. fnum(curTorqueData.maxCombinedTorque))
    debugutil:log("I", "Max Engine Idle Torque: " .. fnum(curTorqueData.engineIdleTorque))
    debugutil:log("I", "Engine Max Torque RPM: " .. fnum(curTorqueData.engineMaxTorqueRpm))
end

-- #region Engine Functions

local function requestEngine(reason)
    if engineDemand.miscDemand then return end
    
    engineDemand.miscDemandReason = reason
    engineDemand.miscDemand = true
end

local function isEngineDemandedOn()
    if electrics.values.evMode and electrics.values.evMode > 0 then
        return false
    end
    
    local inputThrottle = electrics.values.throttleOverride or input.throttle
    local minRunTime
    
    if (state.engineOn or state.timeSincePowerOn > M.vehicleStartupEngineDelay) and engineState.runTimeSincePowerOn < M.vehicleStartupEngineRuntime then
        engineDemand.reason = "vehicle startup"
        return true
    end
    
    if state.watertemp < M.minAutoStopCoolantTempWarmedUp then
        minRunTime = M.engineMinRunTime * 2
    else
        minRunTime = M.engineMinRunTime
    end

    if state.current == "chargemode" or state.chargeModeEnabled then
        engineDemand.reason = "charge mode"
        return true
    end
    if not M.enableAutoStop then
        engineDemand.reason = "auto stop disabled"
        return true
    end
    if state.watertemp < M.minAutoStopCoolantTemp or engineDemand.waterTempDemand then
        engineDemand.reason = "temp"
        engineDemand.waterTempDemand = true
        return true
    else
        
    end -- engine must be warmed up
    if engineDemand.miscDemand then
        engineDemand.reason = engineDemand.miscDemandReason
        return true
    end
    if M.latchBatteryChargeUp:test(power.batteryLevel) then
        engineDemand.reason = "battery level"
        return true
    end -- battery too low (first check; doesn't trigger chargeup)
    
    if state.gear == "R" then
        -- After this point, if in R and battery >= 0.4, force engine off
        engineDemand.reason = "none (forced off due to reverse)"
        return false
    end
    
    if state.speedMph > M.maxEvOnlySpeed then
        engineDemand.reason = "speed"
        return true
    end -- too fast
    if obj:getDirectionVector().z > M.maxEvOnlyPitch then
        engineDemand.reason = "vehicle pitch"
        return true
    end -- pitched too high up (hill start)
    if state.throttleAggressiveness > M.maxEvOnlyThrottleAggressiveness then
        engineDemand.reason = "values.throttle"
        return true
    end
    
    if state.engineOn and (
        not (electrics.values.parkingbrake == 1 or braking.currentBrakingCoef >= M.autoStopBrakeThreshold) or
        braking.timeSinceAppliedBrakes < M.autoStopBrakeTime
    ) then
        -- this check only applies when not in P
        if state.gear ~= "P" then
            engineDemand.reason = "brakes"
            return true
        end
    end

    -- Easy checks are done; dynamic checks now

    -- This is the percentage of the max EV-only kW output that's available to us,
    -- based on battery level

    if state.engineOn then
        if inputThrottle > M.autoStopThrottleThreshold then
            engineDemand.reason = "accelerating with engine on"
            return true
        end -- once engine is on, must coast again to shut off
    else
        if state.gear ~= "R" and gearbox.motorGenerator2Power > M.maxEvOnlyPriorityPowerSpeed:get(state.speedMph) then
            engineDemand.reason = "priority power exceeded"
            return true
        end
    end

    -- Put this last so it doesn't override other reasons
    if (state.current == "startup" or state.current == "running") and engineState.timeSinceStart < minRunTime then
        engineDemand.reason = "time since start"
        return true
    end -- engine has to run for at least 4 seconds

    engineDemand.reason = "none"
    return false -- by default, the engine is off
end

local function stopEngine()
    if not engine then return end
    
    electrics.values.iceThrottle = 0
    engine.ignitionCoef = 0 -- Kill engine
    engine.isDisabled = true -- Prevent the user from manually restarting the engine
    engine:deactivateStarter()
end

local function handleFailedEngine()
    if not engine then return end
    
    if not state.shownDisabledMessage then
        gui.message({txt = "Engine has failed; propulsion power reduced."}, 5, "vehicle.hybrid.general")
        state.shownDisabledMessage = true
        state.reducedPropulsion = true
        engineState.timeSinceCanShutOff = 1
        electrics.values.ev = 1
        engine:disable()
    end
end

local function getEngineShutdownTorque()
    local stationaryCarrierSunAV = gearbox.psd:getSunAVFromCarrierAV(0.0)
    local sunAVError = (gearbox.motorGenerator1AV - stationaryCarrierSunAV) / 100
    local engineShutdownLoad = clamp(sunAVError, 0, 1) * M.engineSpooldownTorque
        
    if gearbox.motorGenerator1AV >= gearbox.generatorMinFullLoadAV or gearbox.motorGenerator1TorqueDirection == 1 then
        -- Regen torque can shut down engine
        engineShutdownLoad = -engineShutdownLoad
    end
    
    return engineShutdownLoad
end

-- #endregion Engine Functions

-- #region State Functions

local function resumeNormalState()
    if state.engineOn then
        state.next = "running"
    else
        state.next = "stopped"
    end
end

local function tryEnableChargeMode()
    if not (M.allowChargeMode and M.chargeModePower > 0) then
        return false
    end
    
    if state.chargeModeEnabled then
        return true
    end
    
    if power.batteryLevel < M.chargeModeUpperThreshold - 0.05 then
        state.chargeModeEnabled = true
        gui.message({txt = "Charge mode enabled"}, 5, "vehicle.hybrid.chargeMode")
        return true
    else
        gui.message({txt = "Charge mode unavailable; battery full"}, 5, "vehicle.hybrid.chargeMode")
    end
    
    return false
end

local function updateInState_init(dt)
    if state.last ~= "init" then
        if state.timeSinceVehicleLoaded >= 0.25 then -- Don't play sounds right when vehicle loads
            -- play startup sound
            obj:playSFXOnce(M.startupSampleName, gearbox.transmissionNodeID, M.startupSoundBaseVolume, 1)

            -- play interior startup sound if present
            if M.interiorStartupSampleName and M.interiorSoundNode then
                obj:playSFXOnce(M.interiorStartupSampleName, M.interiorSoundNode, M.interiorStartupSoundBaseVolume, 1)
            end
            
            state.timeSinceInit = 0
        else -- if vehicle just loaded, initialize immediately
            state.timeSinceInit = M.initializeTime
        end
        
        state.reducedPropulsion = false
        engineState.hasStartedSinceInit = false
        
        -- Reset all the stateful things
        M.throttleSmoother:reset()
        M.throttleLimiterSmoother:reset()
        M.engineStarterSmoother:reset()
        M.engineStartupLoadSmoother:reset()
        M.targetEngineRpmSmoother:set(engine.idleRPM)
        M.engineLoadSmoother:reset()
        M.mg1MaxPowerSmoother:reset()
        M.mg2MaxPowerSmoother:reset()
        M.driveThrottlePid:reset()
        M.chargeThrottlePid:reset()
        M.accelerationLimiterPid:reset()
        M.maxCombinedTorqueSmoother:reset()
        M.engineDeloadSmoother:reset()
    end
    
    state.timeSinceInit = state.timeSinceInit + dt
    electrics.values.checkengine = true
    electrics.values.hazard = true
    electrics.values.battery = true
    
    if state.timeSinceInit >= M.initializeTime then
        if not M.powerOn then
            M.powerOn = true
        end
        
        electrics.values.evMode = input.evMode or 0
        state.gearLockedInPark = false
        
        if M.startEvMode or M.forceEvMode then
            state.next = "evmode"
        elseif power.batteryLevelRaw < M.criticalBatteryPowerLevel then
            -- attempt to start engine
            
            if state.engineOn and electrics.values.rpm >= engine.idleRPM - 100 then
                if state.gear == "P" and engineState.timeSinceStart > M.engineStartupIdleTime then
                    state.next = "chargemode"
                    state.forcedChargeMode = true
                    gui.message({txt = "Battery level critical...entering recovery charge-up mode"}, 5, "vehicle.hybrid.chargeMode")
                end
            elseif engine and engine.hasFuel and M.hasAuxiliaryStarterMotor then
                if engine.starterEngagedCoef < 1 then
                    engine.ignitionCoef = 0
                    engine.isDisabled = false
                    engine.starterDisabled = false
                    engine.idleStartCoef = 1
                    state.startingTime = 0
                    state.timeSinceInit = 0
                    engine:activateStarter()
                else
                    state.startingTime = state.startingTime + dt
                    
                    if state.startingTime > 3 then
                        -- Something's not right; retry at init
                        
                        engine.ignitionCoef = 0
                        engineState.startAttempts = engineState.startAttempts + 1
                        engine:deactivateStarter()
                        state.timeSinceInit = 0
                    end
                end
            else
                resumeNormalState()
                engineState.runTimeSincePowerOn = 0.0
            end
        else
            if engineState.startAttempts > 0 then
                -- immediately try and restart the engine
                
                state.next = "startup"
            else
                resumeNormalState()
                engineState.runTimeSincePowerOn = 0.0
            end
        end
    else
        -- Prevent shifting while initializing
        
        if state.gear == "P" then
            state.gearLockedInPark = true
        end
        
        if state.gearLockedInPark and state.gear ~= "P" then
            controller.mainController.shiftToGearIndex(1)
        end
        
        electrics.values.checkengine = true
    end
end

local function updateInState_evmode(dt)
    if state.last ~= "evmode" then
        stopEngine()
        electrics.values.chargeMode = 0
    end
    
    if not M.forceEvMode and electrics.values.chargeMode == 1 then
        if tryEnableChargeMode() then
            electrics.values.evMode = 0
            resumeNormalState()
        end
    end
    
    electrics.values.chargeMode = 0
    electrics.values.ev = 1

    if electrics.values.evMode ~= 1 then
        if M.forceEvMode then
            electrics.values.evMode = 1
        else
            gui.message({txt = "EV mode disabled"}, 2, "vehicle.hybrid.evMode")
            resumeNormalState()
        end
    end
    
    if power.batteryLevel < 0.05 and not M.forceEvMode then
        electrics.values.evMode = 0
        gui.message({txt = "Battery too low; EV mode disabled"}, 2, "vehicle.hybrid.evMode")
        resumeNormalState()
    end
end

local function updateInState_chargemode(dt)
    local inverseRelativePowerLevel = maprange(power.batteryLevel, 0.95, 1.0, 1.0, 0.05)
    local forcedChargeupPower = M.forcedChargeupPower or gearbox.mg1PowerRating
    local curGenOutput = -min(power.totalPowerOutKw, 0)
    local targGenOutput = inverseRelativePowerLevel * forcedChargeupPower
    
    if state.last ~= state.current then
        M.chargeThrottlePid:reset()
    end
    
    engineState.chargeThrottle = clamp(M.chargeThrottlePid:get(targGenOutput, curGenOutput, dt), 0, 1)

    if state.gear ~= "P" then
        gui.message({txt = "Cannot cancel recovery charge-up. Please wait for battery to charge."}, 5, "vehicle.hybrid.chargeMode")
        controller.mainController.shiftToGearIndex(1) -- Park
    end
    
    if power.batteryLevelRaw >= M.minBatteryPowerLevel then
        state.forcedChargeMode = false
        gui.message({txt = "Forced charge-up complete"}, 2, "vehicle.hybrid.chargeMode")
        resumeNormalState()
    end
end

local function updateInState_stopped(dt)
    -- Stop engine when entering state
    if state.last ~= "stopped" then
        stopEngine()
    end
    
    M.engineStartupLoadSmoother:reset()
    
    -- Engage Charge Mode if needed
    if electrics.values.chargeMode == 1 then
        if not tryEnableChargeMode() then
            electrics.values.chargeMode = 0
        end
    end

    -- Switch to EV Mode if needed
    if electrics.values.evMode == 1 then
        gui.message({txt = "EV mode enabled"}, 2, "vehicle.hybrid.evMode")
        state.next = "evmode"
    end
    
    -- Start engine if needed
    if state.gear ~= "N" and engine and engineDemand.isDemanded then
        if engine.isBroken or state.reducedPropulsion or not engine.hasFuel then
            handleFailedEngine()
        else
            engine.isDisabled = false -- allow engine to restart

            if electrics.values.rpm < engine.idleRPM then
                state.next = "startup"
            else
                resumeNormalState()
            end
        end
    end
end

local function updateInState_startup(dt)
    if not engine then
        resumeNormalState()
        return
    end
    
    if state.last ~= "startup" then
        engineState.timeSinceStart = 0
        state.startingTime = 0
        engine.idleAVStartOffsetSmoother:reset()
        engine.throttleSmoother:reset()
    end
    
    if engineState.startAttempts >= 3 then
        handleFailedEngine()
        resumeNormalState()
    else
        -- Allow engine to start running as it reaches the target RPM
        engine.idleStartCoef = 0
        state.startingTime = state.startingTime + dt
        
        if engineState.rpm >= engineState.minWorkingRpm - M.engineStartupRpmThreshold then
            engine.ignitionCoef = 1
        else
            engine.ignitionCoef = 0
            M.driveThrottlePid:reset()
        end
        
        if state.startingTime <= 3 then
            -- Figure out necessary starter torque and apply it
            
            local targetStarterTorque = maprange(engineState.rpm, engineState.minWorkingRpm - M.engineStartupRpmThreshold, engineState.minWorkingRpm, M.engineStarterTorque, 0)
            local starterTorque = M.engineStarterSmoother:get(targetStarterTorque, dt)
            
            if gearbox.psd.sunGearAV < (-gearbox.generatorMinFullLoadAV * 5) then
                -- can use backwards generation drag to start engine
                starterTorque = -starterTorque
            end
            
            gearbox:setMG1PowerOverride(maprange(power.batteryLevelRaw, M.criticalBatteryPowerLevel, M.minBatteryPowerLevel, 0, gearbox.mg1PowerRating))
            gearbox:setMG1TorqueDirection(1)
            gearbox:setRequestedMG1Torque(starterTorque)
            
            if engine.combustionTorque > 1e-5 and state.engineOn then
                engineState.startAttempts = 0
                resumeNormalState()
            elseif state.gear == "N" then
                -- Starting interrupted by the driver shifting to Neutral
                state.next = "stopped"
            else
                engineState.targetThrottle = max(engineState.targetThrottle, M.engineStartupMinThrottle)
                
                if type(M.engineStartupMaxRpm) == "number" then
                    M.targetEngineRpmSmoother:set(min(M.targetEngineRpmSmoother:value(), M.engineStartupMaxRpm))
                end
            end
        else
            -- Engine isn't starting correctly; retry it from "stopped" state in one second
            
            stopEngine()
            gearbox:setRequestedMG1Torque(0)
            
            if state.startingTime > 4 then
                engineState.startAttempts = engineState.startAttempts + 1
                state.next = "stopped"
            end
        end
    end
end

local function updateInState_running(dt)
    if not state.engineOn then
        -- engine died?

        state.next = "startup"
        return
    end
    
    if electrics.values.evMode == 1 then
        if power.batteryLevel > 0.026 then
            gui.message({txt = "EV mode enabled"}, 2, "vehicle.hybrid.evMode")
            state.next = "evmode"
            return
        else
            gui.message({txt = "Battery too low; EV mode not available"}, 5, "vehicle.hybrid.evMode")
            electrics.values.evMode = 0
        end
    end
        
    if state.chargeModeEnabled then
        if electrics.values.chargeMode == 0 then
            state.chargeModeEnabled = false
            gui.message({txt = "Charge mode disabled"}, 5, "vehicle.hybrid.chargeMode")
        end
        
        if power.batteryLevel >= M.chargeModeUpperThreshold then
            state.chargeModeEnabled = false
            electrics.values.chargeMode = 0
            gui.message({txt = "Charge mode disabled (charge complete)"}, 5, "vehicle.hybrid.chargeMode")
        end
    end
    
    if electrics.values.chargeMode == 1 then
        if not tryEnableChargeMode() then
            electrics.values.chargeMode = 0
        end
    end

    if state.gear == "N" or engineDemand.isDemanded then -- engine keeps state in N; treat it like engine demand
        engineState.timeSinceCanShutOff = 0
    else
        engineState.timeSinceCanShutOff = engineState.timeSinceCanShutOff + dt

        if engineState.timeSinceCanShutOff > 1 then
            state.next = "stopped"
        end
    end
    
    if engineState.timeSinceStart < M.engineStartupIdleTime and type(M.engineStartupMaxRpm) == "number" then
        M.targetEngineRpmSmoother:set(min(M.targetEngineRpmSmoother:value(), M.engineStartupMaxRpm))
    end
end

local function updateInState_shutdown(dt)
    if M.powerOn then
        M.powerOn = false
    end
    
    if state.last ~= "shutdown" and state.timeSinceVehicleLoaded >= 0.25 then
        -- play shutdown sound
        obj:playSFXOnce(M.shutdownSampleName, gearbox.transmissionNodeID, M.shutdownSoundBaseVolume, 1)

        -- play interior shutdown sound if present
        if M.interiorShutdownSampleName and M.interiorSoundNode then
            obj:playSFXOnce(M.interiorShutdownSampleName, M.interiorSoundNode, M.interiorShutdownSoundBaseVolume, 1)
        end
    end
    
    -- Initially reset some variables
    electrics.values.evMode = 0
    electrics.values.chargeMode = 0
    electrics.values.iceThrottle = 0
    electrics.values.checkengine = false
    electrics.values.hazard = false
    electrics.values.battery = false
    state.forcedChargeMode = false
    state.chargeModeEnabled = false
    state.shownDisabledMessage = false
    state.gearLockedInPark = false
    
    -- Prevent restarting until vehicle fully shut down
    M.powerSwitch = false
    
    -- Immediately power off engine and attempt to bring it to 0RPM
    engineState.timeSinceCanShutOff = 0
    engineState.hasStartedSinceInit = false
    stopEngine()
    gearbox:setMG1TorqueDirection(-1)
    gearbox:setRequestedMG1Torque(getEngineShutdownTorque())
    gearbox:setRequestedMG2Torque(0)
    
    if rearMotor then
        rearMotor:setRequestedOutputTorque(0)
    end
    
    if engineState.rpm < 10 then
        state.next = "poweroff"
    end
end

local function updateInState_poweroff(dt)
    if M.powerSwitch then
        -- Power up vehicle
        
        if state.gear == "P" then
            state.next = "init"
            return
        else
            M.powerSwitch = false
            gui.message({txt = "Shift to Park before starting vehicle"}, 2, "vehicle.hybrid.starting")
        end
    end
    
    gearbox:setMG1PowerOverride(0)
    gearbox:setMG2PowerOverride(0)
    engineState.timeSinceCanShutOff = 1
    
    if rearMotor then
        rearMotor:setMotorPowerOverride(0)
    end
    
    if engine then
        engine:deactivateStarter()
        engine.ignitionCoef = 0
        engine.isDisabled = true
    end
    
    engineState.startAttempts = 0
    electrics.values.evMode = 0
    electrics.values.chargeMode = 0
    electrics.values.iceThrottle = 0
    electrics.values.checkengine = false
    electrics.values.hazard = false
    electrics.values.battery = false
    state.forcedChargeMode = false
    state.chargeModeEnabled = false
    state.shownDisabledMessage = false
    state.gearLockedInPark = false
end

-- #endregion State Functions

local function updateState(dt)
    state.timeSinceVehicleLoaded = state.timeSinceVehicleLoaded + dt
    
    ---------------------------
    -- BEGIN ALL-STATE LOGIC --
    ---------------------------

    if engineDemand.isDemanded and not engineDemand.wasDemanded then
        engineDemand.triggerReason = engineDemand.reason
    elseif not engineDemand.isDemanded and state.current ~= "startup" then
        engineDemand.triggerReason = "none"
    end
    
    engineDemand.wasDemanded = engineDemand.isDemanded

    -- keep things like daytime-running-lights on
    electrics.values.ignition = M.powerOn and 1 or 0
    electrics.values.running = M.powerOn and 1 or 0
    
    if engine then
        if power.batteryLevelRaw < M.criticalBatteryPowerLevel and M.hasAuxiliaryStarterMotor then
            -- Fall back to 12V starter
            
            engine.starterDisabled = false
        else
            -- prevent engine starter from working (since the eCVT starts it)
            
            engine:deactivateStarter()
            engine.starterDisabled = true
        end
    end
    
    if not M.powerSwitch and state.current ~= "poweroff" and state.current ~= "shutdown" then
        electrics.values.chargeMode = 0
        electrics.values.evMode = 0
        stopEngine()
        state.next = "shutdown"
    end
    
    -- Critical battery level indicator
    electrics.values.battery = power.batteryLevelRaw < M.criticalBatteryPowerLevel
    
    -- "EV" indicator
    if not M.powerOn or not engine.isBroken and (engineDemand.isDemanded or engineState.timeSinceCanShutOff < 1) then
        electrics.values.ev = 0
    else
        electrics.values.ev = 1
    end
    
    -- "Check Engine" light (might be triggered later too)
    electrics.values.checkengine = state.reducedPropulsion

    --------------------------------
    -- BEGIN STATE-SPECIFIC LOGIC --
    --------------------------------
    
    if state.current == "init" then
        updateInState_init(dt)
    elseif state.current == "evmode" then
        updateInState_evmode(dt)
    elseif state.current == "chargemode" then
        updateInState_chargemode(dt)
    elseif state.current == "stopped" then
        updateInState_stopped(dt)
    elseif state.current == "startup" then
        updateInState_startup(dt)
    elseif state.current == "running" then
        updateInState_running(dt)
    elseif state.current == "shutdown" then
        updateInState_shutdown(dt)
    elseif state.current == "poweroff" then
        updateInState_poweroff(dt)
    end
    
    state.last = state.current
    
    debugutil:putValue("Power", M.powerOn and "On" or "Off")
    debugutil:putValue("Coolant Temp.", electrics.values.watertemp)
    debugutil:putValue("Engine demanded on", tostring(engineDemand.isDemanded))
    debugutil:putValue("Engine on reason", engineDemand.reason)
    debugutil:putValue("Engine triggered on by", engineDemand.triggerReason)
    debugutil:putValue("State", state.current)
    
    if engine then
        debugutil:putValue("Combustion Torque", engine.combustionTorque)
    end
end

local function updateValues(dt)
    if not gearbox or not battery then
        return
    end
    
    local throttleAggressivenessInst = (input.throttle - state.lastThrottle) / dt
    
    -- update engineDemand
    engineDemand.isDemanded = isEngineDemandedOn() -- Have to do this before resetting miscDemand in next two lines, in case miscDemand was set last tick
    engineDemand.miscDemandReason = "none"
    engineDemand.miscDemand = false
    
    -- update engineState
    engineState.rpm = M.actualEngineRpmSmoother:get(electrics.values.rpm)
    
    -- update custom electrics
    electrics.values.batteryLevel = power.batteryLevel
    electrics.values.kw = gearbox.motorGenerator2Power - min(0, gearbox.motorGenerator1Power + power.requestedChargePower) -- propulsion power kw
    
    -- update state
    state.lastThrottle = input.throttle
    state.isInDriveGear = state.gear ~= "P" and state.gear ~= "N"
    state.isParked = state.speedMph < 0.5 and (state.gear == "P" or electrics.values.parkingbrake == 1)
    state.isStopped = state.speedMph < 0.5 and state.brakeInput > 0.1
    state.launchControl = (state.brakeInput >= 0.25 or electrics.values.parkingbrake == 1) and state.throttle > 0.02
    state.engineOn = engine and not (engine.isStalled or engine.isDisabled or engine.ignitionCoef < 0.01 or electrics.values.rpm < engine.idleRPM / 2)
    state.watertemp = electrics.values.watertemp
    state.speedMph = M.vehicleSpeedSmoother:get(electrics.values.wheelspeed * constants.mpsToMph, dt)
    state.gear = electrics.values.gear
    state.mg2TorqueAdjustment = 0
    state.brakeInput = input.brake
    state.throttleAggressiveness = state.throttleAggressiveness + (throttleAggressivenessInst - state.throttleAggressiveness) / 10 -- Rolling average
    
    -- update some throttle stuff
    local throttleRate = maprange(
        abs(state.throttleAggressiveness),
        M.minThrottleAggressivenessRamp,
        M.maxThrottleAggressivenessRamp,
        M.minThrottleAggressivenessFactor,
        M.maxThrottleAggressivenessFactor
    )
    
    if input.throttle < 5e-3 then
        M.accelerationLimiterPid:reset()
    end
    
    -- Calculate some throttle input variables
    local hasThrottleLimit = type(M.maxAcceleration) == "number" and M.maxAcceleration > 0
    local throttleLimit = hasThrottleLimit and M.accelerationLimiterPid:get() or 1 -- limit throttle by acceleration if enabled
    
    state.throttle = electrics.values.throttleOverride or M.throttleSmoother:get(min(throttleLimit, input.throttle), dt * throttleRate)
    
    local throttleOffset
        
    if state.gear == "L" then
        throttleOffset = maprange(braking.maxRegenTorque, 0, braking.uncappedMaxRegenTorque, 0.0001, 0.25)
    else
        throttleOffset = M.evCoastRegen
    end
    
    state.posThrottle = math.pow(maprange(state.throttle, throttleOffset, 1), 1 / M.throttleCurveExponent)
    state.negThrottle = maprange(state.throttle, throttleOffset, 0)

    -- Update engine runtime
    if state.engineOn then
        engineState.runTimeSincePowerOn = engineState.runTimeSincePowerOn + dt
        engineState.timeSinceStart = engineState.timeSinceStart + dt
        
        if state.watertemp >= M.minAutoStopCoolantTemp + 2.5 then
            engineDemand.waterTempDemand = false
        end
    else
        engineState.timeSinceStart = 0
    end
    
    -- Update power-on time
    if M.powerOn then
        state.timeSincePowerOn = state.timeSincePowerOn + dt
    else
        state.timeSincePowerOn = 0
    end
    
    -- Track brake application time for auto-stop purposes
    if M.autoStopBrakeThreshold > 0 and (braking.currentBrakingCoef >= M.autoStopBrakeThreshold or electrics.values.parkingbrake == 1) and state.speedMph <= M.maxEvOnlySpeed then
        braking.timeSinceAppliedBrakes = braking.timeSinceAppliedBrakes + dt
    else
        braking.timeSinceAppliedBrakes = 0
    end
    
    debugutil:putValue("Throttle Rate", throttleRate)
    debugutil:putValue("Engine Run Time", engineState.timeSinceStart)
    debugutil:putValue("Battery SOC % (usable)", math.floor(getProportionalBatteryRatio() * 100))
    debugutil:putValue("Throttle % EV thresh.", math.floor(state.throttle * 100 / M.maxEvOnlyThrottle))
    debugutil:putValue("ICE Ignition Coef", engineState.ignitionCoef)
end

local function updateBattery(dt)
    local mg1Kw = gearbox.motorGenerator1Power
    local mg2Kw = gearbox.motorGenerator2Power
    local rearKw = rearMotor and rearMotor.motorPower or 0
    local idlePowerDraw = M.powerOn and M.idlePowerDraw or 0.0
    
    power.totalPowerOutKw = idlePowerDraw + mg1Kw + mg2Kw + rearKw

    local totalPowerOutKj = power.totalPowerOutKw * dt
    
    -- Apply battery efficiency losses
    if totalPowerOutKj > 1e-20 then
        -- Power coming out of battery; divide by efficiency to pull a bit more
        totalPowerOutKj = totalPowerOutKj / M.regenEfficiency
    elseif totalPowerOutKj < -1e-20 then
        -- Power going into battery; multiply by efficiency to push a bit less
        totalPowerOutKj = totalPowerOutKj * M.regenEfficiency
    end

    debugutil:putValue("Battery power (kW)", power.totalPowerOutKw)
    debugutil:putValue("Generator power (kW)", mg1Kw)

    battery.storedEnergy = mathutil.clamp(battery.storedEnergy - totalPowerOutKj * 1000, 0, battery.initialStoredEnergy)
end

local function updateSounds(dt)
    -- Update sounds
    local mg1Power = abs(gearbox.motorGenerator1Power)
    local mg1AV = abs(gearbox.motorGenerator1AV)
    local mg1InverterPitch = maprange(mg1AV, M.inverterPitchMinSpeedMG1, M.inverterPitchMaxSpeedMG1, M.inverterMinPitchMG1, M.inverterMaxPitchMG1)
    local mg1InverterEffort = min(1, mg1Power / M.inverterMaxVolumeKwMG1)
    local mg1InverterVolume = max(maprange(mg1InverterEffort, 0, 1e-2) * M.inverterMinVolumeMG1, M.inverterBaseVolumeMG1 * maprange(mg1InverterEffort, 1e-2, 1.0))
    
    local mg2Power = abs(gearbox.motorGenerator2Power)
    local mg2AV = abs(gearbox.motorGenerator2AV)
    local mg2InverterPitch = maprange(mg2AV, M.inverterPitchMinSpeedMG2, M.inverterPitchMaxSpeedMG2, M.inverterMinPitchMG2, M.inverterMaxPitchMG2)
    local mg2InverterEffort = min(1, mg2Power / M.inverterMaxVolumeKwMG2)
    local mg2InverterVolume = max(maprange(mg2InverterEffort, 0, 1e-2) * M.inverterMinVolumeMG2, M.inverterBaseVolumeMG2 * maprange(mg2InverterEffort, 1e-2, 1.0))

    obj:setVolumePitch(soundSources.mg1Whine, mg1InverterVolume, M.mg1PitchSmoother:get(mg1InverterPitch, dt))
    obj:setVolumePitch(soundSources.mg2Whine, mg2InverterVolume, M.mg2PitchSmoother:get(mg2InverterPitch, dt))

    local motorPitch = maprange(state.speedMph, M.motorPitchMinSpeed, M.motorPitchMaxSpeed, M.motorMinPitch, M.motorMaxPitch, false)
    local motorVolume = M.motorInverterEffortVolumeCoef * mg2InverterEffort + maprange(state.speedMph, M.motorVolumeMinSpeed, M.motorVolumeMaxSpeed, 0, M.motorBaseVolume)
    
    obj:setVolumePitch(soundSources.motorRamp, motorVolume, motorPitch)
    
    if M.motorHarmonicRatio then
        obj:setVolumePitch(soundSources.motorRampHarmony, motorVolume, M.motorHarmonicRatio * motorPitch)
    else
        obj:setVolumePitch(soundSources.motorRampHarmony, 0, 1)
    end
end

local function updateGUI(dt)
    local maxKw
    
    if battery and battery.jbeamData and (battery.jbeamData.maxPowerOutput or battery.jbeamData.maxPowerInput) then
        local maxOutput = battery.jbeamData.maxPowerOutput or 0
        local maxInput = battery.jbeamData.maxPowerInput or 0
        
        maxKw = max(maxOutput, maxInput)
    else
        maxKw = gearbox and max(gearbox.mg1PowerRating, gearbox.mg2PowerRating) or 0
    end
    
    local mode = "Off"
    
    if M.powerOn then
        if electrics.values.chargeMode > 0 then
            mode = "Charge"
        elseif electrics.values.evMode > 0 then
            mode = "EV"
        else
            mode = "Normal"
        end
    end
    
    if streams.willSend("hybrid") then
        local str = {
            soc = power.batteryLevel,
            kw = power.totalPowerOutKw,
            maxKw = maxKw,
            minSoc = 0.0,
            maxSoc = 1.0,
            powerOn = M.powerOn,
            ev = electrics.values.ev > 0,
            mode = mode,
        }

        gui.send("hybrid", str)
    end
    
    debugutil:sendStreams()
end

local function updateWhileBraking(dt)
    -- Updating while brake pedal is applied
            
    if not state.isInDriveGear or state.gear == "R" then
        -- No regen in P, R, or N
        
        for i = 0, wheels.wheelCount - 1, 1 do
            local wheel = wheels.wheels[i]

            if wheel then
                -- Reset brake split coef
                wheel.brakeInputSplit = initialWheelBrakeSplit[i][1]
                wheel.brakeSplitCoef = initialWheelBrakeSplit[i][2]
            end
        end
        
        state.brake = state.brakeInput
        electrics.values.effectiveBrake = electrics.values.brake

        debugutil:putValue("Friction % of full brakes", math.floor(state.brakeInput * 100))
    else
        -- Blend friction/regen brakes in any other gear
        
        local wheelsSlipping = false
        local preAdjustedFrictionTorque = 0
        local actualFrictionTorque = 0
        local averageWheelSpeed = 0

        for i = 0, wheels.wheelCount - 1, 1 do
            local wheel = wheels.wheels[i]

            if wheel then
                preAdjustedFrictionTorque = preAdjustedFrictionTorque + (state.brakeInput * wheel.brakeTorque)
                actualFrictionTorque = actualFrictionTorque + wheel.desiredBrakingTorque
                wheel.totalTorqueCoef = wheel.brakeTorque / braking.totalPossibleBrakeTorque
                
                if wheel.isPropulsed then
                    wheel.propulsedTorqueCoef = wheel.brakeTorque / braking.totalPropulsedBrakeTorque
                else
                    wheel.unpropulsedTorqueCoef = wheel.brakeTorque / braking.totalUnpropulsedBrakeTorque
                end
                
                averageWheelSpeed = averageWheelSpeed + (wheel.wheelSpeed / wheels.wheelCount)
                
                if abs(wheel.wheelSpeed - braking.lastAverageWheelSpeed) / braking.lastAverageWheelSpeed > M.maxWheelSlipCoefForRegen then
                    wheelsSlipping = true
                end
            end
        end
        
        braking.lastAverageWheelSpeed = averageWheelSpeed

        if braking.disableRegen then
            if not wheelsSlipping then
                braking.timeSinceWheelSlip = braking.timeSinceWheelSlip + dt

                if braking.timeSinceWheelSlip > M.regenWheelslipReenableTime then
                    braking.disableRegen = false
                    braking.timeSinceWheelSlip = 0
                end
            end
        else
            if wheelsSlipping then
                braking.timeSinceWheelSlip = braking.timeSinceWheelSlip + dt

                if braking.timeSinceWheelSlip > M.regenWheelslipDisableTime then
                    braking.disableRegen = true
                    braking.timeSinceWheelSlip = 0
                end
            else
                braking.timeSinceWheelSlip = 0
            end
        end

        -- Combine regen input from brake pedal and throttle pedal (one-pedal driving)
        local regenPercentOfBrakingForce = braking.maxRegenTorque / braking.totalPossibleBrakeTorque
        local regenBrakePedal = maprange(state.brakeInput, 0, regenPercentOfBrakingForce, 0, 1)
        local regenAccelPedal = state.gear == "L" and state.negThrottle or 0
        
        braking.desiredRegen = clamp(regenBrakePedal + regenAccelPedal, 0, 1) * maprange(state.speedMph, 0, M.minRegenFadeSpeed)
        braking.requestedRegenTorque = braking.desiredRegen * braking.maxRegenTorque
        state.brake = min(1, state.brakeInput + regenAccelPedal * regenPercentOfBrakingForce)

        -- Calculate amount of friction brakes needed to cover brake demand
        local requestedBrakingTorqueOnWheels = state.brakeInput * braking.totalPossibleBrakeTorque
        local regenBrakePedalCoef = max(0, (braking.desiredRegen - regenAccelPedal) / braking.desiredRegen)
        local currentRegenTorqueOnWheelsFromBrakePedal = regenBrakePedalCoef * braking.currentRegenTorque
        local regenToTotalTorqueRatio = currentRegenTorqueOnWheelsFromBrakePedal / requestedBrakingTorqueOnWheels
        local neededFrictionBrakingTorque = max(0, requestedBrakingTorqueOnWheels - currentRegenTorqueOnWheelsFromBrakePedal)
        local neededFrictionBrakingCoef = clamp(neededFrictionBrakingTorque / braking.totalPossibleBrakeTorque, 0, 1)
        
        braking.currentBrakingCoef = (actualFrictionTorque + braking.currentRegenTorque) / braking.totalPossibleBrakeTorque

        local adjustedFrictionTorque = 0
        
        -- Apply more friction brakes on wheels without regen to balance them
        for i = 0, wheels.wheelCount - 1, 1 do
            local wd = wheels.wheels[i]
            local thisWheelCurrentTorque = state.brakeInput * wd.brakeTorque
            local thisWheelNeededTorque
            
            wd.brakeInputSplit = 0
            
            if braking.disableRegen then
                -- Reset brake split coef
                wd.brakeInputSplit = initialWheelBrakeSplit[i][1]
                wd.brakeSplitCoef = initialWheelBrakeSplit[i][2]
            else
                if thisWheelCurrentTorque < 1e-30 then
                    wd.brakeSplitCoef = 1
                else
                    if wd.isPropulsed then
                        local thisWheelRegenAdjust = min(braking.currentRegenTorque, braking.totalUnpropulsedBrakeTorque) * wd.propulsedTorqueCoef
                        
                        thisWheelNeededTorque = max(0, neededFrictionBrakingTorque - thisWheelRegenAdjust) * wd.totalTorqueCoef
                    else
                        -- Unpropulsed-only braking until friction brakes = regen brakes
                        local thisWheelRegenBalance = min(braking.currentRegenTorque, neededFrictionBrakingTorque) * wd.unpropulsedTorqueCoef
                        -- Then balanced normally amongst propulsed and unpropulsed
                        local thisWheelNormalBalance = max(0, neededFrictionBrakingTorque - braking.currentRegenTorque) * wd.totalTorqueCoef
                        
                        thisWheelNeededTorque = thisWheelRegenBalance + thisWheelNormalBalance
                    end
                    
                    wd.brakeSplitCoef = clamp(thisWheelNeededTorque / thisWheelCurrentTorque, 0, 1)
                end
            end
            
            adjustedFrictionTorque = adjustedFrictionTorque + state.brakeInput * wd.brakeSplitCoef * wd.brakeTorque
        end
        
        -- Adjust brake input ratio since we may have taken away some brake effort above
        
        electrics.values.brakelights = max(electrics.values.brakelights, 1)
        electrics.values.effectiveBrake = actualFrictionTorque / braking.totalPossibleBrakeTorque

        debugutil:putValue("Actual Regen Torque (Wheels)", braking.currentRegenTorque)
        debugutil:putValue("Requested Braking Torque", requestedBrakingTorqueOnWheels)
        debugutil:putValue("Friction Braking Torque (Req, Act)", fnum(neededFrictionBrakingTorque) .. ", " .. fnum(actualFrictionTorque))
        debugutil:putValue("Total Braking Torque (%)", fnum(braking.currentRegenTorque + preAdjustedFrictionTorque) .. " (" .. fnum(braking.currentBrakingCoef) .. ")")
        debugutil:putValue("Regen % of brake demand", regenToTotalTorqueRatio * 100, 2)
        debugutil:putValue("Friction % of full brakes", neededFrictionBrakingCoef * 100, 2)
        debugutil:putValue("Adjusted friction torque", adjustedFrictionTorque, 2)
        
        --[[ if streams.willSend("profilingData") then
            gui.send("profilingData", {
                -- Blue
                totalBrakeTorque = { title = "Total Brake Torque", color = "#32eaf6", unit = "Nm", value = actualFrictionTorque + currentRegenTorque },
                -- Purple
                requestedBrakeTorque = { title = "Requested Brake Torque", color = "#7700ff", unit = "Nm", value = requestedBrakingTorqueOnWheels },
                -- Green
                regenBrakeTorque = { title = "Regen Brake Torque", color = "#379637", unit = "Nm", value = currentRegenTorque },
                -- Red
                frictionBrakeTorque = { title = "Friction Brake Torque", color = "#f65232", unit = "Nm", value = actualFrictionTorque },
            })
        end ]]
    end
end

local function updateChargePowerWhileDriving(dt)
    if state.chargeModeEnabled then
        power.requestedChargePower = M.idlePowerDraw + M.chargeModePower
    else
        local chargePowerStopped = 0
        local chargePowerDriving = 0        
        
        -- Calculate charge power if stopped
        if M.latchBatteryChargeUp:get() then
            chargePowerStopped = M.idlePowerDraw + max(M.chargeupMinPower, state.isParked and M.maxChargePowerParked or 0.0)
        else
            chargePowerStopped = M.idlePowerDraw + maprange(power.batteryLevel, 0.5, max(0, M.batteryChargeLowRatio), M.minChargePowerParked, M.maxChargePowerParked)
            
            if state.isParked and type(M.parkedChargingMinIdleRpm) == "number" then
                local parkedChargingRpmCoef = maprange(power.requestedChargePower, 0, M.maxChargePowerParked)
                
                engineState.minRpm = max(engineState.minRpm, maprange(parkedChargingRpmCoef, 0, 1, engineState.minRpm, M.parkedChargingMinIdleRpm))
            end
        end
        
        -- Calculate charge power if driving
        local chargePowerThrottleCoef = M.chargePowerCoefVsThrottle:get(state.posThrottle * 100)
        local chargePowerBatteryCoef = M.chargePowerCoefVsBatteryLevel:get(power.batteryLevel * 100)
        local chargePowerRegenCoef = maprange(braking.desiredRegen, 0, 1, 1, 0)
        local chargePowerFactor = state.gear == "R" and 0 or 1
        local maxChargePower = M.maxChargePowerVsSpeed:get(state.speedMph)
        local chargePowerCoef = chargePowerThrottleCoef * chargePowerBatteryCoef * chargePowerRegenCoef
        
        chargePowerDriving = min(chargePowerCoef, chargePowerFactor) * maxChargePower
        
        -- Blend the two together
        power.requestedChargePower = maprange(state.speedMph, 0, M.maxCreepSpeed, chargePowerStopped, chargePowerDriving)
    end
end

local function updateRequestedTorqueInDriveGear(dt)
    -- Update while in a driving gear (anything other than Park or Neutral)
    
    local availablePriorityEvOutputTorque = 0
    local availableSecondaryEvOutputTorque = 0
        
    -- Map throttle to total capable torque output
    torqueData.requestedTotalOutputTorque = state.posThrottle * torqueData.maxCombinedTorque
    
    -- Calculate available EV torque and distribute torque demand between motors and engine
    if state.current == "evmode" then
        local gearboxMaxMG1Torque = gearbox:getMaxMG1Torque(power.availablePriorityEvPower)
        local availableMG1OutputTorque = gearbox.psd:getRingTorqueFromSunTorque(gearboxMaxMG1Torque) * gearbox.counterRingGearRatio * gearbox.cumulativeGearRatio
        local availableMG2OutputTorque = gearbox:getMaxMG2Torque(power.availablePriorityEvPower) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
        
        availablePriorityEvOutputTorque = availableMG1OutputTorque + availableMG2OutputTorque
        availableSecondaryEvOutputTorque = 0
    else
        availablePriorityEvOutputTorque = gearbox:getMaxMG2Torque(power.availablePriorityEvPower) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
        availableSecondaryEvOutputTorque = gearbox:getMaxMG2Torque(max(0, power.availableSecondaryEvPower - power.requestedChargePower)) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
    end
    
    local torqueStillNeeded = torqueData.requestedTotalOutputTorque * gearbox.throttleFactor -- TCS will set this
    local creepThrottle = 0
    
    if state.brakeInput < 0.25 and electrics.values.parkingbrake < 1 and state.gear ~= "P" and state.gear ~= "N" then
        creepThrottle = maprange(state.speedMph, 0, M.maxCreepSpeed, M.evCreepThrottle, 0)
    end
    
    local creepTorque = creepThrottle * torqueData.maxElectricTorque
    
    if state.current == "running" then
        -- Only apply torque for creep at first (electric torque will be applied below)
        
        torqueData.requestedElectricOutputTorque = creepTorque
    else
        -- Consume available primary EV power first (from whatever is available from battery)
        
        torqueData.requestedElectricOutputTorque = min(torqueStillNeeded, availablePriorityEvOutputTorque) + creepTorque
    end
    
    -- Have to adjust for creepTorque here so as not to confuse the following calculations
    torqueStillNeeded = torqueStillNeeded + creepTorque - torqueData.requestedElectricOutputTorque
    
    -- If we would use too much secondary EV power, start the ICE (or keep it on)
    local secondaryPowerDemand = mathutil.torqueToKw(torqueStillNeeded, gearbox.motorGenerator2AV)
        
    if secondaryPowerDemand > M.maxEvOnlySecondaryPower then
        requestEngine("secondary power exceeded")
    end
    
    if state.current == "startup" or state.current == "running" then
        -- Try and distribute torque demand to engine in "balanced mode" where the power consumption of MG2
        -- equals the power generated by MG1
        
        local tO   -- Desired output torque
        local rI   -- Input gear ratio (carrier : engine)
        local rRC  -- Ring:Carrier gear ratio
        local rSC  -- Sun:Carrier gear ratio
        local rFR  -- Final:Ring gear ratio
        local rF2  -- Final:MG2 gear ratio
        local av1  -- Signed angular velocity of MG1
        local av2  -- Abs angular velocity of MG2
        local pP   -- Max priority power available to achieve requested torque w/ electric power
        local dP   -- Desired battery power delta (+ is drain, - is charge)
        local tE   -- Desired engine torque (raw, into gearbox)
        local t2   -- Desired MG2 torque
        
        -- Set up variable values
        tO = max(0, torqueStillNeeded)
        rI = gearbox.inputGearRatio
        rRC = gearbox.psd.ringCarrierRatio
        rSC = gearbox.psd.sunCarrierRatio
        rFR = gearbox.counterRingGearRatio * gearbox.cumulativeGearRatio
        rF2 = gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
        av1 = gearbox.motorGenerator1AV
        av2 = max(1e-30, abs(gearbox.motorGenerator2AV))
        pP = min(torqueStillNeeded, availablePriorityEvOutputTorque) / gearbox.cumulativeGearRatio / gearbox.counterMG2GearRatio * av2
        dP = pP - (power.requestedChargePower * 1000)
        
        -- warning: super ugly equations below
        -- tE and t2 equations are derived from the following:
        --   Net output torque: tO = tE * rI * rRC * rFR + t2 * rF2
        --   Net power delta:   dP = t2 * av2 - tE * rI * rSC * av1
        
        -- Perform initial calculations without clamping
        tE = (tO - rF2 * dP / av2) / (rI * rRC * rFR + ((rI * rSC * rF2 * av1) / av2))
        t2 = (dP + tE * rI * rSC * av1) / av2
        
        -- Clamp t2 to our known bounds and recalculate tE, in case MG2 runs out of torque
        -- △tE can be recalculated using another derivation from above:
        --   △tE = (△tO - △t2 * rF2) / (rI * rRC * rFR) where △t2 in this case is the delta between clamped and unclamped
        --   (△tO is always zero; we want tO to remain the same)
        local clampedT2 = clamp(t2, -torqueData.gearboxMaxMG2TorqueUnchecked, torqueData.gearboxMaxMG2Torque)
        local t2Delta = min(0, clampedT2 - t2)
        local tEDelta = (-t2Delta * rF2) / (rI * rRC * rFR)
        
        tE = tE + tEDelta
        t2 = clampedT2
        
        local engineOutputTorque = rI * rRC * rFR * tE
        local electricOutputTorque = rF2 * t2
        
        torqueData.requestedEngineOutputTorque = min(torqueData.maxEngineTorque, engineOutputTorque)
        
        if state.current == "running" then -- this whole block can run during startup, but don't adjust electric power unless the engine's actually fully running
            torqueData.requestedElectricOutputTorque = torqueData.requestedElectricOutputTorque + electricOutputTorque
        end
        
        torqueStillNeeded = max(0, torqueStillNeeded - (torqueData.requestedEngineOutputTorque + torqueData.requestedElectricOutputTorque))
        
        debugutil:putValue("###### tO", tO)
        debugutil:putValue("###### rI", rI)
        debugutil:putValue("###### rRC", rRC)
        debugutil:putValue("###### rSC", rSC)
        debugutil:putValue("###### rFR", rFR)
        debugutil:putValue("###### rF2", rF2)
        debugutil:putValue("###### av1", av1)
        debugutil:putValue("###### av2", av2)
        debugutil:putValue("###### pP", pP)
        debugutil:putValue("###### dP", dP)
        debugutil:putValue("###### tE", tE)
        debugutil:putValue("###### t2", t2)
    end
    
    -- While the engine is working on revving up, the main drive motor should compensate for it
    -- (using secondary power) so that the driver feels a more immediate acceleration response.
    -- It's okay if we do this even if the battery is low (it's a short period of time it will be drawing power)
    -- By using "actualEngineOutputTorque" and subtracting it from torqueStillNeeded, secondary power
    -- will automatically kick in for this if the engine is producing less than expected
    local actualEngineOutputTorque = max(0, torqueData.actualEngineTorqueRing) * gearbox.counterRingGearRatio * gearbox.cumulativeGearRatio
    
    if M.engineLagCompensationEnabled then
        local engineTorqueCompensation = max(0, torqueData.requestedEngineOutputTorque - actualEngineOutputTorque)
        
        if type(M.maxEngineLagCompensationTorque) == "number" then
            engineTorqueCompensation = min(engineTorqueCompensation, M.maxEngineLagCompensationTorque)
        end
        
        -- Use priority power for compensation
        torqueData.requestedElectricOutputTorque = torqueData.requestedElectricOutputTorque + min(engineTorqueCompensation, availableSecondaryEvOutputTorque)
        
        debugutil:putValue("Engine Torque Compensation", engineTorqueCompensation)
    end
    
    -- Distribute as much remaining torque demand as we can to secondary EV power
    local secondaryTorqueApplied = min(torqueStillNeeded, availableSecondaryEvOutputTorque)
    
    torqueData.requestedElectricOutputTorque = min(torqueData.requestedElectricOutputTorque + secondaryTorqueApplied, torqueData.maxElectricTorque)
    torqueStillNeeded = torqueStillNeeded - min(torqueStillNeeded, availableSecondaryEvOutputTorque)
    
    -- Make sure engine starts if we're out of electric power
    if state.gear ~= "R" and (torqueStillNeeded > 1e-20 or torqueData.requestedElectricOutputTorque > torqueData.maxElectricTorque) then
        requestEngine("electric torque exhausted")
    end
    
    if state.launchControl then
        torqueData.requestedElectricOutputTorque = min(M.maxLaunchControlTorque, torqueData.requestedElectricOutputTorque)
    end
    
    debugutil:putValue("Available EV Torque (Priority/Secondary)", fnum(availablePriorityEvOutputTorque) .. " / " .. fnum(availableSecondaryEvOutputTorque))
end

local function updateTargetEngineRpm(dt)
    if not engine then return end
    
    local minTargetRpm = engineState.minRpm
    local maxTargetRpm = engineState.maxRpm
    
    if not state.isInDriveGear or state.launchControl then
        -- Apply a direct link between throttle pedal and minimum RPM
        
        minTargetRpm = maprange(state.posThrottle, 0, 1, minTargetRpm, maxTargetRpm)
    end
    
    if M.useEngineTorqueCurve then
        -- Use auto RPM calc; "engineRpmVsTorque" is a minimum, if present
        
        if M.engineRpmVsTorque then
            local minimumRpm = M.engineRpmVsTorque:get(torqueData.requestedEngineTorque)
            
            minTargetRpm = max(minTargetRpm, minimumRpm)
        end
        
        engineState.targetRpm = clamp(engineutil.getBestRpmForTorque(engine, torqueData.requestedEngineTorque), minTargetRpm, maxTargetRpm)
    else
        engineState.targetRpm = clamp(M.engineRpmVsTorque:get(torqueData.requestedEngineTorque), minTargetRpm, maxTargetRpm)
    end
    
    debugutil:putValue("Min Target RPM", minTargetRpm)
    debugutil:putValue("Max Target RPM", maxTargetRpm)
end

local function updateEngineLoadTorque(dt)
    local smoothedTargetEngineRpm = M.targetEngineRpmSmoother:get(engineState.targetRpm, dt)
    local loadTorque = 0
    local loadBiasTorque = 0
    
    if engine and state.current == "running" then
        local rpmError = smoothedTargetEngineRpm - engineState.rpm
        local rpmErrorCoef = clamp(rpmError / (rpmError > 0 and M.engineLoadRpmToleranceOut or M.engineLoadRpmToleranceIn), -1, 1)
        local loadBiasCoef = rpmError > 0 and (state.ecvtDriveMode == 1 and M.engineLoadBiasCoefOutModeOne or M.engineLoadBiasCoefOut) or M.engineLoadBiasCoefIn
        local engineStartupCoef = M.engineStartupLoadSmoother:get(1, dt)
        
        loadBiasTorque = torqueData.maxEngineGeneratorTorque * loadBiasCoef * rpmErrorCoef
        loadTorque = engineStartupCoef * torqueData.requestedEngineTorqueSun
        
        if state.ecvtDriveMode == 1 then
            loadBiasTorque = max(0, loadBiasTorque)
        end
        
        debugutil:putValue("gearboxMaxMG1TorqueUnchecked", torqueData.gearboxMaxMG1TorqueUnchecked)
        
        -- Calculate engine deloading
        local maxDeloadCoefChargePower = maprange(power.requestedChargePower, 5, 0)
        local maxDeloadCoefSpeed = maprange(state.speedMph, 0, M.maxCreepSpeed)
        local maxDeloadCoefEngineTorque = maprange(torqueData.requestedEngineTorqueSun, 5, 1e-2)
        local maxDeloadCoef = maxDeloadCoefChargePower * maxDeloadCoefSpeed * maxDeloadCoefEngineTorque
        local deloadCoef = maxDeloadCoef * M.engineDeloadMaxIgnitionCut
        local targetOutputTorqueState
        
        if deloadCoef > 1e-30 then
            M.driveThrottlePid:reset(true)
            engineState.throttleOverride = 0
            targetOutputTorqueState = (1 - deloadCoef) * engineState.initialOutputTorqueState
        else
            engineState.throttleOverride = nil
            targetOutputTorqueState = engineState.initialOutputTorqueState
        end
        
        engine.outputTorqueState = M.engineDeloadSmoother:get(targetOutputTorqueState, dt)
        
        -- Limit engine load based on certain criteria
        local maxEngineLoadStall = maprange(electrics.values.rpm, engine.idleRPM - M.engineLoadRpmToleranceOut, engine.idleRPM)
        local maxEngineLoadBattery = state.ecvtDriveMode == 1 and 1 or maprange(power.batteryLevelRaw, 1, 0.98)
        
        if loadTorque > 0 then
            loadTorque = loadTorque * min(maxEngineLoadStall, maxEngineLoadBattery)
        end
    
        debugutil:putValue("RPM Error", rpmError)
    else
        M.engineLoadSmoother:reset()
        M.engineDeloadSmoother:reset()
        engineState.throttleOverride = nil
        engine.outputTorqueState = engineState.initialOutputTorqueState
    end
    
    -- engineState.loadTorque = M.engineLoadSmoother:get(loadTorque, dt) - loadBiasTorque
    engineState.loadTorque = loadTorque - loadBiasTorque
    
    debugutil:putValue("Target RPM (Raw, Smooth)", fnum(engineState.targetRpm, 0) .. ", " .. fnum(smoothedTargetEngineRpm, 0))
    debugutil:putValue("Load Bias", loadBiasTorque)
    debugutil:putValue("Engine Load Torque", loadTorque)
end

local function updateEngineThrottle(dt)
    if state.current == "stopped" or state.current == "evmode" or not M.powerOn then
        M.driveThrottlePid:reset()
    else
        -- Engine throttle is controlled by the desired output torque of the engine
        -- Engine RPM is controlled by the throttle pedal
        -- Actual load on the engine is biased to achieve the desired RPM
        engineState.targetThrottle = M.driveThrottlePid:get(M.targetEngineRpmSmoother:value(), engineState.rpm, dt)
    end
end

local function updateGearboxWhileDriving(dt)
    if state.current ~= "running" then
        state.ecvtDriveMode = 0
        M.latchDriveModeZero:reset()
        M.latchDriveModeOne:reset()
    end
    
    -- Determine MG2 adjustment/compensation values
    if engine and state.current == "startup" and state.isInDriveGear then
        local currentStarterTorque = max(0, gearbox.motorGenerator1Torque)
        local starterTorqueRingCompensation = gearbox.psd:getRingTorqueFromSunTorque(currentStarterTorque)
        local starterTorqueFinalCompensation = starterTorqueRingCompensation * gearbox.counterRingGearRatio
        local starterTorqueMG2Compensation = starterTorqueFinalCompensation / gearbox.counterMG2GearRatio
        
        state.mg2TorqueAdjustment = starterTorqueMG2Compensation
    end
    
    if state.current == "evmode" then
        -- Use both motors to achieve the requested torque
        
        local maxMG2TorqueToOutput = torqueData.gearboxMaxMG2Torque * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
        local neededMG2TorqueToOutput = min(torqueData.requestedElectricOutputTorque, maxMG2TorqueToOutput)
        local neededMG1CompensationToOutput = state.gear == "R" and 0 or max(0, torqueData.requestedElectricOutputTorque - maxMG2TorqueToOutput)
        local neededMG2Torque = (abs(neededMG2TorqueToOutput) - braking.requestedRegenTorque) / gearbox.cumulativeGearRatio / gearbox.counterMG2GearRatio
        local neededMG1Torque = abs(gearbox.psd:getSunTorqueFromRingTorque(neededMG1CompensationToOutput / gearbox.cumulativeGearRatio / gearbox.counterRingGearRatio))
        
        if type(M.maxEvModeMG1Torque) == "number" then
            neededMG1Torque = min(M.maxEvModeMG1Torque, neededMG1Torque)
        end
        
        torqueData.targetMG2Torque = neededMG2Torque
        torqueData.targetMG1Torque = neededMG1Torque
        torqueData.targetMG1Direction = -1
    else
        local neededMG2Torque = (torqueData.requestedElectricOutputTorque - braking.requestedRegenTorque) / gearbox.cumulativeGearRatio / gearbox.counterMG2GearRatio
        
        if state.ecvtDriveMode == 0 then
            -- engine load can be achieved using gen drag on MG1
            -- charging is done by adding load to the engine
            
            torqueData.targetMG1Torque = -engineState.loadTorque
            torqueData.targetMG1Direction = (state.current == "startup" or state.current == "running") and 1 or -1 -- Only forward when engine running; otherwise backwards to spooldown engine
            torqueData.targetMG2Torque = neededMG2Torque + state.mg2TorqueAdjustment
        else
            -- MG1 spinning too slow (or backwards) for gen drag; use negative power instead
            -- in this case, we have to regen off of MG2 to provide power for MG1
            
            torqueData.targetMG1Torque = abs(engineState.loadTorque)
            torqueData.targetMG1Direction = -sign(engineState.loadTorque)
            torqueData.targetMG2Torque = neededMG2Torque + state.mg2TorqueAdjustment
        end
    end
    
    if state.current ~= "startup" and state.current ~= "running" and engineState.rpm >= 20 then
        -- Shut down the engine faster by applying load to it as it spins down
        torqueData.targetMG1Torque = torqueData.targetMG1Torque + getEngineShutdownTorque()
    end
    
    if state.current == "evmode" then
        state.ecvtDriveMode = 0
    else
        -- Determine which drive mode we should be in next based on whether or not MG1 can provide generation drag
        local modeZeroLatchTest = state.current ~= "running"
            or gearbox.psd.sunGearAV > gearbox.generatorMinFullLoadAV + 50
            or engineState.loadTorque <= 1e-2
        local modeOneLatchTest = state.current == "running" 
            and gearbox.psd.sunGearAV <= gearbox.generatorMinFullLoadAV
            and engineState.loadTorque > 1e-2
        
        if M.latchDriveModeOne:test(modeOneLatchTest, dt) then
            if state.ecvtDriveMode == 0 then
                M.latchDriveModeZero:reset()
                state.ecvtDriveMode = 1
            end
        end
        if M.latchDriveModeZero:test(modeZeroLatchTest, dt) then
            if state.ecvtDriveMode == 1 then
                M.latchDriveModeOne:reset()
                state.ecvtDriveMode = 0
            end
        end
    end
end

local function updateRequestedTorqueGlobal(dt)
    -- apply a final cap to both torques to make sure they don't independently break the max output torque
    torqueData.requestedElectricOutputTorque = min(torqueData.maxElectricTorque, torqueData.requestedElectricOutputTorque)
    torqueData.requestedEngineOutputTorque = min(torqueData.maxEngineTorque, torqueData.requestedEngineOutputTorque)
    
    -- Calculate requested engine torque at the different parts of the gearbox
    local directChargeTorqueSun = state.gear == "P" and mathutil.kwToTorque(power.requestedChargePower / M.regenEfficiency, gearbox.motorGenerator1AV) or 0 -- Only directly apply charge torque in Park
    local directChargeTorqueCarrier = abs(gearbox.psd:getCarrierTorqueFromSunTorque(directChargeTorqueSun))
    
    torqueData.requestedEngineTorqueCarrier = directChargeTorqueCarrier + abs(gearbox.psd:getCarrierTorqueFromRingTorque(torqueData.requestedEngineOutputTorque / gearbox.cumulativeGearRatio / gearbox.counterRingGearRatio))
    torqueData.requestedEngineTorqueSun = abs(gearbox.psd:getSunTorqueFromCarrierTorque(torqueData.requestedEngineTorqueCarrier))
    torqueData.requestedEngineTorqueRing = abs(gearbox.psd:getRingTorqueFromCarrierTorque(torqueData.requestedEngineTorqueCarrier))
    torqueData.requestedEngineTorque = torqueData.requestedEngineTorqueCarrier / gearbox.inputGearRatio
    
    debugutil:putValue("Achievable Engine Torque (Eng, S, C, R)", fnum(torqueData.achievableEngineTorque, 0) .. ", " ..
                                                                    fnum(torqueData.achievableEngineTorqueSun, 0) .. ", " ..
                                                                    fnum(torqueData.achievableEngineTorqueCarrier, 0) .. ", " ..
                                                                    fnum(torqueData.achievableEngineTorqueRing, 0))
    debugutil:putValue("Requested Engine Torque (Eng, S, C, R)", fnum(torqueData.requestedEngineTorque, 0) .. ", " ..
                                                                    fnum(torqueData.requestedEngineTorqueSun, 0) .. ", " ..
                                                                    fnum(torqueData.requestedEngineTorqueCarrier, 0) .. ", " ..
                                                                    fnum(torqueData.requestedEngineTorqueRing, 0))
    debugutil:putValue("Actual Engine Torque (Eng, S, C, R)", fnum(torqueData.actualEngineTorque, 0) .. ", " ..
                                                                fnum(torqueData.actualEngineTorqueSun, 0) .. ", " ..
                                                                fnum(torqueData.actualEngineTorqueCarrier, 0) .. ", " ..
                                                                fnum(torqueData.actualEngineTorqueRing, 0))
end

local function updateHybridLogic(dt)
    -- TOO MANY CALCULATIONS, SEND HELP
            
    -- Reset torque request variables
    torqueData.requestedTotalOutputTorque = 0
    torqueData.requestedElectricOutputTorque = 0
    torqueData.requestedEngineOutputTorque = 0
    
    -- Calculate RPM range for engine based on required sun gear speeds
    engineState.minWorkingRpm = maprange(state.speedMph, 0, M.maxCreepSpeed, M.engineMinIdleRpm, engine.idleRPM)
    engineState.minRpm = max(engineState.minWorkingRpm, (gearbox:getMinCarrierAV() * gearbox.inputGearRatio) * constants.avToRPM)
    engineState.maxRpm = min(M.engineMaxRpm, (gearbox:getMaxCarrierAV() * gearbox.inputGearRatio) * constants.avToRPM)
    engineState.maxRpm = max(engineState.minRpm, engineState.maxRpm)
    
    -- Figure out max possible engine torque at its current RPM, as well as the torque it's currently
    -- applying on\the different transmission components
    torqueData.achievableEngineTorque = engine and engineutil.getAchievableEngineTorque(engine) or 0
    torqueData.achievableEngineTorqueCarrier = torqueData.achievableEngineTorque * gearbox.inputGearRatio
    torqueData.achievableEngineTorqueSun = abs(gearbox.psd:getSunTorqueFromCarrierTorque(torqueData.achievableEngineTorqueCarrier))
    torqueData.achievableEngineTorqueRing = abs(gearbox.psd:getRingTorqueFromCarrierTorque(torqueData.achievableEngineTorqueCarrier))
    torqueData.actualEngineTorque = max(0, engine and engine.outputTorque1 or 0)
    torqueData.actualEngineTorqueCarrier = torqueData.actualEngineTorque * gearbox.inputGearRatio
    torqueData.actualEngineTorqueRing = abs(gearbox.psd:getRingTorqueFromCarrierTorque(torqueData.actualEngineTorqueCarrier))
    torqueData.actualEngineTorqueSun = abs(gearbox.psd:getSunTorqueFromCarrierTorque(torqueData.actualEngineTorqueCarrier))
    
    if state.current == "running" then
        updateChargePowerWhileDriving(dt)
    else
        power.requestedChargePower = 0
    end
    
    if state == "poweroff" then
        torqueData.requestedElectricOutputTorque = 0
        torqueData.requestedEngineOutputTorque = 0
    else
        if state.isInDriveGear then
            updateRequestedTorqueInDriveGear(dt)
        
            if state.gear ~= "R" and not engineDemand.miscDemand then
                if state.throttle > M.maxEvOnlyThrottle then
                    requestEngine("ev throttle limit exceeded")
                end
                if state.launchControl then
                    requestEngine("launch control")
                end
            end
        else
            torqueData.requestedElectricOutputTorque = 0
            torqueData.requestedEngineOutputTorque = maprange(state.posThrottle, M.evCreepThrottle, 1, 0, M.maxParkEngineTorqueCoef * torqueData.maxEngineTorque)
            
            if state.throttle > 0.02 then
                requestEngine("throttle in park")
            end
        end
        
        updateRequestedTorqueGlobal(dt)
        updateTargetEngineRpm(dt)
        updateEngineLoadTorque(dt)
        updateEngineThrottle(dt)
    end
    
    -- Calculate actual torques
    local actualTorque = gearbox.rawOutputTorque * gearbox.cumulativeGearRatio
    local actualTorqueEV = gearbox.motorGenerator2Torque * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
    local actualTorqueICE = -gearbox.psd:getRingTorqueFromCarrierTorque(torqueData.actualEngineTorqueCarrier) * gearbox.counterRingGearRatio * gearbox.cumulativeGearRatio
    
    if rearMotor then
        local rearMotorOutputTorque = rearMotor.motorTorque * rearMotor.cumulativeGearRatio
        
        actualTorque = actualTorque + rearMotorOutputTorque
        
        debugutil:putValue("Actual Torque (Total, EV F, EV R, ICE)", fnum(actualTorque) .. ", " .. fnum(actualTorqueEV) .. ", " .. fnum(rearMotorOutputTorque) .. ", " .. fnum(actualTorqueICE))
    else
        debugutil:putValue("Actual Torque (Total, EV, ICE)", fnum(actualTorque) .. ", " .. fnum(actualTorqueEV) .. ", " .. fnum(actualTorqueICE))
    end
    
    updateGearboxWhileDriving(dt)
    
    debugutil:putValue("Requested Torque (Total, EV, ICE)", fnum(torqueData.requestedTotalOutputTorque) .. ", " .. fnum(torqueData.requestedElectricOutputTorque) .. ", " .. fnum(torqueData.requestedEngineOutputTorque))
    debugutil:putValue("Desired ICE Throttle", engineState.targetThrottle)
    debugutil:putValue("Max Regen Torque (Wheels)", braking.maxRegenTorque)
    debugutil:putValue("Requested Regen Torque (Wheels)", braking.requestedRegenTorque)
    debugutil:putValue("Min RPM (Working/Actual)", fnum(engineState.minWorkingRpm) .. " / " .. fnum(engineState.minRpm))
    debugutil:putValue("Max RPM", engineState.maxRpm)
    debugutil:putValue("Actual RPM", engineState.rpm)
    debugutil:putValue("eCVT Drive Mode", state.ecvtDriveMode)
    debugutil:putValue("Available Power (Priority/Secondary)", fnum(power.availablePriorityEvPower) .. " / " .. fnum(power.availableSecondaryEvPower))
    debugutil:putValue("Requested Charge Power", fnum(power.requestedChargePower) .. " kW")
end

local function updateMotors(dt)
    local mg1PowerGenerated = -min(0, gearbox.motorGenerator1Power)
    local mg2PowerGenerated = -min(0, gearbox.motorGenerator2Power)
    
    -- Calculate motor power restrictions
    power.availableMG1Power = M.mg1MaxPowerSmoother:get(min(gearbox.mg1PowerRating, power.availableBatteryPower + mg2PowerGenerated), dt)
    power.availableMG2Power = M.mg2MaxPowerSmoother:get(min(gearbox.mg2PowerRating, power.availableBatteryPower + mg1PowerGenerated), dt)
    
    -- Hard-limit the amount of power MG1 and MG2 are allowed to consume (so they don't overdraw the battery)
    gearbox:setMG1PowerOverride(power.availableMG1Power)
    gearbox:setMG2PowerOverride(power.availableMG2Power)
    
    debugutil:putValue("Available MG1 Power", power.availableMG1Power, 2)
    debugutil:putValue("Available MG2 Power", power.availableMG2Power, 2)
    
    -- If rear motor is present, it has the same max power as MG2
    if rearMotor then
        rearMotor:setMotorPowerOverride(min(rearMotor.motorPowerRating, power.availableMG2Power))
    end
    
    -- Update AWD if present
    if rearMotor then
        local netOutputTorque = torqueData.requestedTotalOutputTorque - braking.requestedRegenTorque
        local wheelSpeedAggregator = netOutputTorque < 0 and max or min
        local effectiveWheelSpeed
        
        for i = 0, wheels.wheelCount - 1, 1 do
            local wd = wheels.wheels[i]

            if wd then
                effectiveWheelSpeed = wheelSpeedAggregator(effectiveWheelSpeed or wd.wheelSpeed, wd.wheelSpeed)
            end
        end
        
        local effectiveWheelSpeedMph = effectiveWheelSpeed * constants.mpsToMph
        local maxTorqueSplit = netOutputTorque >= 0 and M.maxRearTorqueSplitVsSpeed:get(effectiveWheelSpeedMph) or M.maxRearRegenTorqueSplitVsSpeed:get(effectiveWheelSpeedMph)
        local torqueSign = sign(netOutputTorque)
        local desiredRearOutputTorque = maxTorqueSplit * abs(netOutputTorque)
        local maxRearOutputTorque = rearMotor:getMaxMotorTorque(torqueSign < 0) * rearMotor.cumulativeGearRatio
        local maxOutputTorqueToTakeFromMG2 = abs(torqueData.targetMG2Torque) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
        local maxTorqueToMove = min(maxRearOutputTorque, maxOutputTorqueToTakeFromMG2)
        local torqueToMove = min(desiredRearOutputTorque, maxTorqueToMove)
        local rawTorqueToTakeFromMG2 = torqueSign * torqueToMove / gearbox.cumulativeGearRatio / gearbox.counterMG2GearRatio
        local rawTorqueToSendToRear = torqueSign * torqueToMove / rearMotor.cumulativeGearRatio
        
        torqueData.targetMG2Torque = torqueData.targetMG2Torque - rawTorqueToTakeFromMG2
        torqueData.targetRearMotorTorque = torqueData.targetRearMotorTorque + rawTorqueToSendToRear
        
        debugutil:putValue("Wheel Speed (Avg/Effective, MPH)", fnum(state.speedMph) .. ", " .. fnum(effectiveWheelSpeedMph))
    end
    
    -- Update requested torques
    gearbox:setMG1TorqueDirection(torqueData.targetMG1Direction)
    gearbox:setRequestedMG1Torque(torqueData.targetMG1Torque)
    gearbox:setRequestedMG2Torque(torqueData.targetMG2Torque)
    
    if rearMotor then
        rearMotor:setRequestedOutputTorque(torqueData.targetRearMotorTorque)
        rearMotor:setMotorDirection(state.gear == "R" and -1 or 1)
    end
end

local function update(dt)
    if M.useAccelLimiter then
        -- Update acceleration
        state.currentAcceleration = M.accelerationSmoother:get(-obj:getSensorY(), dt)
        
        -- Update acceleration limit
        local accelLimit = (electrics.values.throttleOverride or input.throttle) * M.maxAcceleration
        
        M.accelerationLimiterPid:update(accelLimit, state.currentAcceleration, dt)
    end
end

local function updateGFX(dt)
    engineState.targetThrottle = 0
    
    updateDefaultDebugValues()
    updateValues(dt)
    calculateElectricLimits(dt)
    calculateTorqueStatistics(dt)
    
    -- Once we know we have all the required components for running,
    -- we can start to perform our (very complex and abundant) hybrid
    -- calculations
    
    -- Calculate some rudimentary locals
    local actualSpeedSign = mathutil.sign(electrics.values.avgWheelAV)
    local desiredSpeedSign = state.gear == "R" and -1 or 1
    
    -- If there is a maximum power input on the battery, we can't exceed it while regenerating
    -- Instead, we have to figure out the maximum regen coefficient that we can apply without
    -- exceeding the battery's max power input
    local canRegen = not (
        state.current == "poweroff" or
        state.current == "shutdown" or
        braking.disableRegen or
        state.speedMph < M.minRegenFadeSpeed
    )
    
    braking.maxRegenTorque = canRegen and min(M.maxRegenTorque, torqueData.gearboxMaxMG2TorqueUnchecked * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio) or 0
    braking.uncappedMaxRegenTorque = braking.maxRegenTorque
    
    if battery.jbeamData.maxPowerInput then
        local batteryPowerInputCoef = maprange(power.batteryLevelRaw, 0.9, 0.95, 1, 0)
        local batteryPowerInputAvailable = battery.jbeamData.maxPowerInput * batteryPowerInputCoef - engineState.powerGenerated
        
        if state.chargeModeEnabled then
            batteryPowerInputAvailable = batteryPowerInputAvailable - M.chargeModePower
        end
        
        -- Calculate what percentage of regen we can use, taking into account
        -- the maximum amount of power the battery can currently accept, minus
        -- however much power the engine is putting into the battery
        local mg2RegenTorque = max(0, mathutil.kwToTorque(batteryPowerInputAvailable, gearbox.motorGenerator2AV) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio)
        
        braking.maxRegenTorque = min(braking.maxRegenTorque, mg2RegenTorque)
        
        if rearMotor then
            local rearRegenTorque = max(0, mathutil.kwToTorque(batteryPowerInputAvailable, rearMotor.motorAV) * rearMotor.cumulativeGearRatio)
            
            braking.maxRegenTorque = min(braking.maxRegenTorque, rearRegenTorque)
        end
    end
    
    if state.speedMph < M.minRegenFadeSpeed then
        braking.currentRegenTorque = 0
    else
        local regenTorqueFromMG2 = max(0, gearbox.motorGenerator2Torque * -actualSpeedSign) * gearbox.counterMG2GearRatio * gearbox.cumulativeGearRatio
        local regenTorqueFromRear = 0
        
        if rearMotor then
            regenTorqueFromRear = max(0, rearMotor.outputTorque1 * -actualSpeedSign) * rearMotor.cumulativeGearRatio
        end
        
        braking.currentRegenTorque = regenTorqueFromMG2 + regenTorqueFromRear
    end
    
    -- Apply hill assist
    local hillAssistAngleCoef = maprange(obj:getDirectionVector().z * desiredSpeedSign, M.minHillAssistPitch, M.maxHillAssistPitch)
    local hillAssistSpeedCoef = maprange(state.speedMph * actualSpeedSign * desiredSpeedSign, 1e-10, 0.05, 1, 0)
    local hillAssistTorqueCoef = maprange(abs(gearbox.outputTorque1), 0, torqueData.maxCombinedTorque, 1, 0)
    local hillAssistBrakeHold = hillAssistAngleCoef * hillAssistSpeedCoef * hillAssistTorqueCoef * M.hillAssistBrakeHoldAmount
    
    if state.isInDriveGear then
        electrics.values.brake = max(electrics.values.brake, hillAssistBrakeHold)
    end

    if state.brakeInput > 0 then
        updateWhileBraking(dt)
    else
        braking.disableRegen = false
        braking.currentBrakingCoef = braking.currentRegenTorque / braking.totalPossibleBrakeTorque
        electrics.values.effectiveBrake = electrics.values.brake

        if state.isInDriveGear then
            -- Regenerative braking when letting off accelerator (tiny bit in D, full in L)
            local regenPercentOfBrakingForce = braking.maxRegenTorque / braking.totalPossibleBrakeTorque
            local zeroThrottleRegen = state.gear == "L" and 1 or M.evCoastRegen
            
            state.brake = state.negThrottle * zeroThrottleRegen * regenPercentOfBrakingForce
            braking.desiredRegen = state.negThrottle * zeroThrottleRegen * maprange(state.speedMph, 0, M.minRegenFadeSpeed)
            braking.requestedRegenTorque = braking.desiredRegen * braking.maxRegenTorque
        else
            state.brake = 0
            braking.desiredRegen = 0
            braking.requestedRegenTorque = 0
        end
        
        debugutil:putValue("Total Braking Torque (%)", fnum(braking.currentRegenTorque) .. " (" .. fnum(braking.currentBrakingCoef) .. ")")
            
        --[[ if streams.willSend("profilingData") then
            gui.send("profilingData", {
                -- Blue
                totalBrakeTorque = { title = "Total Brake Torque", color = "#32eaf6", unit = "Nm", value = actualFrictionTorque + currentRegenTorque },
                -- Purple
                requestedBrakeTorque = { title = "Requested Brake Torque", color = "#7700ff", unit = "Nm", value = requestedBrakingTorqueOnWheels },
                -- Green
                regenBrakeTorque = { title = "Regen Brake Torque", color = "#379637", unit = "Nm", value = currentRegenTorque },
                -- Red
                frictionBrakeTorque = { title = "Friction Brake Torque", color = "#f65232", unit = "Nm", value = actualFrictionTorque },
            })
        end ]]
    end
    
    torqueData.targetMG1Direction = 1
    torqueData.targetMG1Torque = 0.0
    torqueData.targetMG2Torque = 0.0
    torqueData.targetRearMotorTorque = 0.0
    
    if state.current == "init" then
        engineState.targetThrottle = 0
    elseif M.latchIsInNeutral:test(state.gear == "N", dt) then
        engineState.targetThrottle = state.throttle
        power.requestedChargePower = 0
        M.targetEngineRpmSmoother:set(engine.idleRPM)
        M.driveThrottlePid:reset()
    elseif state.current == "chargemode" then
        engineState.targetThrottle = engineState.chargeThrottle
        
        local chargeTargetRpm = M.forcedChargeupRpm or M.engineMaxRpm
        local desiredGenPowerCoef = maprange(electrics.values.rpm, chargeTargetRpm - M.engineLoadRpmToleranceIn, chargeTargetRpm)
        
        torqueData.targetMG1Torque = -desiredGenPowerCoef * torqueData.gearboxMaxMG1TorqueUnchecked
    else
        updateHybridLogic(dt)
    end
    
    updateMotors(dt)
    updateState(dt)
    updateBattery(dt)
    
    electrics.values.iceThrottle = engineState.throttleOverride or engineState.targetThrottle

    if mainTank then
        debugutil:putValue("Fuel Remaining (L)", mainTank.remainingVolume, 2)
    else
        debugutil:putValue("Fuel Remaining (L)", "No fuel tank installed")
    end
    
    debugutil:putValue("Battery SOC %", battery.remainingRatio * 100)
    debugutil:putValue("Input Throttle", (state.throttle or 0))
    debugutil:putValue("Effective Throttle / Brake", fnum(state.throttle or 0) .. " / " .. fnum(state.brake or 0))
    debugutil:putValue("Actual ICE Throttle", electrics.values.iceThrottle)
    debugutil:putValue("Throttle Aggr.", state.throttleAggressiveness)
    debugutil:putValue("Charge Throttle", engineState.chargeThrottle)
    debugutil:putValue("MG2 Adjustment", state.mg2TorqueAdjustment)
    debugutil:putValue("Acceleration", state.currentAcceleration)
    
    M.accelerationLimiterPid:get() -- just update debug
    
    if engine then
        debugutil:putValue("ICE Load", engine.engineLoad)
        debugutil:putValue("ICE Torque", engine.outputTorque1)
        debugutil:putValue("ICE HP", mathutil.torqueToKw(engine.outputTorque1, engine.outputAV1) * 1.34102)
    end
    
    if state.next then
        state.current = state.next
        state.next = nil
    end

    updateGUI(dt)
    updateSounds(dt)
end

local function applyControlParameters()
    M.throttleCurveExponent = controlParameters.hybrid.throttleCurveExponent
end

local function init(jbeamData)
    state.timeSinceVehicleLoaded = 0
    
    M.jbeamData = jbeamData
    
    -- Power/startup parameters
    M.startEvMode = jbeamData.startEvMode == true
    M.forceEvMode = jbeamData.forceEvMode == true
    M.startPoweredOff = jbeamData.startPoweredOff or false
    M.powerSwitch = not M.startPoweredOff
    M.powerOn = not M.startPoweredOff
    M.initializeTime = jbeamData.initializeTime or 2.0
    M.vehicleStartupEngineDelay = jbeamData.vehicleStartupEngineDelay or 10
    M.vehicleStartupEngineRuntime = jbeamData.vehicleStartupEngineRuntime or 15
    M.hasAuxiliaryStarterMotor = jbeamData.hasAuxiliaryStarterMotor or false
    
    -- EV-mode parameters
    M.evCreepThrottle = jbeamData.evCreepThrottle or 0.25
    M.maxCreepSpeed = jbeamData.maxCreepSpeed or 5
    M.maxEvOnlySecondaryPower = jbeamData.maxEvOnlySecondaryPower or 15 -- kW
    M.maxEvOnlySpeed = jbeamData.maxEvOnlySpeed or 65 -- MPH
    M.maxEvOnlyThrottle = jbeamData.maxEvOnlyThrottle or 0.65 -- ratio
    M.maxEvOnlyThrottleAggressiveness = jbeamData.maxEvOnlyThrottleAggressiveness or 20
    M.maxEvOnlyPitch = jbeamData.maxEvOnlyPitch or math.pi -- 180 degrees pitch by default; will never happen (effectively disabled)
    M.maxEvModeMG1Torque = jbeamData.maxEvModeMG1Torque
    
    -- Regen parameters
    M.maxRegenTorque = jbeamData.maxRegenTorque or 1000
    M.regenWheelslipDisableTime = jbeamData.regenWheelslipDisableTime or 0.5
    M.regenWheelslipReenableTime = jbeamData.regenWheelslipReenableTime or 1.0
    M.maxWheelSlipCoefForRegen = jbeamData.maxWheelSlipCoefForRegen or 0.05
    M.minRegenFadeSpeed = jbeamData.minRegenFadeSpeed or 3 -- MPH
    M.regenEfficiency = jbeamData.regenEfficiency or 0.93
    M.evCoastRegen = jbeamData.evCoastRegen or 0.05
    
    -- Power/battery parameters
    M.idlePowerDraw = jbeamData.idlePowerDraw or 1 -- kW
    M.minBatteryPowerLevel = jbeamData.minBatteryPowerLevel or 0.25 -- ratio; low end of max-kW slope
    M.maxBatteryPowerLevel = jbeamData.maxBatteryPowerLevel or 0.85 -- ratio; high end of max-kW slope
    M.criticalBatteryPowerLevel = jbeamData.criticalBatteryPowerLevel or 0.05 -- raw state-of-charge
    M.batteryChargeLowRatio = jbeamData.batteryChargeLowRatio or 0.15
    M.batteryChargeHighRatio = jbeamData.batteryChargeHighRatio or 0.25
    M.forcedChargeupPower = jbeamData.forcedChargeupPower or 25 -- kW
    M.forcedChargeupRpm = jbeamData.forcedChargeupRpm
    M.minChargePowerParked = jbeamData.minChargePowerParked or 0
    M.maxChargePowerParked = jbeamData.maxChargePowerParked or 5
    M.parkedChargingMinIdleRpm = jbeamData.parkedChargingMinIdleRpm
    M.chargeupMinPower = jbeamData.chargeupMinPower or 3
    M.allowChargeMode = jbeamData.allowChargeMode ~= false
    M.chargeModePower = jbeamData.chargeModePower or 10
    M.chargeModeUpperThreshold = jbeamData.chargeModeUpperThreshold or 0.8
    
    -- Engine auto-stop parameters
    M.enableAutoStop = jbeamData.enableAutoStop ~= false
    M.autoStopBrakeThreshold = jbeamData.autoStopBrakeThreshold or 0
    M.autoStopBrakeTime = jbeamData.autoStopBrakeTime or 0
    M.engineMinRunTime = jbeamData.engineMinRunTime or 4 -- seconds
    M.minAutoStopCoolantTemp = jbeamData.minAutoStopCoolantTemp or 30
    M.minAutoStopCoolantTempWarmedUp = jbeamData.minAutoStopCoolantTempWarmedUp or 57
    M.autoStopThrottleThreshold = jbeamData.autoStopThrottleThreshold or 0.015
    
    -- Engine parameters
    M.engineMinIdleRpm = jbeamData.engineMinIdleRpm
    M.engineMaxRpm = jbeamData.engineMaxRpm
    M.useEngineTorqueCurve = jbeamData.useEngineTorqueCurve
    M.engineStartupIdleTime = jbeamData.engineStartupIdleTime or 0.15
    M.engineStartupMaxRpm = jbeamData.engineStartupMaxRpm
    M.engineStartupRpmThreshold = jbeamData.engineStartupRpmThreshold or 100
    M.engineStartupMinThrottle = jbeamData.engineStartupMinThrottle or 0.065
    M.engineStarterTorque = jbeamData.engineStarterTorque or 40
    M.engineSpooldownTorque = jbeamData.engineSpooldownTorque or 15
    M.idealEngineLoad = jbeamData.idealEngineLoad
    M.engineLoadRpmToleranceIn = jbeamData.engineLoadRpmToleranceIn or jbeamData.engineLoadRpmTolerance or 300
    M.engineLoadRpmToleranceOut = jbeamData.engineLoadRpmToleranceOut or jbeamData.engineLoadRpmTolerance or 150
    M.engineLoadBiasCoefIn = jbeamData.engineLoadBiasCoefIn or jbeamData.engineLoadBiasCoef or 0.45
    M.engineLoadBiasCoefOut = jbeamData.engineLoadBiasCoefOut or jbeamData.engineLoadBiasCoef or 0.55
    M.engineLoadBiasCoefOutModeOne = jbeamData.engineLoadBiasCoefOutModeOne or jbeamData.engineLoadBiasCoefOut or jbeamData.engineLoadBiasCoef or 0.25
    M.engineLagCompensationEnabled = jbeamData.engineLagCompensationEnabled ~= false
    M.maxEngineLagCompensationTorque = jbeamData.maxEngineLagCompensationTorque
    M.engineDeloadMaxIgnitionCut = jbeamData.engineDeloadMaxIgnitionCut or 0.4
    M.maxParkEngineTorqueCoef = jbeamData.maxParkEngineTorqueCoef or 0.5
    
    -- Throttle parameters
    M.minThrottleAggressivenessRamp = jbeamData.minThrottleAggressivenessRamp or 1.5
    M.maxThrottleAggressivenessRamp = jbeamData.maxThrottleAggressivenessRamp or 8
    M.minThrottleAggressivenessFactor = jbeamData.minThrottleAggressivenessFactor or 2
    M.maxThrottleAggressivenessFactor = jbeamData.maxThrottleAggressivenessFactor or 15
    
    if jbeamData.throttleCurveExponent then
        controlParameters.hybrid.throttleCurveExponent = jbeamData.throttleCurveExponent
    end
    
    -- Output torque parameters
    M.maxLaunchControlTorque = jbeamData.maxLaunchControlTorque or 50
    M.reducedPropulsionTorqueCoef = jbeamData.reducedPropulsionTorqueCoef or 0.5
    M.maxAcceleration = jbeamData.maxAcceleration -- in m/s/s
    M.useAccelLimiter = type(M.maxAcceleration) == "number" and M.maxAcceleration > 0
    
    -- Hill start assist parameters
    M.hillAssistBrakeHoldAmount = jbeamData.hillAssistBrakeHoldAmount or 0.15 -- %
    M.minHillAssistPitch = jbeamData.minHillAssistPitch or 0.0523599 -- rads; 3 degrees
    M.maxHillAssistPitch = jbeamData.maxHillAssistPitch or 0.20944 -- rads; 12 degrees
    
    -- MG1 sound parameters
    M.inverterSampleNameMG1 = jbeamData.inverterSampleNameMG1 or jbeamData.inverterSampleName or "vehicles/hybrid_common/sounds/inverter.ogg"
    M.inverterMaxVolumeKwMG1 = jbeamData.inverterMaxVolumeKwMG1 or jbeamData.inverterMaxVolumeKw or 20
    M.inverterBaseVolumeMG1 = jbeamData.inverterBaseVolumeMG1 or jbeamData.inverterBaseVolume or 0.35
    M.inverterMinVolumeMG1 = jbeamData.inverterMinVolumeMG1 or jbeamData.inverterMinVolume or 0.05
    M.inverterPitchMinSpeedMG1 = jbeamData.inverterPitchMinSpeedMG1 or jbeamData.inverterPitchMinSpeed or 1
    M.inverterPitchMaxSpeedMG1 = jbeamData.inverterPitchMaxSpeedMG1 or jbeamData.inverterPitchMaxSpeed or 1100
    M.inverterMinPitchMG1 = jbeamData.inverterMinPitchMG1 or jbeamData.inverterMinPitch or 0.025
    M.inverterMaxPitchMG1 = jbeamData.inverterMaxPitchMG1 or jbeamData.inverterMaxPitch or 1.0
    
    -- MG2 sound parameters
    M.inverterSampleNameMG2 = jbeamData.inverterSampleNameMG2 or jbeamData.inverterSampleName or "vehicles/hybrid_common/sounds/inverter.ogg"
    M.inverterMaxVolumeKwMG2 = jbeamData.inverterMaxVolumeKwMG2 or jbeamData.inverterMaxVolumeKw or 20
    M.inverterBaseVolumeMG2 = jbeamData.inverterBaseVolumeMG2 or jbeamData.inverterBaseVolume or 0.35
    M.inverterMinVolumeMG2 = jbeamData.inverterMinVolumeMG2 or jbeamData.inverterMinVolume or 0.05
    M.inverterPitchMinSpeedMG2 = jbeamData.inverterPitchMinSpeedMG2 or jbeamData.inverterPitchMinSpeed or 1
    M.inverterPitchMaxSpeedMG2 = jbeamData.inverterPitchMaxSpeedMG2 or jbeamData.inverterPitchMaxSpeed or 1100
    M.inverterMinPitchMG2 = jbeamData.inverterMinPitchMG2 or jbeamData.inverterMinPitch or 0.025
    M.inverterMaxPitchMG2 = jbeamData.inverterMaxPitchMG2 or jbeamData.inverterMaxPitch or 1.0
    
    -- Transmission whine parameters
    M.motorSampleName = jbeamData.motorSampleName or "vehicles/hybrid_common/sounds/motor_ramp.ogg"
    M.motorBaseVolume = jbeamData.motorBaseVolume or 0
    M.motorVolumeMinSpeed = jbeamData.motorVolumeMinSpeed or 4
    M.motorVolumeMaxSpeed = jbeamData.motorVolumeMaxSpeed or 15
    M.motorPitchMinSpeed = jbeamData.motorPitchMinSpeed or 0
    M.motorPitchMaxSpeed = jbeamData.motorPitchMaxSpeed or 50
    M.motorMinPitch = jbeamData.motorMinPitch or 0.01
    M.motorMaxPitch = jbeamData.motorMaxPitch or 1
    M.motorInverterEffortVolumeCoef = jbeamData.motorInverterEffortVolumeCoef or 0
    M.motorHarmonicRatio = jbeamData.motorHarmonicRatio
    
    -- Node IDs
    if jbeamData.inverterSoundNode_nodes and type(jbeamData.inverterSoundNode_nodes) == "table" then
        M.inverterSoundNodeMG1 = jbeamData.inverterSoundNode_nodes[1]
        M.inverterSoundNodeMG2 = jbeamData.inverterSoundNode_nodes[1]
    end
    
    if jbeamData.inverterSoundNodeMG1_nodes and type(jbeamData.inverterSoundNodeMG1_nodes) == "table" then
        M.inverterSoundNodeMG1 = jbeamData.inverterSoundNodeMG1_nodes[1]
    end
    
    if jbeamData.inverterSoundNodeMG2_nodes and type(jbeamData.inverterSoundNodeMG2_nodes) == "table" then
        M.inverterSoundNodeMG2 = jbeamData.inverterSoundNodeMG2_nodes[1]
    end
    
    if jbeamData.motorSoundNode_nodes and type(jbeamData.motorSoundNode_nodes) == "table" then
        M.motorSoundNode = jbeamData.motorSoundNode_nodes[1]
    end
    
    -- Startup/shutdown sound parameters
    M.startupSampleName = jbeamData.startupSampleName or "file:>vehicles>hybrid_common>sounds>hybrid_startup.ogg"
    M.startupSoundBaseVolume = jbeamData.startupSoundBaseVolume or 0.25
    M.shutdownSampleName = jbeamData.shutdownSampleName or "file:>vehicles>hybrid_common>sounds>hybrid_shutdown.ogg"
    M.shutdownSoundBaseVolume = jbeamData.shutdownSoundBaseVolume or 0.25
    M.interiorStartupSampleName = jbeamData.interiorStartupSampleName
    M.interiorStartupSoundBaseVolume = jbeamData.interiorStartupSoundBaseVolumne or 0.25
    M.interiorShutdownSampleName = jbeamData.interiorShutdownSampleName
    M.interiorShutdownSoundBaseVolume = jbeamData.interiorShutdownSoundBaseVolumne or 0.25
    
    if jbeamData.interiorSoundNode_nodes and type(jbeamData.interiorSoundNode_nodes) == "table" then
        M.interiorSoundNode = jbeamData.interiorSoundNode_nodes[1]
    end
    
    -- Various curves
    if jbeamData.engineRpmVsTorque then
        M.engineRpmVsTorque = simpleCurve.createSimpleCurve(jbeamData.engineRpmVsTorque, "torque", "rpm")
    else
        if M.useEngineTorqueCurve ~= true then
            error("engineRpmVsTorque must be provided if useEngineTorqueCurve is not set to true")
        end
    end
    
    M.maxChargePowerVsSpeed = simpleCurve.createSimpleCurve(jbeamData.maxChargePowerVsSpeed, "speed", "maxChargePower", {
        {"speed", "maxChargePower"},
        {0, 5},
        {25, 15},
    })
    M.chargePowerCoefVsThrottle = simpleCurve.createSimpleCurve(jbeamData.chargePowerCoefVsThrottle, "throttle", "chargePowerCoef", {
        {"throttle", "chargePowerCoef"},
        {0, 0.65},
        {75, 1},
        {100, 0},
    })
    M.chargePowerCoefVsBatteryLevel = simpleCurve.createSimpleCurve(jbeamData.chargePowerCoefVsBatteryLevel, "batteryLevel", "chargePowerCoef", {
        {"batteryLevel", "chargePowerCoef"},
        {30, 1},
        {50, 0},
    })
    M.maxEvOnlyPriorityPowerSpeed = simpleCurve.createSimpleCurve(jbeamData.maxEvOnlyPriorityPowerSpeed, "speed", "power", {
        {"speed", "power"},
        {0, 25},
        {25, 15},
    })
    M.maxEngineAssistPriorityPowerSpeed = simpleCurve.createSimpleCurve(jbeamData.maxEngineAssistPriorityPowerSpeed, "speed", "power", {
        {"speed", "power"},
        {15, 5},
        {30, 15},
    })
    M.maxEngineAssistSecondaryPowerSpeed = simpleCurve.createSimpleCurve(jbeamData.maxEngineAssistSecondaryPowerSpeed, "speed", "power", {
        {"speed", "power"},
        {0, 20},
        {25, 20},
    })
    M.maxPriorityPowerBatteryLevel = simpleCurve.createSimpleCurve(jbeamData.maxPriorityPowerBatteryLevel, "batteryLevel", "power", {
        {"batteryLevel", "power"},
        {20, 0},
        {50, 50},
    })
    M.maxSecondaryPowerBatteryLevel = simpleCurve.createSimpleCurve(jbeamData.maxSecondaryPowerBatteryLevel, "batteryLevel", "power", {
        {"batteryLevel", "power"},
        {0, 0},
        {20, 40}
    })
    M.maxRearTorqueSplitVsSpeed = simpleCurve.createSimpleCurve(jbeamData.maxRearTorqueSplitVsSpeed, "speed", "torqueSplit", {
        {"speed", "torqueSplit"},
        {25, 0.65},
        {60, 0.25},
    })
    M.maxRearRegenTorqueSplitVsSpeed = simpleCurve.createSimpleCurve(jbeamData.maxRearRegenTorqueSplitVsSpeed, "speed", "torqueSplit", {
        {"speed", "torqueSplit"},
        {25, 0.3},
        {60, 0.1},
    })
    
    if jbeamData.maxOutputTorqueVsSpeed then
        M.maxOutputTorqueVsSpeed = simpleCurve.createSimpleCurve(jbeamData.maxOutputTorqueVsSpeed, "speed", "maxOutputTorque")
    end
    if jbeamData.maxReverseOutputTorqueVsSpeed then
        M.maxReverseOutputTorqueVsSpeed = simpleCurve.createSimpleCurve(jbeamData.maxReverseOutputTorqueVsSpeed, "speed", "maxOutputTorque")
    end
    
    -- PIDs
    M.driveThrottlePid = pid.newPid({
        kPIn = jbeamData.throttlePidPFactorIn or jbeamData.throttlePidPFactor or 1.5,
        kPOut = jbeamData.throttlePidPFactorOut or jbeamData.throttlePidPFactor or 1.75,
        kIIn = jbeamData.throttlePidIFactorIn or jbeamData.throttlePidIFactor or 6.5,
        kIOut = jbeamData.throttlePidIFactorOut or jbeamData.throttlePidIFactor or 4.0,
        kD = jbeamData.throttlePidDFactor or 0.005,
        scale = 1 / (jbeamData.throttlePidScale or 2000),
        integralLockThreshold = jbeamData.throttlePidIntegralLockThreshold,
        minOutput = 0,
        maxOutput = 1,
        name = "throttle",
        debug = true,
    })
    M.chargeThrottlePid = pid.newPid({
        kP = jbeamData.chargeThrottlePidPFactor or 5.5, 
        kI = jbeamData.chargeThrottlePidIFactor or 8,
        scale = 1 / (jbeamData.chargeThrottlePidScale or 1500),
        minOutput = 0,
        maxOutput = 1,
        -- debug = true,
    })
    M.accelerationLimiterPid = pid.newPid({
        kP = jbeamData.accelerationLimiterPidPFactor or 0.25,
        kI = jbeamData.accelerationLimiterPidIFactor or 1.75,
        scale = 1 / (M.maxAcceleration or 1),
        minOutput = 0,
        maxOutput = 1,
        name = "accelerationLimiter",
        -- debug = true,
    })

    -- Lots of temporal smoothers!
    M.throttleSmoother = newTemporalSmoothingNonLinear(
        jbeamData.throttleSmoothingIn or 1.5,
        jbeamData.throttleSmoothingOut or 1.5)
    M.vehicleSpeedSmoother = newTemporalSmoothingNonLinear(jbeamData.vehicleSpeedSmoothing or 20)
    M.throttleLimiterSmoother = newTemporalSmoothingNonLinear(jbeamData.throttleLimitSmoothing or 0.95)
    M.engineStarterSmoother = newTemporalSmoothingNonLinear(
        jbeamData.engineStarterSmoothingIn or 30,
        jbeamData.engineStarterSmoothingOut or 30)
    M.mg1MaxPowerSmoother = newTemporalSmoothingNonLinear(jbeamData.motorPowerSmoothing or 25)
    M.mg2MaxPowerSmoother = newTemporalSmoothingNonLinear(jbeamData.motorPowerSmoothing or 25)
    M.targetEngineRpmSmoother = newTemporalSmoothingNonLinear(
        jbeamData.targetEngineRpmSmoothingIn or jbeamData.targetEngineRpmSmoothing or 3,
        jbeamData.targetEngineRpmSmoothingOut or jbeamData.targetEngineRpmSmoothing or 4
    )
    M.actualEngineRpmSmoother = newExponentialSmoothing(jbeamData.actualEngineRpmSmoothing or 5)
    M.engineStartupLoadSmoother = newTemporalSmoothing(jbeamData.engineStartupLoadSmoothing or 4.5)
    M.engineLoadSmoother = newTemporalSmoothingNonLinear(jbeamData.engineLoadSmoothing or 8)
    M.engineDeloadSmoother = newTemporalSmoothingNonLinear(jbeamData.engineDeloadSmoothing or 5)
    M.accelerationSmoother = newTemporalSmoothingNonLinear(jbeamData.accelerationSmoothing or 4)
    M.maxCombinedTorqueSmoother = newTemporalSmoothingNonLinear(jbeamData.maxCombinedTorqueSmoothing or 2)
        
    -- Sound smoothers
    M.mg1PitchSmoother = newTemporalSmoothingNonLinear(6)
    M.mg2PitchSmoother = newTemporalSmoothingNonLinear(6)
    
    -- Latches
    M.latchDriveModeZero = latch.newTimedLatch(jbeamData.modeZeroLatchTime or 0.25)
    M.latchDriveModeOne  = latch.newTimedLatch(jbeamData.modeOneLatchTime or 0.25, true)
    M.latchIsInNeutral = latch.newTimedLatch(jbeamData.neutralLatchTime or 0.5, false, true)
    M.latchBatteryChargeUp = latch.newThresholdGate(M.batteryChargeLowRatio, M.batteryChargeHighRatio)
    
    applyControlParameters()
end

local function reset()
    M.init(M.jbeamData)
    M.initSecondStage()
end

local function initSounds()
    soundSources.mg1Whine = obj:createSFXSource(
        M.inverterSampleNameMG1,
        "AudioDefaultLoop3D",
        "mg1Whine",
        M.inverterSoundNodeMG1 or gearbox.transmissionNodeID
    )
    
    soundSources.mg2Whine = obj:createSFXSource(
        M.inverterSampleNameMG2,
        "AudioDefaultLoop3D",
        "mg2Whine",
        M.inverterSoundNodeMG2 or gearbox.transmissionNodeID
    )

    soundSources.motorRamp = obj:createSFXSource(
        M.motorSampleName,
        "AudioDefaultLoop3D",
        "motorRamp",
        M.motorSoundNode or gearbox.transmissionNodeID
    )

    soundSources.motorRampHarmony = obj:createSFXSource(
        M.motorSampleName,
        "AudioDefaultLoop3D",
        "motorRampHarmony",
        M.motorSoundNode or gearbox.transmissionNodeID
    )
end

local function overrideSetStarter(enabled)
    if enabled then
        M.powerSwitch = not M.powerSwitch
    end
end

local function overrideSetIgnition(enabled)
    if not enabled then
        M.powerSwitch = false
    end
end

local function initSecondStage()
    engine = powertrain.getDevice("mainEngine")
    gearbox = powertrain.getDevice("gearbox")
    rearMotor = powertrain.getDevice("rearMotor")
    battery = energyStorage.getStorage("tractionBattery")
    mainTank = energyStorage.getStorage("mainTank")
    
    local hasError = false
    
    if not engine or engine.type ~= "combustionEngine" then
        debugutil:log("W", "Engine not found")
        engine = nil
    end
    if not gearbox then
        debugutil:log("E", "Gearbox not found; cannot continue")
        hasError = true
    end
    if rearMotor then
        debugutil:log("D", "Discovered rear electric motor; AWD enabled!")
    end
    if not battery then
        debugutil:log("E", "Battery not found; cannot continue")
        hasError = true
    end
    if not mainTank then
        debugutil:log("W", "Main fuel tank not found")
    end
    if gearbox.type ~= "ecvtGearbox" then
        debugutil:log("E", "Gearbox must be of type ecvtGearbox; cannot continue")
        hasError = true
    end
    
    if hasError then
        M.updateGFX = nop
        return
    end
    
    local cmuControllers = controller.getControllersByType("drivingDynamics/CMU")
    
    if cmuControllers and #cmuControllers == 1 then
        CMU = cmuControllers[1]
    else
        debugutil:log("W", "Vehicle does not have a valid ESC/CMU")
    end
    
    -- Override setStarter and setEngineIgnition
    if controller.mainController then
        controller.mainController.setStarter = overrideSetStarter
        controller.mainController.setEngineIgnition = overrideSetIgnition
    end

    M.minBatteryPowerLevel = battery.jbeamData.minWorkingSoc or M.minBatteryPowerLevel
    M.maxBatteryPowerLevel = battery.jbeamData.maxWorkingSoc or M.maxBatteryPowerLevel
    M.criticalBatteryPowerLevel = battery.jbeamData.criticalSoc or M.criticalBatteryPowerLevel

    if not M.forcedChargeupPower then
        M.forcedChargeupPower = battery.jbeamData.maxPowerInput
    end
    
    local engineTorqueData = engine:getTorqueData()
    
    if not M.jbeamData.engineMaxRpm then
        M.engineMaxRpm = engineTorqueData.maxPowerRPM
    end
    
    if not M.engineMinIdleRpm then
        M.engineMinIdleRpm = engine.idleRPM + 200
    end
    
    if not M.idealEngineLoad then
        M.idealEngineLoad = engineutil.calculateBestEfficiencyLoad(engine)
    end
    
    engineutil.setIdealLoadForEngine(engine, M.idealEngineLoad)

    calculateTorqueStatistics(0)
    printTorqueData()

    braking.totalPossibleBrakeTorque = 0
    braking.totalPropulsedBrakeTorque = 0
    braking.totalUnpropulsedBrakeTorque = 0
    
    for i = 0, wheels.wheelCount - 1, 1 do
        local wd = wheels.wheels[i]
        
        braking.totalPossibleBrakeTorque = braking.totalPossibleBrakeTorque + wd.brakeTorque
        
        if wd.isPropulsed then
            braking.totalPropulsedBrakeTorque = braking.totalPropulsedBrakeTorque + wd.brakeTorque
        else
            braking.totalUnpropulsedBrakeTorque = braking.totalUnpropulsedBrakeTorque + wd.brakeTorque
        end
        
        if not initialWheelBrakeSplit[i] then
            initialWheelBrakeSplit[i] = {wd.brakeInputSplit, wd.brakeSplitCoef}
        end
    end

    if M.startEvMode then
        electrics.values.evMode = 1
    end
    
    if engine and M.startPoweredOff then
        engine.outputAV = 0
    end
    
    -- prefill electric values
    electrics.values.evMode = 0
    electrics.values.chargeMode = 0
    electrics.values.batteryLevel = 0
    
    -- reset local state objects
    -- state
    state.current = "init"
    state.next = nil
    state.last = nil
    state.timeSinceVehicleLoaded = 0
    state.timeSinceInit = max(0, M.initializeTime - 0.25)
    state.timeSincePowerOn = 0
    state.shownDisabledMessage = false
    state.chargeModeEnabled = false
    state.miscEngineDemandReason = "none"
    state.miscEngineDemand = false
    state.startingTime = 0
    state.forcedChargeMode = false
    state.reducedPropulsion = false
    state.currentAcceleration = 0
    
    -- engineDemand
    engineDemand.isDemanded = false
    engineDemand.wasDemanded = false
    engineDemand.reason = "none"
    engineDemand.triggerReason = "none"
    engineDemand.miscDemand = false
    engineDemand.miscDemandReason = "none"
    engineDemand.waterTempDemand = false
    
    -- engineState
    engineState.runTimeSincePowerOn = 0
    engineState.timeSinceStart = 5
    engineState.timeSinceCanShutOff = 1
    engineState.targetThrottle = 0
    engineState.chargeThrottle = 0
    engineState.startAttempts = 0
    engineState.hasStartedSinceInit = false
    engineState.initialOutputTorqueState = engine.outputTorqueState
    
    if not engineState.initialScaleOutputTorque then
        engineState.initialScaleOutputTorque = engine.scaleOutputTorque
    end
    
    -- torqueData
    torqueData.firstCalculation = true
    
    -- monkey-patch the scaleOutputTorque function on engine so we can listen to it
    engine.scaleOutputTorque = function(device, scale, maxReduction)
        -- scale our own copy of the initial value
        engineState.initialOutputTorqueState = max(engineState.initialOutputTorqueState * scale, maxReduction or 0)
        engineState.initialScaleOutputTorque(device, scale, maxReduction)
    end
    
    -- Reset all the stateful things
    M.throttleSmoother:reset()
    M.throttleLimiterSmoother:reset()
    M.engineStarterSmoother:reset()
    M.engineStartupLoadSmoother:reset()
    M.targetEngineRpmSmoother:set(engine.idleRPM)
    M.engineLoadSmoother:reset()
    M.mg1MaxPowerSmoother:reset()
    M.mg2MaxPowerSmoother:reset()
    M.driveThrottlePid:reset()
    M.chargeThrottlePid:reset()
    M.accelerationLimiterPid:reset()
    M.maxCombinedTorqueSmoother:reset()
    M.engineDeloadSmoother:reset()
    
    -- Pre-fill debug table with empty strings to set up proper order
    debugutil:clearValues()
    updateDefaultDebugValues()
    
    initialControlParameters = deepcopy(controlParameters)
end

local function setParameters(parameters)
    if not CMU then return end
    
    CMU.applyParameter(controlParameters, initialControlParameters, parameters, "hybrid.throttleCurveExponent")
    
    applyControlParameters()
end

M.update = update
M.updateGFX = updateGFX
M.init = init
M.initSounds = initSounds
M.initSecondStage = initSecondStage
M.reset = reset
M.getTorqueData = getTorqueData
M.printTorqueData = printTorqueData
M.setParameters = setParameters

return M
