-- This Source Code Form is subject to the terms of the bCDDL, v. 1.1.
-- If a copy of the bCDDL was not distributed with this
-- file, You can obtain one at http://beamng.com/bCDDL-1.1.txt

local M = {}
local planetary = require("arcanox/planetaryGearset")
local mathutil = require("arcanox/mathutil")
local debugutil = require("arcanox/debugutil")("ecvt", true)
local constants = {
    rpmToAV = 0.104719755,
    avToRPM = 9.549296596425384,
    torqueToHpCoef = 7120.54016,
    torquePowerCoef = 9.5488
}

M.outputPorts = {[1] = true}
-- "viscouscoupling" below is a hack-around to allow a second gearbox to be piggybacked onto this one for insane hax
M.deviceCategories = {gearbox = true, viscouscoupling = true, clutchlike = true}
M.requiredExternalInertiaOutputs = {1}

local max = math.max
local min = math.min
local abs = math.abs
local sqrt = math.sqrt
local clamp = mathutil.clamp
local sign = mathutil.sign
local maprange = mathutil.maprange

local function formatAv(av)
    return debugutil:formatNumber(av * constants.avToRPM, 2) .. " (" .. debugutil:formatNumber(av, 2) .. ")"
end

-- ecvtGearbox Prototype
local ecvt = {}
ecvt.__index = ecvt

function ecvt:velocityUpdate(dt)
    -- update planet carrier AV based on existing input AV and sun gear torque
    local carrierTorqueFromSun = -self.psd:getCarrierTorqueFromSunTorque(self.motorGenerator1Torque)
    local carrierTorque = self.adjustedInputTorque + carrierTorqueFromSun
    
    -- One-way sprag preventing reverse engine rotation
    self.engineSpragAngle = min(max(self.engineSpragAngle - self.psd.carrierAV * dt, 0), self.maxEngineSpragAngle)
    carrierTorque = carrierTorque + self.engineSpragAngle * self.engineSpragSpring
    
    -- we can reuse torque ratio functions for inertia as well
    local combinedCarrierInertia = self.psd:getTotalCarrierInertia() + abs(self.psd:getCarrierTorqueFromRingTorque(self.cumulativeInertia))
    local carrierAV = self.psd.carrierAV + carrierTorque * dt / combinedCarrierInertia
    
    -- ring gear is connected directly to output reduction gears
    local ringGearAV = self.outputAV1 * self.counterRingGearRatio
    
    -- update PSD internal AVs
    self.psd:updateAVs(nil, carrierAV, ringGearAV) -- sun, carrier, ring; omit sun and let planetary figure it out
    
    -- propagate AV changes to parent
    self.inputAV = self.parent[self.parentOutputAVName]
    
    -- These are really just aliases
    self.motorGenerator1AV = self.psd.sunGearAV
    self.motorGenerator2AV = self.outputAV1 * self.counterMG2GearRatio
    
    debugutil:putValue("Sun Gear RPM (AV)", formatAv(self.psd.sunGearAV))
    debugutil:putValue("Carrier RPM (AV)", formatAv(self.psd.carrierAV))
    debugutil:putValue("Ring Gear RPM (AV)", formatAv(self.psd.ringGearAV))
    debugutil:putValue("MG1 RPM (AV)", formatAv(self.motorGenerator1AV))
    debugutil:putValue("MG2 RPM (AV)", formatAv(self.motorGenerator2AV))
    debugutil:putValue("Input RPM (AV)", formatAv(self.inputAV))
end

function ecvt:torqueUpdate(dt)
    if self.children and self.children[1] then
        self.cumulativeGearRatio = self.children[1].cumulativeGearRatio
    end
    
    -- built-in clutch
    local avDiff = self.inputAV - self.psd.carrierAV * self.inputGearRatio
    
    self.clutchAngle = clamp(self.clutchAngle + avDiff * dt * self.clutchStiffness, -self.maxClutchAngle, self.maxClutchAngle)
    
    local clutchFreePlay = self.clutchFreePlay
    local absFreeClutchAngle = max(abs(self.clutchAngle) - clutchFreePlay, 0)
    local lockTorque = self.lockTorque
    local lockDampAV = min(abs(self.clutchAngle) / clutchFreePlay, 1) * avDiff

    self.torqueDiff = (clamp(min(1, absFreeClutchAngle) * absFreeClutchAngle * fsign(self.clutchAngle) * self.lockSpring + self.lockDamp * lockDampAV, -lockTorque, lockTorque))
    
    local reverseGearRatioCoef = self.reverseGearRatioCoef
    
    -- input torque from engine
    local rawCarrierTorque = self.torqueDiff * self.inputGearRatio
    
    self.adjustedInputTorque = rawCarrierTorque
    
    -- reset values for this tick
    self.motorGenerator1Torque = 0
    self.motorGenerator2Torque = 0
    
    if self.mode ~= "neutral" then
        if self.motorGenerator1TorqueDemand < 0 then
            self.motorGenerator1Torque = -sign(self.motorGenerator1AV) * min(-self.motorGenerator1TorqueDemand, self:getMaxMG1RegenTorque())
        else
            self.motorGenerator1Torque = self.motorGenerator1TorqueDirection * min(self.motorGenerator1TorqueDemand, self:getMaxMG1Torque())
        end
        
        if self.motorGenerator2TorqueDemand < 0 then -- regenerative braking
            self.motorGenerator2Torque = -sign(self.motorGenerator2AV) * min(-self.motorGenerator2TorqueDemand, self:getMaxMG2RegenTorque())
        else
            self.motorGenerator2Torque = reverseGearRatioCoef * min(self.motorGenerator2TorqueDemand, self:getMaxMG2Torque())
        end
    end
    
    -- motor powers
    local mg1PowerCoef = self.motorGenerator1TorqueDemand < 0 and 1 or self:getPowerCoef(self.motorGenerator1AV)
    local mg2PowerCoef = self.motorGenerator2TorqueDemand < 0 and 1 or self:getPowerCoef(self.motorGenerator2AV)
    
    self.motorGenerator1Power = mg1PowerCoef * abs(mathutil.torqueToKw(self.motorGenerator1Torque, max(self.minPowerAV, abs(self.motorGenerator1AV)))) * sign(self.motorGenerator1TorqueDemand)
    self.motorGenerator2Power = mg2PowerCoef * abs(mathutil.torqueToKw(self.motorGenerator2Torque, max(self.minPowerAV, abs(self.motorGenerator2AV)))) * sign(self.motorGenerator2TorqueDemand)
    
    if abs(self.outputAV1) < 100 then
        self.parkEngaged = 1
    end
    
    -- output torque is the same as ring gear torque, which is a sum of
    -- the torque of MG2 plus the ratio-adjusted torques from the input
    -- and MG1. If the input AV is near-zero and MG1 is applying torque
    -- that, without a sprag, would cause it to spin backwards, the sprag
    -- causes this torque to transfer to the ring gear directly from MG1
    -- using the stationary carrier as a reduction.
    local ringTorqueFromSun = -self.psd:getRingTorqueFromSunTorque(self.motorGenerator1Torque)
    local finalTorqueFromRing = ringTorqueFromSun * self.counterRingGearRatio
    local finalTorqueFromMG2 = self.motorGenerator2Torque * self.counterMG2GearRatio
    
    self.rawOutputTorque = finalTorqueFromRing + finalTorqueFromMG2
    self.outputTorque1 = self.rawOutputTorque - self.friction * clamp(self.outputAV1, -1, 1)
    
    if self.mode == "park" then
        self.parkClutchAngle = min(max(self.parkClutchAngle + self.outputAV1 * dt, -self.maxParkClutchAngle), self.maxParkClutchAngle)
        self.outputTorque1 = self.outputTorque1 + -self.parkClutchAngle * self.parkLockSpring * self.parkEngaged * self.lockCoef
    end
    
    debugutil:putValue("MG1 Torque", self.motorGenerator1Torque, 2)
    debugutil:putValue("MG1 Torque Demand", self.motorGenerator1TorqueDemand, 2)
    debugutil:putValue("MG1 Torque Coef", self.motorGenerator1TorqueCoef, 2)
    debugutil:putValue("MG1 Power", self.motorGenerator1Power, 2)
    debugutil:putValue("MG2 Torque", self.motorGenerator2Torque, 2)
    debugutil:putValue("MG2 Torque Demand", self.motorGenerator2TorqueDemand, 2)
    debugutil:putValue("MG2 Torque Coef", self.motorGenerator2TorqueCoef, 2)
    debugutil:putValue("MG2 Power", self.motorGenerator2Power, 2)
    debugutil:putValue("Sun->Ring Torque", ringTorqueFromSun, 2)
    debugutil:putValue("Ring->Final Torque", finalTorqueFromRing, 2)
    debugutil:putValue("MG2->Final Torque", finalTorqueFromMG2, 2)
    debugutil:putValue("Output Torque (Final)", self.outputTorque1, 2)
    debugutil:putValue("Output Torque (Wheels)", self.outputTorque1 * self.cumulativeGearRatio, 2)
end

function ecvt:validate()
    if not self.parent.deviceCategories.engine then
        log("E", "ecvtGearbox.validate", "Parent device is not an engine...")
        log("E", "ecvtGearbox.validate", "Actual parent:")
        log("E", "ecvtGearbox.validate", powertrain.dumpsDeviceData(self.parent))
        return false
    end
    
    self.lockTorque = self.lockTorque or (self.parent.torqueData.maxTorque * 1.25 + self.parent.maxRPM * self.parent.inertia * math.pi / 30)
    return true
end

function ecvt:setMode(mode)
    self.mode = mode
    self.reverseGearRatioCoef = mode == "reverse" and -1 or 1
end

function ecvt:setGearRatio(ratio)
end

function ecvt:setRequestedMG1Torque(requestedTorque)
    self.motorGenerator1TorqueDemand = clamp(requestedTorque, -self.mg1TorqueRating, self.mg1TorqueRating)
end

function ecvt:setRequestedMG2Torque(requestedTorque)
    self.motorGenerator2TorqueDemand = clamp(requestedTorque, -self.mg2TorqueRating, self.mg2TorqueRating)
end

function ecvt:setMG1PowerOverride(maxPowerOverride)
    self.maxMG1PowerOverride = min(maxPowerOverride, self.mg1PowerRating)
end

function ecvt:setMG2PowerOverride(maxPowerOverride)
    self.maxMG2PowerOverride = min(maxPowerOverride, self.mg2PowerRating)
end

function ecvt:setMG1TorqueDirection(direction)
    self.motorGenerator1TorqueDirection = (direction == -1) and -1 or 1
end

function ecvt:setLock(enabled)
    self.lockCoef = enabled and 0 or 1
end

function ecvt:calculateInertia()
    local outputInertia = 0
    
    self.cumulativeGearRatio = 1
    self.maxCumulativeGearRatio = 1
    
    if self.children and #self.children > 0 then
        local child = self.children[1]
        
        outputInertia = child.cumulativeInertia
        self.cumulativeGearRatio = child.cumulativeGearRatio
        self.maxCumulativeGearRatio = child.maxCumulativeGearRatio
    end
    
    -- built-in clutch spring
    self.lockSpring = self.lockSpringBase or (powertrain.stabilityCoef * powertrain.stabilityCoef * self.cumulativeInertia * self.lockSpringCoef) --Nm/rad
    self.lockDamp = self.lockDampRatio * sqrt(self.lockSpring * self.cumulativeInertia)

    --^2 spring but linear spring after 1 rad
    self.maxClutchAngle = sqrt(self.lockTorque / self.lockSpring) + max(self.lockTorque / self.lockSpring - 1, 0) + self.clutchFreePlay
    
    -- calculate inertia on ring gear from output
    self.cumulativeInertia = outputInertia / self.counterRingGearRatio / self.counterRingGearRatio
    self.invCumulativeInertia = 1 / self.cumulativeInertia

    self.parkLockSpring = self.jbeamData.parkLockSpring or (powertrain.stabilityCoef * powertrain.stabilityCoef * outputInertia * 0.5) --Nm/rad
    self.maxParkClutchAngle = self.parkLockTorque / self.parkLockSpring
    
    self.engineSpragSpring = self.jbeamData.engineSpragSpring or 1000 -- Nm/rad
    self.maxEngineSpragAngle = self.engineSpragLockTorque / self.engineSpragSpring
end

function ecvt:getMaxMG1Torque(overrideOrIgnore)
    local mg1AV = max(abs(self.motorGenerator1AV), 1e-30)
    local maxPower = self.mg1PowerRating
    
    if type(overrideOrIgnore) == "number" then
        maxPower = min(overrideOrIgnore, maxPower)
    elseif type(self.maxMG1PowerOverride) == "number" and not (type(overrideOrIgnore) == "boolean" and overrideOrIgnore == true) then
        maxPower = self.maxMG1PowerOverride
    end
    
    local maxTorque = mathutil.kwToTorque(maxPower, mg1AV)
    
    return min(maxTorque, self.mg1TorqueRating)
end

function ecvt:getMaxMG1RegenTorque(maxPower)
    local mg1AV = abs(self.motorGenerator1AV)
    local maxTorque = self:getMaxMG1Torque(maxPower or true)
    
    return maprange(mg1AV, 0, self.generatorMinFullLoadAV, 0, maxTorque)
end

function ecvt:getMaxMG2Torque(overrideOrIgnore)
    local mg2AV = max(abs(self.motorGenerator2AV), 1e-30)
    local maxPower = self.mg2PowerRating
    
    if type(overrideOrIgnore) == "number" then
        maxPower = min(overrideOrIgnore, maxPower)
    elseif type(self.maxMG2PowerOverride) == "number" and not (type(overrideOrIgnore) == "boolean" and overrideOrIgnore == true) then
        maxPower = self.maxMG2PowerOverride
    end
    
    local maxTorque = mathutil.kwToTorque(maxPower, mg2AV)
    
    return min(maxTorque, self.mg2TorqueRating)
end

function ecvt:getMaxMG2RegenTorque(maxPower)
    local mg2AV = abs(self.motorGenerator2AV)
    local maxTorque = self:getMaxMG2Torque(maxPower or true)
    
    return maprange(mg2AV, self.regenFadeMinAV, self.regenFadeMaxAV, 0, maxTorque)
end

function ecvt:getPowerCoef(motorAv)
    return maprange(abs(motorAv), 0, self.minPowerAV, self.startingPowerCoef, 1)
end

function ecvt:getMinCarrierAV()
    return self.psd:getCarrierAVFromSunAV(self.minMG1AV)
end

function ecvt:getMaxCarrierAV()
    return self.psd:getCarrierAVFromSunAV(self.maxMG1AV)
end

function ecvt:updateGFX(dt)
    if streams.willSend("ecvt") then
        gui.send("ecvt", {
            ringAv = self.psd.ringGearAV,
            sunAv = self.psd.sunGearAV,
            carrierAv = self.psd.carrierAV,
            ringTeeth = self.ringGearTeeth,
            sunTeeth = self.sunGearTeeth,
            planetsTeeth = self.planetGearTeeth
        })
    end
end

function ecvt:reset()
    local jbeamData = self.jbeamData
    
    self.gearRatio = 1
    self.friction = jbeamData.friction or 0
    self.cumulativeInertia = 1
    self.cumulativeGearRatio = 1
    self.maxCumulativeGearRatio = 1
    
    self.clutchAngle = 0
    self.clutchRatio = 1
    self.torqueDiff = 0

    self.outputAV1 = 0
    self.inputAV = 0
    self.rawOutputTorque = 0
    self.outputTorque1 = 0
    self.psd.carrierAV = 0
    self.isBroken = false
    self.psd.ringGearAV = 0

    self.lockCoef = 1
    self.reverseGearRatioCoef = 1
    self.parkClutchAngle = 0

    --one way viscous coupling (prevents rolling backwards)
    self.oneWayTorqueSmoother:reset()
    
    -- fill in one-time debug values
    local mg1MaxTorque = self:getMaxMG1Torque(true)
    local mg2MaxTorque = self:getMaxMG2Torque(true)
    
    debugutil:putValue("MG1 Max Torque", mg1MaxTorque, 2)
    debugutil:putValue("MG2 Max Torque", mg2MaxTorque, 2)
end

local function new(jbeamData)
    local device = {
        deviceCategories = shallowcopy(M.deviceCategories),
        requiredExternalInertiaOutputs = shallowcopy(M.requiredExternalInertiaOutputs),
        outputPorts = shallowcopy(M.outputPorts),
        name = jbeamData.name,
        type = jbeamData.type,
        inputName = jbeamData.inputName,
        inputIndex = jbeamData.inputIndex,
        counterRingGearRatio = jbeamData.counterRingGearRatio or 0.815, -- (53 / 65)
        counterMG2GearRatio = jbeamData.counterMG2GearRatio or 3.118, -- (53 / 17)
        inputGearRatio = jbeamData.inputGearRatio or 1.0,
        friction = jbeamData.friction or 200,
        cumulativeInertia = 1,
        cumulativeGearRatio = 1,
        maxCumulativeGearRatio = 1,
        isPhysicallyDisconnected = true,
        throttleFactor = 1,
        throttle = 0,
        
        lockDampRatio = jbeamData.lockDampRatio or 0.15, --1 is critically damped
        clutchStiffness = jbeamData.clutchStiffness or 1,
        clutchFreePlay = jbeamData.clutchFreePlay or 0.125,
        lockSpringCoef = jbeamData.lockSpringCoef or 1,
        clutchAngle = 0,
        torqueDiff = 0,
        
        outputAV1 = 0,
        inputAV = 0,
        sunGearAV = 0,
        planetCarrierAV = 0,
        ringGearAV = 0,
        
        rawOutputTorque = 0,
        outputTorque1 = 0,
        adjustedInputTorque = 0,
        motorGenerator1TorqueDemand = 0,
        motorGenerator1Torque = 0,
        motorGenerator1TorqueDirection = 1,
        motorGenerator2TorqueDemand = 0,
        motorGenerator2Torque = 0,
        
        motorGenerator1AV = 0,
        motorGenerator2AV = 0,
        
        motorGenerator1Power = 0,
        motorGenerator2Power = 0,
        
        isBroken = false,
        lockCoef = 1,
        reverseGearRatioCoef = 1,
        
        -- potentially neede for compatibility but not used
        gearRatio = 1,
        gearCount = 1,
        minGearIndex = 1,
        maxGearIndex = 1,
        gearIndex = 1,
    }
    
    setmetatable(device, ecvt)
    
    -- initialize planetary gearset
    device.psd = planetary.newPlanetaryGearset({
        sunGearTeeth = jbeamData.sunGearTeeth or 30,
        planetGearTeeth = jbeamData.planetGearTeeth or 23,
        ringGearTeeth = jbeamData.ringGearTeeth or 78,
        planetGearCount = jbeamData.planetGearCount or 4,
        gearRadiusCoef = jbeamData.gearRadiusCoef or 0.00075,
        gearWidth = jbeamData.gearWidth or 0.05, -- m; equal to 2 in
        gearDensity = jbeamData.gearDensity or 8050, -- kg/m3, 8,050 is the density of steel
    })
    
    --gearbox park locking clutch
    device.parkClutchAngle = 0
    device.parkLockTorque = jbeamData.parkLockTorque or 1000 --Nm
    
    --engine one-way sprag
    device.engineSpragAngle = 0
    device.engineSpragLockTorque = jbeamData.engineSpragLockTorque or 1000 -- Nm

    --one way viscous coupling (prevents rolling backwards)
    device.oneWayViscousCoef = jbeamData.oneWayViscousCoef or 0
    device.oneWayViscousTorque = jbeamData.oneWayViscousTorque or device.oneWayViscousCoef * 25
    device.oneWayTorqueSmoother = newExponentialSmoothing(jbeamData.oneWayViscousSmoothing or 50)

    --electric stuff
    device.mg1ZeroSpeedMaxTorqueCoef = jbeamData.mg1ZeroSpeedMaxTorqueCoef or 3.0
    device.maxMG1AV = jbeamData.maxMG1AV or 1780 -- AV
    device.minMG1AV = jbeamData.minMG1AV or -device.maxMG1AV
    device.mg1PowerRating = jbeamData.mg1PowerRating or 40 -- kW
    device.mg1TorqueRating = jbeamData.mg1TorqueRating or 80 -- Nm
    device.mg1PeakEfficiencyAv = device.mg1PowerRating * 1000 / device.mg1TorqueRating -- av = power / torque, so peak AV = maxPower / maxTorque
    device.maxMG1PowerOverride = nil
    device.maxMG2AV = jbeamData.maxMG2AV or 1780 -- AV
    device.minMG2AV = jbeamData.minMG2AV or -device.maxMG2AV
    device.mg2PowerRating = jbeamData.mg2PowerRating or 53 -- kW
    device.mg2TorqueRating = jbeamData.mg2TorqueRating or 163 -- Nm
    device.mg2PeakEfficiencyAv = device.mg2PowerRating * 1000 / device.mg2TorqueRating -- av = power / torque, so peak AV = maxPower / maxTorque
    device.maxMG2PowerOverride = nil
    device.minPowerAV = jbeamData.minPowerAV or 15
    device.startingPowerCoef = jbeamData.startingPowerCoef or 1.5
    device.regenFadeMinAV = jbeamData.regenFadeMinAV or 40 -- AV
    device.regenFadeMaxAV = jbeamData.regenFadeMaxAV or 300 -- AV
    device.generatorMinFullLoadAV = jbeamData.generatorMinFullLoadAV or 30 -- AV

    if jbeamData.gearboxNode_nodes and type(jbeamData.gearboxNode_nodes) == "table" then
        device.transmissionNodeID = jbeamData.gearboxNode_nodes[1]
    end

    device.breakTriggerBeam = jbeamData.breakTriggerBeam
    if device.breakTriggerBeam and device.breakTriggerBeam == "" then
        --get rid of the break beam if it's just an empty string (cancellation)
        device.breakTriggerBeam = nil
    end

    device.jbeamData = jbeamData
    
    -- fill in one-time debug values
    local mg1MaxTorque = device:getMaxMG1Torque(device.motorGenerator1TorqueDemand < 0)
    local mg2MaxTorque = device:getMaxMG2Torque(device.motorGenerator2TorqueDemand < 0)
    
    debugutil:putValue("MG1 Max Torque", mg1MaxTorque, 2)
    debugutil:putValue("MG2 Max Torque", mg2MaxTorque, 2)

    return device
end

M.new = new

return M
