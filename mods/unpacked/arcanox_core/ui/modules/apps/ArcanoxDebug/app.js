angular
	.module( "beamng.apps" )
	.directive( "arcDebug", [ function () {
		return {
			templateUrl: "modules/apps/ArcanoxDebug/arc-debug.html",
			replace: true,
			restrict: "EA",
			scope: true,
			controller: [ "$scope", "StreamsManager", function ( $scope, StreamsManager ) {
				var StreamName = "arcanox:debug";

				StreamsManager.add( [ StreamName ] );

				$scope.$on( "$destroy", function () {
					StreamsManager.remove( [ StreamName ] );
				} );

				var curStreamName = null;
				var debugStreamNames = null;
				var streamCache = {};

				function updateScopeData( name, rawData ) {
					var debugData = [];

					Object.keys( rawData ).forEach( key => {
						var split = key.split( ":" );
						var id = +split[ 0 ];
						var name = split[ 1 ];

						debugData.push( { id, name, value: rawData[ key ] } );
					} );

					$scope.debugStreamName = name;
					$scope.debugData = debugData;
				}

				$scope.changeStream = function () {
					if ( !debugStreamNames || debugStreamNames.length === 0 )
						return;

					var curIndex = debugStreamNames.indexOf( curStreamName );

					// Delete the existing debug data to prepare for the new stream
					delete $scope.debugData;

					if ( curIndex < 0 ) {
						curStreamName = debugStreamNames[ 0 ];
						return;
					}

					curIndex = ( curIndex + 1 ) % debugStreamNames.length;
					curStreamName = debugStreamNames[ curIndex ];

					if ( streamCache[ curStreamName ] ) {
						updateScopeData( curStreamName, streamCache[ curStreamName ] );
					}
				};

				$scope.$on( "streamsUpdate", function ( _, streams ) {
					$scope.$evalAsync( function () {
						if ( streams != null && streams[ StreamName ] != null ) {
							var debugStreams = streams[ StreamName ];

							debugStreamNames = Object.keys( debugStreams );

							if ( debugStreamNames.length === 0 ) {
								return;
							}

							debugStreamNames.forEach( streamName => {
								streamCache[ streamName ] = debugStreams[ streamName ];
							} );

							if ( !curStreamName || !( curStreamName in debugStreams ) ) {
								if ( curStreamName ) {
									delete streamCache[ curStreamName ];
								}

								delete $scope.debugData;
								curStreamName = debugStreamNames[ 0 ];
								return;
							}

							var rawDebugData = debugStreams[ curStreamName ];

							updateScopeData( curStreamName, rawDebugData );
						} else {
							debugStreamNames = null;
							delete $scope.debugData;
						}
					} );
				} );
			} ]
		};
	} ] );
