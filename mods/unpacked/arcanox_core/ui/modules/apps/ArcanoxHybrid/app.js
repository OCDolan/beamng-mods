function TemporalSmoothing( inRate, outRate, startingValue ) {
	this.inRate = inRate || 1;
	this.outRate = outRate || this.inRate;
	this.state = startingValue || 0;
}

TemporalSmoothing.prototype.get = function ( sample, dt ) {
	var diff = sample - this.state;
	var rate = diff * this.state >= 0 ? this.outRate : this.inRate;

	this.state = this.state + diff * Math.min( rate * dt, 1 );
	return this.state;
}

angular
	.module( "beamng.apps" )
	.directive( "arcanoxHybrid", [ "StreamsManager", function ( StreamsManager ) {
		return {
			templateUrl: "modules/apps/ArcanoxHybrid/arc-hybrid.html",
			replace: true,
			restrict: "EA",
			link: function ( scope, element, attrs ) {
				function clamp( val, min, max ) {
					return Math.max( Math.min( val, max ), min );
				}

				function maprange( val, srcMin, srcMax, dstMin, dstMax, clampOutput ) {
					if ( clampOutput !== false ) clampOutput = true;
					if ( typeof dstMin !== "number" ) dstMin = 0.0;
					if ( typeof dstMax !== "number" ) dstMax = 1.0;

					var normalized = ( val - srcMin ) / ( srcMax - srcMin ); // 0.0 to 1.0 when inside[ srcMin, srcMax ]

					var dstRange = dstMax - dstMin;
					var output = dstRange * normalized + dstMin;

					if ( clampOutput ) {
						var cMin = Math.min( dstMin, dstMax );
						var cMax = Math.max( dstMin, dstMax );

						output = clamp( output, cMin, cMax );
					}

					return output;
				}

				StreamsManager.add( [ "hybrid" ] );

				scope.$on( "$destroy", function () {
					StreamsManager.remove( [ "hybrid" ] );
				} );

				var svgElement = element[ 0 ].querySelector( "#app-arc-hybrid" );

				svgElement.data += "?t=" + new Date().valueOf(); // bust the cache
				svgElement.addEventListener( "load", function () {
					var svg = svgElement.contentDocument;
					var rootLayer = element[ 0 ];
					var batteryLevelGauge = {
						valOver: svg.getElementById( "val-bat-over" ),
						valUnder: svg.getElementById( "val-bat-under" ),
						clip: {
							rect: svg.getElementById( "clip-bat-rect" ),
							height: +svg.getElementById( "clip-bat-rect" ).getAttribute( "height" ),
							y: +svg.getElementById( "clip-bat-rect" ).getAttribute( "y" ),
						},
					};
					var powerDrawGauge = {
						valOver: svg.getElementById( "val-kw-over" ),
						valUnder: svg.getElementById( "val-kw-under" ),
						clip: {
							rect: svg.getElementById( "clip-kw-rect" ),
							height: +svg.getElementById( "clip-kw-rect" ).getAttribute( "height" ),
							y: +svg.getElementById( "clip-kw-rect" ).getAttribute( "y" ),
						},
					};
					var readyIcon = {
						element: svg.getElementById( "icon-ready" ),
						onColor: "#00ebff",
						offColor: "#aaaaaa88",
					};
					var evIcon = {
						element: svg.getElementById( "icon-ev-mode" ),
						onColor: "#00ff00",
						offColor: "#aaaaaa88",
					};
					var modeText = svg.getElementById( "text-drive-mode" );

					const AverageWindow = 5;
					var updating = false;
					var updateInterval = null;
					var socSmoothing = new TemporalSmoothing( 2.0 );
					var kwSmoothing = new TemporalSmoothing( 2.0 );
					var time = new Date().valueOf();
					var dt = 0;
					var stateOfCharge = 0;
					var minStateOfCharge = 0;
					var maxStateOfCharge = 1;
					var powerDraw = 0;
					var maxPowerDraw = 1;
					var ready = false;
					var ev = false;
					var mode = "Off";

					function updateScreen() {
						// Update battery level meter
						var normalizedStateOfCharge = socSmoothing.get( maprange( stateOfCharge, minStateOfCharge, maxStateOfCharge ), dt );
						var batteryLevelHeight = normalizedStateOfCharge * batteryLevelGauge.clip.height;

						batteryLevelGauge.valOver.innerHTML = `${Math.round( stateOfCharge * 100 )}%`;
						batteryLevelGauge.valUnder.innerHTML = batteryLevelGauge.valOver.innerHTML;
						batteryLevelGauge.clip.rect.setAttribute( "height", batteryLevelHeight );
						batteryLevelGauge.clip.rect.setAttribute( "y", batteryLevelGauge.clip.y + ( batteryLevelGauge.clip.height - batteryLevelHeight ) );

						// Update power meter
						var smoothedPowerDraw = kwSmoothing.get( powerDraw, dt );

						powerDrawGauge.valOver.innerHTML = `${Math.round( smoothedPowerDraw )} kW`;
						powerDrawGauge.valUnder.innerHTML = powerDrawGauge.valOver.innerHTML;

						if ( maxPowerDraw === 0 ) {
							powerDrawPosHeight = powerDrawNegHeight = 0;
						} else {
							var halfHeight = powerDrawGauge.clip.height / 2.0;

							if ( smoothedPowerDraw >= 0 ) {
								var powerDrawPosNormalized = isNaN( smoothedPowerDraw ) ? 0 : clamp( smoothedPowerDraw / maxPowerDraw, 0, 1 );
								var powerDrawHeight = Math.abs( powerDrawPosNormalized ) * halfHeight;

								powerDrawGauge.clip.rect.setAttribute( "y", powerDrawGauge.clip.y + ( halfHeight - powerDrawHeight ) );
								powerDrawGauge.clip.rect.setAttribute( "height", powerDrawHeight );
							} else {
								var powerDrawNegNormalized = isNaN( smoothedPowerDraw ) ? 0 : clamp( smoothedPowerDraw / maxPowerDraw, -1, 0 );
								var powerDrawHeight = Math.abs( powerDrawNegNormalized ) * halfHeight;

								powerDrawGauge.clip.rect.setAttribute( "y", powerDrawGauge.clip.y + halfHeight );
								powerDrawGauge.clip.rect.setAttribute( "height", powerDrawHeight );
							}
						}

						// Update info section
						readyIcon.element.style.fill = ready ? readyIcon.onColor : readyIcon.offColor;
						evIcon.element.style.fill = ev ? evIcon.onColor : evIcon.offColor;
						modeText.innerHTML = mode;
					}

					scope.$on( "streamsUpdate", function ( event, streams ) {
						var newTime = new Date().valueOf();

						dt = ( newTime - time ) / 1000.0;
						time = newTime;

						if ( streams != null && streams.hybrid != null ) {
							rootLayer.style.opacity = 1.0;
							stateOfCharge = isNaN( streams.hybrid.soc ) ? 0.0 : streams.hybrid.soc;
							minStateOfCharge = isNaN( streams.hybrid.minSoc ) ? 0.0 : streams.hybrid.minSoc;
							maxStateOfCharge = isNaN( streams.hybrid.maxSoc ) ? 1.0 : streams.hybrid.maxSoc;
							powerDraw = isNaN( streams.hybrid.kw ) ? 0.0 : streams.hybrid.kw;
							maxPowerDraw = isNaN( streams.hybrid.maxKw ) ? 0.0 : streams.hybrid.maxKw;
							ready = !!streams.hybrid.powerOn;
							ev = !!streams.hybrid.ev;
							mode = streams.hybrid.mode;

							if ( !updating ) {
								updating = true;
								updateInterval = setInterval( updateScreen, 1000 / 60 ); // 60 fps
							}
						} else {
							rootLayer.style.opacity = 0.0;

							if ( updating ) {
								updating = false;
								clearInterval( updateInterval );
							}
						}
					} );
				} );
			}
		};
	} ] );
