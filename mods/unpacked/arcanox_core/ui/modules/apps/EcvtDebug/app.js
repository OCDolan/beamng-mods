angular
    .module( "beamng.apps" )
    .directive( "ecvtDebug", [ "StreamsManager", function ( StreamsManager ) {
        return {
            template:
                "<object class='bngApp' style='width:100%; height:100%; pointer-events: none' type='image/svg+xml' data='modules/apps/EcvtDebug/gearset.svg?t=" + Date.now() + "'/>",
            replace: true,
            restrict: "EA",
            link: function ( scope, element, attrs ) {
                function clamp( val, min, max ) {
                    return Math.max( Math.min( val, max ), min );
                }

                function maprange( val, srcMin, srcMax, dstMin, dstMax, clampOutput ) {
                    if ( clampOutput !== false ) clampOutput = true;
                    if ( typeof dstMin !== "number" ) dstMin = 0.0;
                    if ( typeof dstMax !== "number" ) dstMax = 1.0;

                    var normalized = ( val - srcMin ) / ( srcMax - srcMin ); // 0.0 to 1.0 when inside[ srcMin, srcMax ]

                    var dstRange = dstMax - dstMin;
                    var output = dstRange * normalized + dstMin;

                    if ( clampOutput ) {
                        var cMin = Math.min( dstMin, dstMax );
                        var cMax = Math.max( dstMin, dstMax );

                        output = clamp( output, cMin, cMax );
                    }

                    return output;
                }

                function getAngle( el ) {
                    var att = el.getAttribute( "transform" );

                    if ( !att )
                        return 0;

                    var match = att.match( /rotate\(([\d\.\-e]+)\)/i );

                    if ( !match )
                        return 0;

                    return +match[ 1 ] * Math.PI / 180;
                }

                function setAngle( el, angle ) {
                    var att = el.getAttribute( "transform" ) || "rotate(0)";
                    var match = att.match( /rotate\(.*\)/i );
                    var angleDeg = Math.floor( ( angle * 180 / Math.PI ) * 100 ) / 100;
                    var bbox = el.getBBox();
                    var cx = bbox.x + ( bbox.width / 2.0 );
                    var cy = bbox.y + ( bbox.height / 2.0 );

                    if ( match ) {
                        att = att.replace( /rotate\(.*\)/, `rotate(${angleDeg}, ${cx}, ${cy})` );
                    } else {
                        att = `${att} rotate(${angleDeg}, ${cx}, ${cy})`;
                    }

                    el.setAttribute( "transform", att );
                }

                StreamsManager.add( [ "ecvt" ] );

                scope.$on( "$destroy", function () {
                    StreamsManager.remove( [ "ecvt" ] );
                } );

                element.on( "load", function () {
                    var N_SUN = 20;
                    var N_PLANETS = 10;
                    var N_RING = 40;
                    var AV_SCALE = 1 / 250;

                    var svg = element[ 0 ].contentDocument;
                    var ringGear = svg.getElementById( "gear_ring" );
                    var sunGear = svg.getElementById( "gear_sun" );
                    var planetGears = [ 1, 2, 3, 4 ].map( d => svg.getElementById( `gear_planet${d}` ) );
                    var carrierGroup = svg.getElementById( "group_carrier" );
                    var angles = {
                        ring: getAngle( ringGear ),
                        sun: getAngle( sunGear ),
                        planets: planetGears.map( getAngle ),
                        carrier: getAngle( carrierGroup ),
                    };

                    var prevTime;
                    var curTime = performance.now();
                    var dt;
                    var ringAv = 0,
                        sunAv = 0,
                        carrierAv = 0,
                        planetsAv = 0;
                    var ringTeeth = N_RING,
                        sunTeeth = N_SUN,
                        planetsTeeth = N_PLANETS;

                    scope.$on( "streamsUpdate", function ( event, streams ) {
                        ringAv = 0;
                        sunAv = 0;
                        carrierAv = 0;
                        ringTeeth = N_RING;
                        sunTeeth = N_SUN;
                        planetsTeeth = N_PLANETS;

                        prevTime = curTime;
                        curTime = performance.now();
                        dt = ( curTime - prevTime ) / 1000.0;

                        if ( streams != null && streams.ecvt != null ) {
                            var ecvt = streams.ecvt;

                            ringAv = ecvt.ringAv;
                            carrierAv = ecvt.carrierAv;
                            sunAv = ecvt.sunAv;

                            ringTeeth = ecvt.ringTeeth;
                            sunTeeth = ecvt.sunTeeth;
                            planetsTeeth = ecvt.planetsTeeth;
                        }

                        // Figure out AV of planet gears (this is purely visual; use the number of teeth on the SVG)
                        planetsAv = (ringTeeth * ringAv + (sunTeeth + planetsTeeth) * carrierAv - (ringTeeth - planetsTeeth) * carrierAv - sunTeeth * sunAv) / (2 * planetsTeeth)

                        // Scale the speeds
                        ringAv *= AV_SCALE;
                        sunAv *= AV_SCALE;
                        carrierAv *= AV_SCALE;
                        planetsAv *= AV_SCALE;

                        // Update angles based on AV (angles are in radians, AV is rads/sec)
                        angles.ring += ringAv * dt;
                        angles.sun += sunAv * dt;
                        angles.carrier += carrierAv * dt;

                        for ( var i = 0; i < angles.planets.length; i++ )
                            angles.planets[ i ] += planetsAv * dt;

                        // Update actual element angles
                        setAngle( ringGear, angles.ring );
                        setAngle( sunGear, angles.sun );
                        setAngle( carrierGroup, angles.carrier );

                        planetGears.forEach( ( p, i ) => setAngle( p, angles.planets[ i ] ) );
                    } );
                } );
            }
        };
    } ] );
