local dd = obj.debugDrawProxy
local textCol = color(255, 60, 255, 255)
local mathutil = require("mathutil")

local debugModules = {}
local debugStream

local function sendWholeStream()
    if not streams.willSend("arcanox:debug") then return end
    
    debugStream = {}
    
    for k, v in pairs(debugModules) do
        if v.enabled then
            debugStream[k] = v.debugStream
        end
    end
    
    gui.send("arcanox:debug", debugStream)
end

local debugObj = {}
debugObj.__index = debugObj

local function getOrCreateDebugObj(logTag, enable)
    if debugModules[logTag] then
        return debugModules[logTag]
    end
    
    local obj = {
        logTag = logTag,
        enabled = enable or false,
        ids = {},
        debugStream = {},
        numVals = 0,
    }
    
    setmetatable(obj, debugObj)
    debugModules[logTag] = obj

    return obj
end

function debugObj:dText(row, text)
    if not self.enabled then
        return
    end

    dd:drawText2D(float3(20, 20 + (15 * row), 0), textCol, text)
end

function debugObj:log(level, text)
    if not self.enabled then
        return
    end

    if not text then -- Shift arguments
        text = level
        level = "D"
    end

    log(level, self.logTag, text)
end

function debugObj:formatNumber(number, places)
    if number == nil then return "nil" end
    
    places = places or 2
    
    return tostring(mathutil.round(number, places))
end

function debugObj:putValue(key, value, places)
    if not self.ids[key] then
        self.ids[key] = self.numVals
        self.numVals = self.numVals + 1
    end
    
    if type(value) == "number" and type(places) == "number" then
        value = self:formatNumber(value, places)
    end
        
    self.debugStream[self.ids[key] .. ":" .. key] = value
end

function debugObj:clearValues() 
    self.ids = {}
    self.debugStream = {}
    self.numVals = 0
end

function debugObj:sendStreams()
    sendWholeStream()
end

function debugObj:dispose()
    if debugModules[self.logTag] then
        debugModules[self.logTag] = nil
    end
end

return getOrCreateDebugObj