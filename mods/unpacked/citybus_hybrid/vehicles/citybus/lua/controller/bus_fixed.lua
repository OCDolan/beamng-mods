local M = {}

M.type = "auxilliary"
M.relevantDevice = nil
M.defaultOrder = 999

local bus = nil
local _onGameplayEvent

local function geCallback(event,data)
  local pos = obj:getPosition()
  local rot = obj:getRotation()

  data.event = event
  --transform userdata into table
  data.pos = vec3(pos):toTable()
  data.rot = quat(rot):toTable()
  obj:queueGameEngineLua("if core_busRouteManager then core_busRouteManager.onBusUpdate("..dumps(data)..") end")
end

local function onGameplayEvent(eventName, eventData)
    if eventName == "bus_onTriggerTick" then
        eventData.speed = electrics.values.wheelspeed * 3.6
        eventData.bus_dooropen = bus.doorsOpen
        eventData.bus_kneel = bus.isKneeling
        
        -- Fix the "Stop the bus" bug due to wheelspeed being non-zero even when stopped
        if eventData.speed < 0.5 then
            eventData.speed = 0
        end
        
        geCallback("onTriggerTick", eventData)
    else
        -- Call the original handler
        
        _onGameplayEvent(eventName, eventData)
    end
end

local function init()
end

local function initSecondStage()
    -- Patch the bus's onGameplayEvent handler to fix the "Stop the bus" bug while driving bus routes
    bus = controller.getController("bus")
    _onGameplayEvent = bus.onGameplayEvent
    bus.onGameplayEvent = onGameplayEvent
end

M.init = init
M.initSecondStage = initSecondStage

return M