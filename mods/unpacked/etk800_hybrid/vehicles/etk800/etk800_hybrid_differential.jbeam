{
"etk800_ecvt_differential_F": {
    "information":{
        "authors":"Arcanox",
        "name":"Open Front Differential (for eCVT)",
        "value":800,
    },
    "slotType" : "etk800_differential_F",
    "slots": [
        ["type", "default", "description"],
        ["etk800_halfshafts_F","etk800_halfshafts_F", "Front Halfshafts"],
    ],
    "controller": [
        ["fileName"],
        ["drivingDynamics/actuators/electronicDiffLock", {"name":"lockFront", "differentialName":"differential_F"}],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["torsionReactor", "torsionReactorF", "gearbox", 1],
        ["differential", "differential_F", "torsionReactorF", 1, {"diffType":"open", "gearRatio": 3.476, "friction":6, "uiName":"Front Differential","defaultVirtualInertia":0.25}]
    ],
    "torsionReactorF": {
        "torqueReactionNodes:":["e1l","e1r","e2r"]
    },
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["etk800_halfshaft_F", ["etk800_lowerarm_F_b","etk800_subframe_F","etk800_diff_F"]],
        ["etk800_diff_F", ["etk800_subframe_F","etk800_diff_F"]],
    ],
    "nodes": [
            ["id", "posX", "posY", "posZ"],
            //--diff weight--
            {"selfCollision":false},
            {"collision":false},
            {"nodeMaterial":"|NM_METAL"},
            {"frictionCoef":0.5},
            {"group":"etk800_diff_F"},
            {"nodeWeight":31},
            ["fdiff", 0.0, -1.40, 0.36],
            {"group":""},
    ],
    "beams": [
            ["id1:", "id2:"],
            {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
            {"beamSpring":1500600,"beamDamp":60},
            {"beamDeform":30000,"beamStrength":"FLT_MAX"},
            ["fdiff","fx2r"],
            ["fdiff","fx2l"],
            ["fdiff","fx5r"],
            ["fdiff","fx5l"],
            ["fdiff","f10ll"],
            ["fdiff","f12ll"],
            //halfshafts
            {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.04, "beamShortBound":0.04},
            {"beamSpring":0,"beamDamp":0,"beamDeform":6000,"beamStrength":16000},
            {"beamLimitSpring":2501000,"beamLimitDamp":100},
            {"optional":true},
            {"breakGroupType":1},
            {"breakGroup":"wheel_FR"},
            ["fw1r","fdiff", {"name":"axle_FR"}],
            {"breakGroup":"wheel_FL"},
            ["fw1l","fdiff", {"name":"axle_FL"}],
            {"breakGroup":""},
            {"breakGroupType":0},
            {"optional":false},
            {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
},
"etk_electric_differential_R": {
    "information":{
        "authors":"Arcanox",
        "name":"Electric Open Rear Differential",
        "value":1500,
    },
    "slotType" : "etk800_differential_R",
    "slots": [
        ["type", "default", "description"],
        ["etk800_halfshafts_R","etk800_halfshafts_R", "Rear Halfshafts"],
        ["etk800_finaldrive_R","etk800_finaldrive_R_323", "Rear Final Drive", {"coreSlot":true}],
    ],
    "controller": [
        ["fileName"],
        ["drivingDynamics/actuators/electronicDiffLock", {"name":"lockRear", "differentialName":"differential_R"}],
    ],
    "powertrain" : [
        ["type", "name", "inputName", "inputIndex"],
        ["arcanoxElectricMotor", "rearMotor", "dummy", 1],
        ["differential", "differential_R", "rearMotor", 1, {"diffType":"open", "gearRatio": 3.476, "friction":8, "uiName":"Rear Differential","defaultVirtualInertia":0.25}]
    ],
    "flexbodies": [
        ["mesh", "[group]:", "nonFlexMaterials"],
        ["etk800_diff", ["etk800_subframe_R","etk800_body"]],
        ["etk800_halfshaft_R", ["etk800_hub_R","etk800_lowerarm_R","etk800_diff_R"]],
    ],
    "nodes": [
        ["id", "posX", "posY", "posZ"],
        //--diff weight--
        {"selfCollision":false},
        {"collision":false}
        {"nodeMaterial":"|NM_METAL"},
        {"frictionCoef":0.5},
        {"group":"etk800_diff_R"},
        {"nodeWeight":45},
        ["rdiff", 0, 1.41, 0.26],
        {"group":""},
    ],
    "beams": [
        ["id1:", "id2:"],
        //differential node
        {"beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
        {"beamSpring":4800600,"beamDamp":138},
        {"beamDeform":27000,"beamStrength":"FLT_MAX"},
        //["rdiff","r2"],
        ["rdiff","rx1"],
        ["rdiff","r1"],
        ["rdiff","rx2r"],
        ["rdiff","rx2l"],
        ["rdiff","rx1r"],
        ["rdiff","rx1l"],
        ["rdiff","rx4r"],
        ["rdiff","rx4l"],
        ["rdiff","rx5r"],
        ["rdiff","rx5l"],
        //halfshafts
        {"beamPrecompression":1, "beamType":"|BOUNDED", "beamLongBound":0.02, "beamShortBound":0.02},
        {"beamSpring":0,"beamDamp":0,"beamDeform":3600,"beamStrength":9500},
        {"beamLimitSpring":7501000,"beamLimitDamp":230},
        {"breakGroupType":1},
        {"optional":true},
        {"breakGroup":"wheel_RR"},
        ["rw1r","rdiff", {"name":"axle_RR"}],
        {"breakGroup":"wheel_RL"},
        ["rw1l","rdiff", {"name":"axle_RL"}],
        {"breakGroup":""},
        {"optional":false},
        {"breakGroupType":0},
        {"beamPrecompression":1, "beamType":"|NORMAL", "beamLongBound":1.0, "beamShortBound":1.0},
    ],
}
}