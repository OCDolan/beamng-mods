

angular.module( 'gaugesScreen', [] )
	.controller( 'GaugesScreenController', function ( $scope, $window ) {
		var unit = null;
		var kw = null;
		var powerOn = false;
		var evmode = false;
		var regenKw = 80;
		var ecoKw = 15;
		var maxKw = 120;
		var batteryLevel = 0;

		function maprange( x, fromStart, fromEnd, toStart, toEnd, clamp ) {
			toStart = toStart || 0;
			toEnd = toEnd || 1;
			clamp = clamp !== false;

			var ret = ( ( x - fromStart ) / ( fromEnd - fromStart ) * ( toEnd - toStart ) ) + toStart;

			if ( clamp ) {
				var cMin = Math.min( toStart, toEnd );
				var cMax = Math.max( toStart, toEnd );

				ret = Math.max( Math.min( ret, cMax ), cMin );
			}

			return ret;
		}

		$scope.data = {}
		// overwriting plain javascript function so we can access from within the controller
		$window.setUnits = ( data ) => {
			unit = data.unitType;
		}

		$window.setHybridParams = data => {
			regenKw = data.regenKw;
			ecoKw = data.ecoKw;
			maxKw = data.maxKw;
		}

		$window.updateHybridData = data => {
			kw = data.kw;
			powerOn = data.powerOn;
			evmode = data.evmode;
			batteryLevel = data.batteryLevel;
		}

		$window.updateData = ( data ) => {
			$scope.$evalAsync( function () {
				// We need access to the efficiency bar svg element so that we can animate it
				var eff = document.getElementById( "efficiency" );
				// dash array needs to be the same as the circumference of the circle (radius * 2 * PI)

				eff.style.strokeDasharray = 342;

				if ( kw >= 0 ) {
					if ( kw <= ecoKw ) {
						eff.style.stroke = "#4EFF4E";
					} else {
						eff.style.stroke = "#EFBE49";
					}
					// We need to add the value to the negative circumference of the circle so that we can fill up the bar
					eff.style.strokeDashoffset = -342 + maprange( kw, 0, ecoKw, 0, 24 ) + maprange( kw, ecoKw, maxKw, 0, 47 );
				} else {
					eff.style.stroke = "#00e4ff";
					// We need to add the value to the negative circumference of the circle so that we can fill up the bar
					eff.style.strokeDashoffset = -342 + maprange( kw, 0, -regenKw, 0, -22 );
				}

				if ( data.gear === -1 ) {
					$scope.data.gear = "R";
				}
				else if ( data.gear === 0 ) {
					$scope.data.gear = "N";
				}
				else {
					$scope.data.gear = data.gear;
				}

				$scope.data.gaugesVisible = powerOn || false;
				$scope.data.time = data.time;
				$scope.data.evMode = evmode ? "EV" : "";

				// checking if metric gauge cluster is being used
				if ( unit === "metric" ) {
					$scope.data.speedVal = ( data.speed * 3.6 ).toFixed( 0 );
					$scope.data.speedUnit = "km/h";
					if ( data.temp.toFixed( 1 ) > 99.9 || data.temp.toFixed( 1 ) < -99.9 ) {
						$scope.data.temp = "---°C";
					}
					else {
						$scope.data.temp = data.temp.toFixed( 1 ) + "°C";
					}
					if ( data.averageFuelConsumption === 0 ) {
						;
						$scope.data.consumptionVal = "---"
					}
					else {
						$scope.data.consumptionVal = data.averageFuelConsumption.toFixed( 1 )
					}
					$scope.data.consumptionUnit = "l/100km";
				}
				else {
					$scope.data.speedVal = ( data.speed * 2.23694 ).toFixed( 0 );
					$scope.data.speedUnit = "mph";
					$scope.data.temp = ( data.temp * 1.8 + 32 ).toFixed( 0 ) + "°F";
					if ( data.averageFuelConsumption === 0 ) {
						$scope.data.consumptionVal = "---";
					}
					else {
						$scope.data.consumptionVal = ( 235 / data.averageFuelConsumption ).toFixed( 1 );
					}
					$scope.data.consumptionUnit = "mpg";
				}

				// set text of SVG to hybrid variant
				document.getElementById( 'markOne' ).textContent = "0";
				document.getElementById( 'markTwo' ).textContent = "eco";
				document.getElementById( 'markThree' ).textContent = "";
				document.getElementById( 'markFour' ).textContent = "max";

				// update battery gauge
				var batGaugeFill = document.getElementById( "battery_fill" );
				var batGaugeStepSize = 4;
				var batGaugeMaxSteps = 14;
				var batGaugeOffset = 11.05;
				var numSteps = Math.round( ( 1 - batteryLevel ) * batGaugeMaxSteps );

				batGaugeFill.style.y = `${batGaugeOffset + numSteps * batGaugeStepSize}px`;
			} )
		}
	} );    