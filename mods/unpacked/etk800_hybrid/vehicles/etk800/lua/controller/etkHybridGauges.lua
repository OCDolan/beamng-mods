local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 2000

local htmlTexture = require("htmlTexture")
local mathutil = require("arcanox/mathutil")
local debugutil = require("arcanox/debugutil")("hybridGauges", true)

-- upvalue functions
local abs = math.abs
local blend = mathutil.blend
local clamp = mathutil.clamp

-- other parts
local hybrid
local gearbox

-- state
local lastPower = false
local isInStartupAnim = false
local startupAnimTime = 0

local function updateGFX(dt)
    local rpmDisplay
    local speedDisplay
    
    if hybrid.powerOn or hybrid.powerSwitch then
        if not lastPower then
            isInStartupAnim = true
            startupAnimTime = 0
            lastPower = true
        end
        
        if isInStartupAnim then
            local movingAnimTime = M.totalAnimationTime - 0.5 -- half second pause at end
            local animTimeWithPause = movingAnimTime + 0.1
            local gaugeCoef = clamp(animTimeWithPause - abs(2 * animTimeWithPause * startupAnimTime - animTimeWithPause), 0, 1) -- fancy time function to animate up and back down
            
            rpmDisplay = blend(M.minRpm, M.maxRpm, gaugeCoef)
            speedDisplay = blend(M.minSpeed, M.maxSpeed, gaugeCoef)
            startupAnimTime = startupAnimTime + dt
            
            if startupAnimTime >= M.totalAnimationTime then
                isInStartupAnim = false
                startupAnimTime = 0
            end
        else
            -- Push correct value to tacho
            rpmDisplay = electrics.values.rpm
            speedDisplay = electrics.values.wheelspeed
        end
    else
        -- Make tacho needle drop to "Off"
        rpmDisplay = M.minRpm
        speedDisplay = M.minSpeed
        lastPower = false
    end
    
    electrics.values.rpmDisplay = M.rpmDisplaySmoother:get(rpmDisplay, dt)
    electrics.values.speedDisplay = M.speedDisplaySmoother:get(speedDisplay, dt)
    
    local propulsionPower = electrics.values.kw
    local hybridData = {
        --kw = M.kwDisplaySmoother:get(gearbox.motorGenerator2Power, dt),
        kw = M.kwDisplaySmoother:get(propulsionPower, dt),
        powerOn = hybrid.powerSwitch,
        evmode = (electrics.values.ev or 0) > 0,
        batteryLevel = electrics.values.batteryLevel or 0,
    }
    
    htmlTexture.call(M.gaugesScreenName, "updateHybridData", hybridData)
    
    debugutil:putValue("Startup Animation", isInStartupAnim)
    debugutil:putValue("Startup Anim. Time", startupAnimTime, 3)
    debugutil:putValue("RPM Display", rpmDisplay, 3)
    debugutil:putValue("Speed Display", speedDisplay, 3)
end

local function init(jbeamData)
    M.jbeamData = jbeamData
    
    M.minRpm = jbeamData.minRpm or -500
    M.maxRpm = jbeamData.maxRpm or 7500
    M.minSpeed = jbeamData.minSpeed or 0
    M.maxSpeed = jbeamData.maxSpeed or 72.222 -- m/s
    M.totalAnimationTime = jbeamData.totalAnimationTime or 2.0
    M.rpmDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.rpmSmoothing or 10)
    M.speedDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.speedSmoothing or 10)
    M.kwDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.kwSmoothing or 10)
    
    M.gaugesScreenName = jbeamData.materialName
    M.htmlPath = jbeamData.htmlPath
    
    if not M.gaugesScreenName then
        log("E", "etkHybridGauges", "Got no material name for the texture, can't display anything...")
        M.updateGFX = nop
        return
    end
    
    if not M.htmlPath then
        log("E", "etkHybridGauges", "Got no html path for the texture, can't display anything...")
        M.updateGFX = nop
    end
    
    M.regenKw = jbeamData.regenKw or 80
    M.ecoKw = jbeamData.ecoKw or 20
    M.maxKw = jbeamData.maxKw or 120
    
    htmlTexture.call(M.gaugesScreenName, "setHybridParams", {
        regenKw = M.regenKw,
        ecoKw = M.ecoKw,
        maxKw = M.maxKw,
    })
end

local function initSecondStage()
    hybrid = controller.getController("hybridArcanox")
    gearbox = powertrain.getDevice("gearbox")
    
    if not hybrid then
        log("E", "etkHybridGauges", "Hybrid controller not found")
        M.updateGFX = nop
        return
    end
    
    if not gearbox or gearbox.type ~= "ecvtGearbox" then
        log("E", "etkHybridGauges", "Could not find eCVT gearbox")
        M.updateGFX = nop
        return
    end
end

local function reset()
    init(M.jbeamData)
    initSecondStage()
end

M.updateGFX = updateGFX
M.init = init
M.initSecondStage = initSecondStage
M.reset = reset

return M