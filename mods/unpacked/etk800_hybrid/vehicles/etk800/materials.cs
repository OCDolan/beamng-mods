singleton Material(etk800_hybrid_lettering)
{
    mapTo = "etk800_hybrid_lettering";
    specularMap[0] = "vehicles/etk800/etk800_hybrid_lettering_s.png";
    normalMap[0] = "vehicles/etk800/etk800_hybrid_lettering_n.png";
    diffuseMap[0] = "vehicles/etk800/etk800_hybrid_lettering_d.png";
    reflectivityMap[0] = "vehicles/etk800/etk800_hybrid_lettering_s.png";
    specularPower[0] = "128";
    pixelSpecular[0] = "1";
    diffuseColor[0] = "1 1 1 1";
    useAnisotropic[0] = "1";
    castShadows = "0";
    translucent = "1";
    //translucentBlendOp = "None";
    alphaTest = "0";
    alphaRef = "0";
    dynamicCubemap = true;
    materialTag0 = "beamng"; materialTag1 = "vehicle"; materialTag2 = "decal";
    //translucentZWrite = "1";
};