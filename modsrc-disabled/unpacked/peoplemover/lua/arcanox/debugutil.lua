local dd = obj.debugDrawProxy
local textCol = color(255, 60, 255, 255)

local debugModules = {}
local debugStream

local function sendWholeStream()
    debugStream = {}
    
    for k, v in pairs(debugModules) do
        debugStream[k] = v.debugStream
    end
    
    gui.send("arcanox:debug", debugStream)
end

return function(logTag, enable)
    if debugModules[logTag] then
        return debugModules[logTag]
    end
    
    local M = {
        enabled = enable or false,
        ids = {},
        debugStream = {},
        numVals = 0,
    }

    function dText(row, text)
        if not M.enabled then
            return
        end

        dd:drawText2D(float3(20, 20 + (15 * row), 0), textCol, text)
    end

    function _log(level, text)
        if not M.enabled then
            return
        end

        if not text then -- Shift arguments
            text = level
            level = "D"
        end

        log(level, logTag, text)
    end
    
    function putValue(key, value)
        if not M.ids[key] then
            M.ids[key] = M.numVals
            M.numVals = M.numVals + 1
        end
            
        M.debugStream[M.ids[key] .. ":" .. key] = value
    end
    
    function clearValues() 
        M.ids = {}
        M.debugStream = {}
        M.numVals = 0
    end
    
    function sendStreams()
        sendWholeStream()
    end

    M.dText = dText
    M.log = _log
    M.putValue = putValue
    M.clearValues = clearValues
    M.sendStreams = sendStreams
    
    debugModules[logTag] = M

    return M
end