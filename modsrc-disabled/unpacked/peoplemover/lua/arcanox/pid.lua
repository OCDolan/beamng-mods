local M = {}

function get(pid, target, actual, dt)
    local error = target - actual
    local delta = actual - pid.lastValue
    local errorDelta = (error - pid.lastError) / dt
    local pFactor = pid.kP * pid.scale * error
    local dFactor = pid.kD * pid.scale * errorDelta
    local lockI = pid.lockI
    local lastErrorSum = pid.errorSum
    
    if not lockI then
        pid.errorSum = pid.errorSum + error * dt
    end
    
    local iFactor = pid.kI * pid.scale * pid.errorSum
    local output = pFactor + iFactor + dFactor
    
    if pid.minOutClamp ~= nil and pid.maxOutClamp ~= nil then
        -- Check to see if the change in error sum made us overflow the output
        
        if (output > pid.maxOutClamp and pid.errorSum > lastErrorSum) 
        or (output < pid.minOutClamp and pid.errorSum < lastErrorSum) then
            -- Clamp output
            pid.errorSum = lastErrorSum
            iFactor = pid.kI * pid.scale * pid.errorSum
        end
        
        output = math.max(math.min(pFactor + iFactor + dFactor, pid.maxOutClamp), pid.minOutClamp)
    end
    
    if pid.dThreshold and math.abs(delta) > pid.dThreshold then
        lockI = true
        
        if pid.dThresholdReset then
            pid.errorSum = 0
        end
    end
    
    if pid.minIClamp then
        pid.errorSum = math.max(pid.errorSum, pid.minIClamp)
    end
    
    if pid.maxIClamp then
        pid.errorSum = math.min(pid.errorSum, pid.maxIClamp)
    end
    
    pid.lastError = error
    pid.lastValue = actual
    
    return output
end

function reset(pid)
    pid.lastError = 0
    pid.lastValue = 0
    pid.errorSum = 0
    
    return pid
end

function setILock(pid, lockI)
    pid.lockI = lockI
    
    return pid
end

function setDynamicILock(pid, dThreshold, resetWhenOutside)
    pid.dThreshold = dThreshold
    pid.dThresholdReset = resetWhenOutside
    
    return pid
end

function setScale(pid, scale)
    pid.scale = scale
    
    return pid
end

function setIClamp(pid, minIClamp, maxIClamp)
    if minIClamp and not maxIClamp then
        maxIClamp = minIClamp
        minIClamp = -maxIClamp
    end
    
    pid.minIClamp = minIClamp
    pid.maxIClamp = maxIClamp
    
    return pid
end

function clampOutput(pid, minClamp, maxClamp)
    if not minClamp or not maxClamp then
        pid.minOutClamp = nil
        pid.maxOutClamp = nil
    else
        pid.minOutClamp = minClamp
        pid.maxOutClamp = maxClamp
    end
    
    return pid
end

function newPid(kP, kI, kD, minIClamp, maxIClamp)
    local pid = {
        kP = kP or 0,
        kI = kI or 0,
        kD = kD or 0,
        minIClamp = minIClamp,
        maxIClamp = maxIClamp,
        minOutClamp = nil,
        maxOutClamp = nil,
        scale = 1,
        
        lastError = 0,
        lastValue = 0,
        errorSum = 0,
        lockI = false,
        dThreshold = nil,
        dThresholdReset = false,
        
        get = get,
        reset = reset,
        setILock = setILock,
        setDynamicILock = setDynamicILock,
        setScale = setScale,
        setIClamp = setIClamp,
        clampOutput = clampOutput,
    }
    
    return pid
end

M.newPid = newPid

return M